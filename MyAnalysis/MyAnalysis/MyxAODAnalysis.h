#ifndef MyAnalysis_MyxAODAnalysis_H
#define MyAnalysis_MyxAODAnalysis_H


#include <EventLoop/Algorithm.h>

// Truth                                                                                                                                                                                                         
#include "xAODTruth/TruthEventContainer.h"                                                                                                                                                                        
#include "xAODTruth/TruthEvent.h"                                                                                                                                                                                
#include "xAODTruth/TruthParticle.h"                                                                                                                                                                             
#include "xAODTruth/TruthParticleContainer.h"                                                                                                                                                                    
                          
// EDM includes:
#include "xAODEventInfo/EventInfo.h"
#include "xAODRootAccess/tools/Message.h"
#include "xAODRootAccess/TEvent.h"                        

#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>

/* xAOD includes */
#include "xAODRootAccess/tools/Message.h"
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"
#include "xAODMuon/Muon.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODMuon/MuonAuxContainer.h"
#include "xAODEgamma/Egamma.h"
#include "xAODEgamma/Electron.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/ElectronAuxContainer.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODTau/TauJetContainer.h"
#include "xAODTau/TauJetAuxContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODBase/IParticle.h"
#include "xAODBase/IParticleContainer.h"
#include "xAODBase/IParticleHelpers.h"
#include "xAODCore/ShallowAuxContainer.h"
#include "xAODCore/ShallowCopy.h"
#include "xAODCutFlow/CutBookkeeper.h"
#include "xAODCutFlow/CutBookkeeperContainer.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODMissingET/MissingETAssociationMap.h"
#include "xAODMissingET/MissingETAuxContainer.h"

/* Tools includes */
#include "GoodRunsLists/GoodRunsListSelectionTool.h"
#include "PileupReweighting/PileupReweightingTool.h"
#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "TriggerMatchingTool/MatchingTool.h"
#include "MuonSelectorTools/MuonSelectionTool.h"
#include "MuonMomentumCorrections/MuonCalibrationAndSmearingTool.h"
#include "MuonEfficiencyCorrections/MuonTriggerScaleFactors.h"
#include "MuonEfficiencyCorrections/MuonEfficiencyScaleFactors.h"
#include "ElectronPhotonSelectorTools/AsgElectronLikelihoodTool.h"
#include "ElectronPhotonFourMomentumCorrection/EgammaCalibrationAndSmearingTool.h"
#include "EgammaAnalysisHelpers/PhotonHelpers.h"
#include "ElectronPhotonSelectorTools/AsgPhotonIsEMSelector.h"
#include "ElectronEfficiencyCorrection/AsgElectronEfficiencyCorrectionTool.h"
#include "TauAnalysisTools/TauSelectionTool.h"
#include "TauAnalysisTools/TauEfficiencyCorrectionsTool.h"
#include "TauAnalysisTools/TauOverlappingElectronLLHDecorator.h"
#include "TauAnalysisTools/TauTruthMatchingTool.h"
#include "TauAnalysisTools/TauSmearingTool.h"
#include "IsolationSelection/IsolationSelectionTool.h"
#include "IsolationCorrections/IsolationCorrectionTool.h"
#include "JetMomentTools/JetVertexTaggerTool.h"
#include "JetMomentTools/JetForwardJvtTool.h"
#include "JetSelectorTools/JetCleaningTool.h"
#include "JetJvtEfficiency/JetJvtEfficiency.h"
#include "JetCalibTools/JetCalibrationTool.h"
#include "JetResolution/JERTool.h"
#include "JetResolution/JERSmearingTool.h"
#include "JetUncertainties/JetUncertaintiesTool.h"
#include "xAODBTaggingEfficiency/BTaggingSelectionTool.h"
#include "xAODBTaggingEfficiency/BTaggingEfficiencyTool.h"
#include "xAODPrimitives/IsolationType.h"
#include "METUtilities/METMaker.h"
#include "METUtilities/CutsMETMaker.h"
#include "METUtilities/METSystematicsTool.h"
#include "AssociationUtils/OverlapRemovalInit.h"
#include "AssociationUtils/OverlapRemovalDefs.h"
#include "PATInterfaces/CorrectionCode.h"
#include "PATInterfaces/ISystematicsTool.h"
#include "LFVUtils/MCSampleInfo.h"
#include "LFVUtils/MCSampleOverlap.h"

/* ROOT includes */
#include <TH1.h>
#include <TObject.h>
#include <TLorentzVector.h>
#include <TTree.h>
#include <TFile.h>

/* Package includes */
#include <MyAnalysis/EnumDef.h>

/* ---------------------------------------------------- */
/* Helper for checking xAOD::TReturnCode return values */
#define CHECK( CONTEXT, EXP )                           \
  do {                                                  \
    if( ! EXP.isSuccess() ) {                           \
      Error( CONTEXT,                                   \
	     XAOD_MESSAGE( "Failed to execute: %s" ),   \
	     #EXP );                                    \
      return EL::StatusCode::FAILURE;                   \
    }                                                   \
  } while( false )
/* -----------------------------------------------------*/




using namespace std;



class MyxAODAnalysis : public EL::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
private:
  xAOD::TEvent *event; //!


public:
  
  TTree* lfvTree; //!

  std::string m_MCtype; 

  // float cutValue;
  int m_eventCounter; //!
  int m_numCleanEvents; //!
  int NumberElectronMuon; //!
  int NumberElectronTau; //!
  int NumberMuonTau; //!
  int NumberElectronMuon_MET; //!
  int NumberElectronTau_MET; //!
  int NumberMuonTau_MET; //!
  double m_nEventsProcessed; //!
  double m_sumOfWeights; //!
  bool m_NoTauId      ; //!
  bool m_FailTauId    ; //!
  double m_sumOfWeightsSquared; //!

  bool m_isData; 
  bool m_isDerivation; //!
  bool NewFile; //!
  bool CalcOwnMu; //!
  bool m_debug; 
  bool ApplyCorrections; 
  bool m_FastSim; 
  bool m_electronMuon; //!
  bool m_electronTau; //!
  bool m_muonTau; //!
  int m_year;         //!

  TH1F *h_DeltaPhi; //!
  TH1F *h_MuonPt; //!
  TH1F *h_MuonIso; //!
  TH1F *h_BornMass; //!
  TH1F *h_BornMassLinear; //!
  TH1F *h_InvMass; //!
  TH1F *h_InvMass_emu; //!
  TH1F *h_InvMass_etau; //!
  TH1F *h_InvMass_mutau; //!
  TH1F *h_InvMassTemplate; //!
  TH1F *h_MuonEta; //!
  TH1F *h_CutFlow; //!
  TH1F *h_emuCutFlow; //!
  TH1F *h_etauCutFlow; //!
  TH1F *h_mutauCutFlow; //!
  TH1F *h_PhotonCutFlow; //!
  TH1F *h_ElectronCutFlow; //!
  TH1F *h_TauCutFlow; //!
  TH1F *h_MuonCutFlow; //!
  TH1F *h_JetCutFlow; //!
  TH1F *h_Ae_emu; //!
  TH1F *h_Ae_etau; //!
  TH1F *h_Ae_mutau; //!

  TProfile* MassResolution; //!
  TProfile* MassResolutionLog; //!
  TProfile* MassResolution_emu; //!

  TH1 *h_L1CaloWeightedEvents_emu; //!
  TH1 *h_L1CaloWeightedEvents_etau; //!
  TH1 *h_L1CaloWeightedEvents_mutau; //!

  TH1 *h_Electron_Pt_BTrig; //!
  TH1 *h_Electron_Pt_ATrig; //!

  TH1 *h_Electron_ID_Eta_Den; //!
  TH1 *h_Electron_ID_Eta_Num; //!
  TH1 *h_Electron_ID_pT_Den; //!                                                                       
  TH1 *h_Electron_ID_pT_Num; //!
  TH1 *h_Electron_Iso_pT_Den; //!                                                                       
  TH1 *h_Electron_Iso_pT_Num; //!

  TH1 *h_Muon_Iso_pT_Den; //!                                                                       
  TH1 *h_Muon_Iso_pT_Num; //!  
  TH1 *h_Muon_ID_Eta_Den; //!                                                                       
  TH1 *h_Muon_ID_Eta_Num; //!                                                                       
  TH1 *h_Muon_ID_pT_Den; //!                                                                        
  TH1 *h_Muon_ID_pT_Num; //! 

  TH1 *h_Electron_Pt; //!
  TH1 *h_Electron_Eta; //!
  TH1 *h_Electron_Phi; //!

  TH1 *h_Electron_d0; //!
  TH1 *h_Electron_Pix; //!
  TH1 *h_Electron_SCT; //!

  TH1 *h_Electron_Reta; //!
  TH1 *h_Electron_Rphi; //!
  TH1 *h_Electron_e277; //!
  TH1 *h_Electron_Rhad1; //!
  TH1 *h_Electron_Rhad; //!
  TH1 *h_Electron_weta1; //!
  TH1 *h_Electron_weta2; //!
  TH1 *h_Electron_f1; //!
  TH1 *h_Electron_f3; //!
  TH1 *h_Electron_Eratio; //!

  TH1 *h_Electron_d0After; //!
  TH1 *h_Electron_PixAfter; //!
  TH1 *h_Electron_SCTAfter; //!

  TH1 *h_Electron_RetaAfter; //!
  TH1 *h_Electron_RphiAfter; //!
  TH1 *h_Electron_e277After; //!
  TH1 *h_Electron_Rhad1After; //!
  TH1 *h_Electron_RhadAfter; //!
  TH1 *h_Electron_weta1After; //!
  TH1 *h_Electron_weta2After; //!
  TH1 *h_Electron_f1After; //!
  TH1 *h_Electron_f3After; //!
  TH1 *h_Electron_EratioAfter; //!

  TH1 *h_Electron_PtBefore; //!
  TH1 *h_Electron_PtAfter; //!
  TH2 *h_Electron_PtIso; //!

  TH1D* h_InvMassLinear; //!

  TH1D* h_muAvg; //!
  TH1D* h_muAvg_Reweight; //!
  TH1D* h_muAct; //!                                                                                   
  TH1D* h_muAct_Reweight; //! 

  void SetTreeBranches(); //!
  void ResetVariables();  //!
  double GetMuonTrackSF(const xAOD::IParticle *p); //!
  //double GetTauRecoSF(const xAOD::IParticle *p); //!
  std::string GetFileName( std::string name ); //!
  double GetMuonRecoSF(const xAOD::IParticle *p); //!
  double GetMuonIsoSF(const xAOD::IParticle *p); //!
  double GetElectronIDSF(const xAOD::IParticle *p); //!  
  double GetElectronRecoSF(const xAOD::IParticle *p); //!  
  double GetElectronIsoSF(const xAOD::IParticle *p); //!  
  double GetMuonTriggerSF(const xAOD::IParticle *p, int rNum); //!
  double GetElectronTrigSF(const xAOD::IParticle *p); //!
  double GetElectronIDTightSF(const xAOD::IParticle *p); //!  
  double GetElectronIsoTightSF(const xAOD::IParticle *p); //!  
  double GetElectronTrigTightSF(const xAOD::IParticle *p); //!
  float CalculateDeltaPhi( float Phi1, float Phi2 ); //!

  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
public:
  // Tree *myTree; //!
  // TH1 *myHist; //!
  GoodRunsListSelectionTool *m_grl; //!

  // MuonCalibrationAndSmearing and Selection
  ORUtils::ORFlags orFlags; //!
  ORUtils::ToolBox toolBox; //!

  const bool outputPassValue = false; // overlap objects are 'true'
  const std::string inputLabel = "selected"; //!
  const std::string outputLabel = "overlaps"; //!
  const std::string bJetLabel = "isBJet";
  //const ort::inputAccessor_t selectAcc; //!
  //const ort::inputDecorator_t selectDec; //!
  //const ort::outputAccessor_t overlapAcc; //!

  ToolHandle<CP::IPileupReweightingTool> PRWToolHandle;//!          
  //ToolHandle PRWToolHandle;//!          
  TauAnalysisTools::TauOverlappingElectronLLHDecorator *TOELLHDecorator; //!
  TauAnalysisTools::TauTruthMatchingTool *T2MT; //!
  TauAnalysisTools::TauEfficiencyCorrectionsTool *TauEffTrigTool; //!
  TauAnalysisTools::TauEfficiencyCorrectionsTool *TauEffCorrTool; //!
  TauAnalysisTools::TauEfficiencyCorrectionsTool *TauEffEleOLRTool; //!
  TauAnalysisTools::TauEfficiencyCorrectionsTool *TauEffIDTool; //!
  TauAnalysisTools::TauSmearingTool *TauSmeTool; //!
  TauAnalysisTools::TauSelectionTool *TauSelTool; //!
  TauAnalysisTools::TauSelectionTool *TauSelTool_NoID; //!
  TauAnalysisTools::TauSelectionTool *TauSelTool_NoOLR; //!
  TauAnalysisTools::TauSelectionTool *TauSelTool_OnlyOLR; //! 
  CP::MuonCalibrationAndSmearingTool *m_muSmear2016; //! 
  CP::MuonCalibrationAndSmearingTool *m_muSmear2017; //! 
  CP::MuonCalibrationAndSmearingTool *m_muSmear2018; //!
  std::vector<CP::MuonCalibrationAndSmearingTool*> m_muSmear;        //!
  CP::MuonSelectionTool *m_muonSelection; //!
  AsgElectronLikelihoodTool *m_ElectronIDTool; //!
  AsgElectronLikelihoodTool *m_ElectronIDTool_Tight; //!
  AsgElectronLikelihoodTool *m_ElectronIDTool_Loose; //!
  TrigConf::xAODConfigTool *m_TrigConfigTool; //!
  Trig::TrigDecisionTool *m_TrigDecTool; //!
  ToolHandle<IJERTool> m_jetResoHand;
  Trig::MatchingTool* m_match_Tool; //!  s
  CP::IsolationSelectionTool *m_iso; //!
  CP::IsolationSelectionTool *iso_ele; //!
  CP::IsolationSelectionTool *iso_ele_track; //!
  CP::IsolationSelectionTool *iso_muon; //!
  CP::EgammaCalibrationAndSmearingTool *m_EleCalibTool; //!
  CP::PileupReweightingTool *m_Pileup; //!
  AsgElectronEfficiencyCorrectionTool *m_Reco_SF; //!
  AsgElectronEfficiencyCorrectionTool *m_ID_SF_Tight; //!
  AsgElectronEfficiencyCorrectionTool *m_ID_SF_Med; //!
  AsgElectronEfficiencyCorrectionTool *m_Trig_SF_Tight; //!
  AsgElectronEfficiencyCorrectionTool *m_Iso_SF_Tight; //!
  asg::AnaToolHandle<ORUtils::IOverlapRemovalTool> orTool; //!
  CP::MuonEfficiencyScaleFactors *m_muRecoSF; //!
  CP::MuonEfficiencyScaleFactors *m_muTTVASF; //!
  CP::MuonTriggerScaleFactors *m_muTrigSF; //!
  CP::MuonEfficiencyScaleFactors *m_muIsoSF; //!
  CP::IIsolationCorrectionTool* m_isoCorrTool; //!
  LFVUtils::MCSampleInfo *mcInfo; //!    
  LFVUtils::MCSampleOverlap *m_mcOver; //!
  CP::JetJvtEfficiency *m_jetJvtSF; //!
  JetUncertaintiesTool *m_jetUnc; //! 
  JetCleaningTool *m_jetCleaning; //! 
  JetCleaningTool *m_jetCleaning_Bad; //! 
  JERTool *m_JERTool; //! 
  JERSmearingTool *smearJetTool; //!
  ToolHandle<IJERTool> jerHandle; //!
  JetVertexTaggerTool* pjvtag; //!
  ToolHandle<IJetUpdateJvt> hjvtagup; //!
  JetCalibrationTool *m_jetCalibration; //!
  std::vector<CP::SystematicSet> m_sysList; //!
  JetForwardJvtTool *m_jetFrwJvtTool; //!
  BTaggingEfficiencyTool* m_btagSF; //!
  BTaggingSelectionTool * m_btagSelection; //!
  const xAOD::JetContainer* m_jetCont;               //!
  xAOD::JetContainer* m_jetContCopy;       //!
  xAOD::PhotonContainer *m_MetCorrPhotons;                      //!
  xAOD::MuonContainer *m_MetCorrMuons;                      //!
  //xAOD::MuonAuxContainer *m_MetCorrMuonsAux;                //!
  xAOD::ElectronContainer *m_MetCorrElectrons;              //!
  //xAOD::ElectronAuxContainer *m_MetCorrElectronsAux;        //!
  xAOD::TauJetContainer *m_MetCorrTaus;                     //!
  //xAOD::TauJetAuxContainer *m_MetCorrTausAux;               //!
  const xAOD::PhotonContainer* m_phCont;             //!
  xAOD::MissingETContainer *BuildMet;                       //!
  xAOD::MissingETAuxContainer *BuildMetAux;                 //!
  const xAOD::MissingETContainer* m_metCont;         //!
  const xAOD::MissingETAssociationMap* m_metMapCont; //!
  asg::AnaToolHandle<IMETMaker> m_metMaker;      //!
  xAOD::MissingET* MET;                                     //!
  xAOD::MissingET* softTrkMet;                                     //!
  //define TStore
  xAOD::TStore* m_store; //!
  ORUtils::ORFlags m_orFlags;                                        //!

  int numJet00; //! jet pt > 0
  int numJet20; //! jet pt > 20
  int numJet25; //! jet pt > 20 && -2.5 < eta < 2.5
  int numJet08; //! jet pt > 20 && -2.5 < eta < 2.5 && discriminat < -0.8
  int numBEvent; //!
  int numBeforeBtagging; //!
  int TotalNumberGoodMuons=0, TotalNumberGoodElectrons=0, TotalNumberGoodLooseMuons=0, TotalNumberGoodLooseElectrons=0; //!

  xAOD::MuonContainer *SelectedMuon; //!
  xAOD::MuonAuxContainer *SelectedMuonAux; //!
  xAOD::Muon* newMuon; //!
  std::string m_muTMstring; //!

  // Sys
  double Arg_isMC; //!
  std::string Arg_SampleName; //!
  std::string Arg_Sys; //
  std::string sys_name; //!
  std::vector<CP::SystematicSet>::const_iterator sysListItr; //!

  std::vector<float> Muon_Phi;       //!
  std::vector<float> Muon_Eta;       //!
  std::vector<float> LooseMuon_Phi;       //!
  std::vector<float> LooseMuon_Eta;       //!
  std::vector<float> Electron_Phi;       //!
  std::vector<float> Electron_Eta;       //!
  std::vector<float> LooseElectron_Phi;       //!
  std::vector<float> LooseElectron_Eta;       //!
  std::vector<float> Tau_Phi;       //!
  std::vector<float> Tau_Eta;       //!

  /* Tree Variables */
  int Jet_N; //!
  double PDF_X1; //!
  double PDF_X2; //!
  double PDF_Q2; //!
  int PDGID1; //!
  int PDGID2; //!
  int PDFID1; //!
  int PDFID2; //!
  int NumberEventsSample;            //!
  unsigned long long eventNumber;            //!
  int run;              //!
  int lbn;              //!
  int mcChannelNumber;              //!
  double mcweight;      //!
  double pileupweight;  //!
  double weight;        //!
  double xsec;          //!
  double bornMass;          //!
  int avgIntxCross;  //!
  int actIntxCross;  //!
  int NumberTightElectrons_tree;  //!
  int NumberTightMuons_tree;  //!
  int NumberLooseElectrons_tree;  //!
  int NumberLooseMuons_tree;  //!
  double bornDilepMass;     //!
  double bornDilepPt;       //!
  double bornDilepEta;      //!
  double bornDilepPhi;      //!
  double bornDilepDeltaPhi; //!
  std::vector<int>    Mu_isHighPt;  //!
  std::vector<int>    Mu_isID;  //!
  bool    Mu_TriggerMatched;  //!
  std::vector<int>    Mu_Quality;  //!
  std::vector<int>    Mu_Charge;       //!
  std::vector<double> Mu_pt;       //!
  std::vector<double> Mu_eta;      //!
  std::vector<double> Mu_phi;      //!
  std::vector<double> Mu_d0;       //!
  std::vector<double> Mu_d0sig;    //!
  std::vector<double> Mu_z0;       //!
  std::vector<bool>   Mu_deltaz0;  //!
  std::vector<bool>   Mu_OLR;  //!
  std::vector<int>    Mu_Iso;      //!
  std::vector<double> Mu_TrigSF;   //!
  std::vector<double> Mu_RecoSF;   //!
  std::vector<double> Mu_TrackSF;   //!
  std::vector<double> Mu_IsoSF;    //!
  std::vector<bool>   Mu_isBad;     //!
  std::vector<bool>   Ele_MuonOLR;  //!
  std::vector<int>    Ele_OQ;  //!
  std::vector<int>    Ele_ID;  //!
  std::vector<int>    Ele_ID_Tight;  //!
  std::vector<int>    Ele_ID_Loose;  //!
  bool    Ele_TriggerMatched;  //!
  bool    IsEventBjet; //!
  std::vector<int>    Ele_Charge;       //!
  std::vector<double> Ele_pt;       //!
  std::vector<double> Ele_isTight;       //!
  std::vector<double> Ele_isLoose;       //!
  std::vector<double> Ele_eta;      //!
  std::vector<double> Ele_eta_calo; //!
  std::vector<double> Ele_phi;      //!
  std::vector<double> Ele_d0;       //!
  std::vector<double> Ele_d0sig;    //!
  std::vector<bool>   Ele_deltaZ0;    //!
  std::vector<bool>   Ele_IsoCorr;    //! 
  std::vector<int>    Ele_Iso;      //!
  std::vector<int>    Ele_Iso_Loose;      //!
  std::vector<double> Ele_TrigSF;   //!
  std::vector<double> Ele_RecoSF;   //!
  std::vector<double> Ele_IDSF;     //!
  std::vector<double> Ele_IsoSF;     //!
  double Ele_L1CaloSF;     //!
  std::vector<double> Tau_JetBDT;     //!
  std::vector<double> Tau_pt;     //!
  std::vector<double> Tau_eta;     //!
  std::vector<double> Tau_phi;     //!
  std::vector<bool> Tau_ID;     //!
  std::vector<bool> Tau_ID_Med;     //!
  std::vector<bool> Tau_TruthMatched;     //!
  std::vector<int> Tau_TruthParticle;     //!
  std::vector<int> Tau_TruthJet;     //!
  std::vector<bool> Tau_isElectron;     //!
  std::vector<bool> Tau_isMuon;     //!
  std::vector<bool> Tau_isTau;     //!
  std::vector<bool> Tau_isHadTau;     //!
  std::vector<int> Tau_charge;     //!
  std::vector<int> Tau_ntracks;     //!
  std::vector<bool> Tau_OLR;     //!
  std::vector<int> TauSelector_OnlyOLR;     //!
  std::vector<int> TauSelector;     //!
  std::vector<int> TauSelector_WithOLR;     //!
  std::vector<int> TauSelector_NoID;     //!
  double MET_SumEt; //!
  double MET_Py; //!
  double MET_Px; //!
  double MET_Phi; //!
  double MET_Et; //!

  bool IsBadJetEvent; //!
  bool HLT_mu50; //!
  bool HLT_e60; //!
  bool HLT_e120; //!
  bool isBTagged; //!
  bool isEvtGood; //!
  bool isEvtClean; //!
  bool isVertexGood; //!
  bool isTrigGood; //!
  bool isNlepGood; //!
  bool isOSGood; //!
  bool isBTBGood; //!
  bool isGood; //!
  bool isKeepEvent; //!
  double KFactor; //!
  double normalisation; //!
  double dilepMass; //!
  double dilepPt; //!
  double dilepEta; //!
  double dilepPhi; //!
  double dilepDeltaPhi; //! 

  double PDF_MMHT2014; //! 
  double PDF_CT14NNLO; //! 
  double PDF_NNPDF30; //! 
  double PDF_AlphasUP; //! 
  double PDF_AlphasDOWN; //! 		

  std::string filename; //!

  std::map<int, std::map<std::string, int> > m_variations;


  // this is a standard constructor
  MyxAODAnalysis ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(MyxAODAnalysis, 1);
};

#endif
