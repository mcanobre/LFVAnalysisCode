#include "xAODRootAccess/Init.h"
#include <TSystem.h>
#include <EventLoopAlgs/NTupleSvc.h>
#include "xAODRootAccess/tools/Message.h"

/* EventLoop includes */
#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <EventLoop/DirectDriver.h>
#include <EventLoopGrid/PrunDriver.h>
#include <EventLoop/OutputStream.h> 

/* SampleHandler includes */
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "SampleHandler/DiskListLocal.h"
#include <SampleHandler/Global.h>
#include <SampleHandler/Sample.h>
#include <SampleHandler/ScanDir.h>


#include "MyAnalysis/MyxAODAnalysis.h"

int main( int argc, char* argv[] ) {

  // Take the submit directory from the input if provided:
  std::string submitDir = "submitDir";
  if( argc > 1 ) submitDir = argv[ 1 ];

  // Set up the job for xAOD access:
  xAOD::Init().ignore();

  // Construct the samples to run on:
  SH::SampleHandler sh;

  //sh.setMetaData("nc_grid_filter" );"data*")

  //SH::scanRucio( sh,"data16_13TeV.00300487.physics_Main.merge.DAOD_HIGG4D2.f705_m1606_p2950/");
  //SH::scanRucio( sh,"data16_13TeV.00300540.physics_Main.merge.DAOD_HIGG4D2.f705_m1606_p2950/");
//SH::scanRucio( sh,"data16_13TeV.00300571.physics_Main.merge.DAOD_HIGG4D2.f705_m1606_p2950/");
//SH::scanRucio( sh,"data16_13TeV.00301912.physics_Main.merge.DAOD_HIGG4D2.f709_m1611_p2950/");

//SH::scanRucio( sh,"data15_13TeV.periodD.physics_Main.PhysCont.DAOD_HIGG4D2.grp15_v01_p2950/");
//SH::scanRucio( sh,"data15_13TeV.periodE.physics_Main.PhysCont.DAOD_HIGG4D2.grp15_v01_p2950/");
//SH::scanRucio( sh,"data15_13TeV.periodF.physics_Main.PhysCont.DAOD_HIGG4D2.grp15_v01_p2950/");
//SH::scanRucio( sh,"data15_13TeV.periodG.physics_Main.PhysCont.DAOD_HIGG4D2.grp15_v01_p2950/");
//SH::scanRucio( sh,"data15_13TeV.periodH.physics_Main.PhysCont.DAOD_HIGG4D2.grp15_v01_p2950/");
//SH::scanRucio( sh,"data15_13TeV.periodJ.physics_Main.PhysCont.DAOD_HIGG4D2.grp15_v01_p2950/");
 
//SH::scanRucio( sh,"data16_13TeV.periodA.physics_Main.PhysCont.DAOD_HIGG4D2.grp16_v01_p2950/");
//SH::scanRucio( sh,"data16_13TeV.periodB.physics_Main.PhysCont.DAOD_HIGG4D2.grp16_v01_p2950/");
//SH::scanRucio( sh,"data16_13TeV.periodC.physics_Main.PhysCont.DAOD_HIGG4D2.grp16_v01_p2950/");
//SH::scanRucio( sh,"data16_13TeV.periodD.physics_Main.PhysCont.DAOD_HIGG4D2.grp16_v01_p2950/");
//SH::scanRucio( sh,"data16_13TeV.periodE.physics_Main.PhysCont.DAOD_HIGG4D2.grp16_v01_p2950/");
//SH::scanRucio( sh,"data16_13TeV.periodF.physics_Main.PhysCont.DAOD_HIGG4D2.grp16_v01_p2950/");
//SH::scanRucio( sh,"data16_13TeV.periodG.physics_Main.PhysCont.DAOD_HIGG4D2.grp16_v01_p2950/");
//SH::scanRucio( sh,"data16_13TeV.periodI.physics_Main.PhysCont.DAOD_HIGG4D2.grp16_v02_p2950/");
//SH::scanRucio( sh,"data16_13TeV.periodK.physics_Main.PhysCont.DAOD_HIGG4D2.grp16_v01_p2950/");
//SH::scanRucio( sh,"data16_13TeV.periodL.physics_Main.PhysCont.DAOD_HIGG4D2.grp16_v01_p2950/");

//data 2015
//SH::scanRucio( sh,"data15_13TeV.00276952.physics_Main.deriv.DAOD_HIGG4D2.r9264_p3083_p3750");
//SH::scanRucio( sh,"data16_13TeV.00302737.physics_Main.deriv.DAOD_HIGG4D2.r9264_p3083_p3750");

SH::scanRucio( sh,"mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_HIGG4D2.e6337_e5984_s3126_r9364_r9315_p3749");

//Backgrounds
/*SH::scanRucio( sh,"mc15_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.merge.DAOD_HIGG4D2.e3601_s2576_s2132_r7725_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.merge.DAOD_HIGG4D2.e3601_s2576_s2132_r7725_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.361108.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Ztautau.merge.DAOD_HIGG4D2.e3601_s2726_r7725_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301000.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_120M180.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301001.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_180M250.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301002.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_250M400.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301003.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_400M600.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301004.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_600M800.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301005.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_800M1000.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301006.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_1000M1250.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301007.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_1250M1500.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301008.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_1500M1750.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301009.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_1750M2000.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301010.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_2000M2250.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301011.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_2250M2500.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301012.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_2500M2750.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301013.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_2750M3000.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301014.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_3000M3500.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301015.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_3500M4000.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301016.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_4000M4500.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301017.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_4500M5000.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301018.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYee_5000M.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301020.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_120M180.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301021.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_180M250.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301022.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_250M400.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301023.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_400M600.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301024.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_600M800.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301025.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_800M1000.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301026.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_1000M1250.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301027.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_1250M1500.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301028.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_1500M1750.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301029.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_1750M2000.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301030.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_2000M2250.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301031.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_2250M2500.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301032.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_2500M2750.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301033.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_2750M3000.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301034.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_3000M3500.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301035.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_3500M4000.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301036.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_4000M4500.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301037.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_4500M5000.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301038.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYmumu_5000M.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301040.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYtautau_120M180.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301041.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYtautau_180M250.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301042.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYtautau_250M400.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301043.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYtautau_400M600.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301044.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYtautau_600M800.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301045.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYtautau_800M1000.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301046.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYtautau_1000M1250.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301047.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYtautau_1250M1500.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301048.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYtautau_1500M1750.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301049.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYtautau_1750M2000.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301050.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYtautau_2000M2250.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301051.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYtautau_2250M2500.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301052.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYtautau_2500M2750.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301053.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYtautau_2750M3000.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301054.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYtautau_3000M3500.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301055.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYtautau_3500M4000.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301056.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYtautau_4000M4500.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301057.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYtautau_4500M5000.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301058.PowhegPythia8EvtGen_AZNLOCTEQ6L1_DYtautau_5000M.merge.DAOD_HIGG4D2.e3649_s2576_s2132_r7772_r7676_p2949/");*/
 	 
/*SH::scanRucio( sh,"mc15_13TeV.306600.Sherpa_221_NNPDF30NNLO_ttbar_ee_0M150.merge.DAOD_HIGG4D2.e5452_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.306601.Sherpa_221_NNPDF30NNLO_ttbar_ee_150M250.merge.DAOD_HIGG4D2.e5452_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.306602.Sherpa_221_NNPDF30NNLO_ttbar_ee_250M500.merge.DAOD_HIGG4D2.e5452_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.306603.Sherpa_221_NNPDF30NNLO_ttbar_ee_500M1000.merge.DAOD_HIGG4D2.e5452_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.306604.Sherpa_221_NNPDF30NNLO_ttbar_ee_1000M2000.merge.DAOD_HIGG4D2.e5452_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.306605.Sherpa_221_NNPDF30NNLO_ttbar_ee_2000M3000.merge.DAOD_HIGG4D2.e5452_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.306606.Sherpa_221_NNPDF30NNLO_ttbar_ee_3000M4000.merge.DAOD_HIGG4D2.e5452_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.306607.Sherpa_221_NNPDF30NNLO_ttbar_ee_4000M5000.merge.DAOD_HIGG4D2.e5452_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.306608.Sherpa_221_NNPDF30NNLO_ttbar_ee_5000M.merge.DAOD_HIGG4D2.e5452_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.306609.Sherpa_221_NNPDF30NNLO_ttbar_mumu_0M150.merge.DAOD_HIGG4D2.e5452_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.306610.Sherpa_221_NNPDF30NNLO_ttbar_mumu_150M250.merge.DAOD_HIGG4D2.e5452_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.306611.Sherpa_221_NNPDF30NNLO_ttbar_mumu_250M500.merge.DAOD_HIGG4D2.e5452_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.306612.Sherpa_221_NNPDF30NNLO_ttbar_mumu_500M1000.merge.DAOD_HIGG4D2.e5452_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.306613.Sherpa_221_NNPDF30NNLO_ttbar_mumu_1000M2000.merge.DAOD_HIGG4D2.e5452_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.306614.Sherpa_221_NNPDF30NNLO_ttbar_mumu_2000M3000.merge.DAOD_HIGG4D2.e5452_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.306615.Sherpa_221_NNPDF30NNLO_ttbar_mumu_3000M4000.merge.DAOD_HIGG4D2.e5452_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.306616.Sherpa_221_NNPDF30NNLO_ttbar_mumu_4000M5000.merge.DAOD_HIGG4D2.e5452_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.306617.Sherpa_221_NNPDF30NNLO_ttbar_mumu_5000M.merge.DAOD_HIGG4D2.e5452_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.307479.Sherpa_221_NNPDF30NNLO_ttbar_emu_0M150.merge.DAOD_HIGG4D2.e5625_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.307480.Sherpa_221_NNPDF30NNLO_ttbar_emu_150M500.merge.DAOD_HIGG4D2.e5625_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.307481.Sherpa_221_NNPDF30NNLO_ttbar_emu_500M1000.merge.DAOD_HIGG4D2.e5625_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.307482.Sherpa_221_NNPDF30NNLO_ttbar_emu_1000M2000.merge.DAOD_HIGG4D2.e5625_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.307483.Sherpa_221_NNPDF30NNLO_ttbar_emu_2000M3000.merge.DAOD_HIGG4D2.e5625_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.307484.Sherpa_221_NNPDF30NNLO_ttbar_emu_M3000.merge.DAOD_HIGG4D2.e5625_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.307485.Sherpa_221_NNPDF30NNLO_ttbar_etau_0M150.merge.DAOD_HIGG4D2.e5625_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.307486.Sherpa_221_NNPDF30NNLO_ttbar_etau_150M500.merge.DAOD_HIGG4D2.e5625_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.307487.Sherpa_221_NNPDF30NNLO_ttbar_etau_500M1000.merge.DAOD_HIGG4D2.e5625_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.307488.Sherpa_221_NNPDF30NNLO_ttbar_etau_1000M2000.merge.DAOD_HIGG4D2.e5625_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.307489.Sherpa_221_NNPDF30NNLO_ttbar_etau_2000M3000.merge.DAOD_HIGG4D2.e5625_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.307490.Sherpa_221_NNPDF30NNLO_ttbar_etau_M3000.merge.DAOD_HIGG4D2.e5625_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.307491.Sherpa_221_NNPDF30NNLO_ttbar_mutau_0M150.merge.DAOD_HIGG4D2.e5625_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.307492.Sherpa_221_NNPDF30NNLO_ttbar_mutau_150M500.merge.DAOD_HIGG4D2.e5625_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.307493.Sherpa_221_NNPDF30NNLO_ttbar_mutau_500M1000.merge.DAOD_HIGG4D2.e5625_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.307494.Sherpa_221_NNPDF30NNLO_ttbar_mutau_1000M2000.merge.DAOD_HIGG4D2.e5625_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.307495.Sherpa_221_NNPDF30NNLO_ttbar_mutau_2000M3000.merge.DAOD_HIGG4D2.e5625_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.307496.Sherpa_221_NNPDF30NNLO_ttbar_mutau_M3000.merge.DAOD_HIGG4D2.e5625_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.307497.Sherpa_221_NNPDF30NNLO_ttbar_tautau_0M150.merge.DAOD_HIGG4D2.e5625_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.307498.Sherpa_221_NNPDF30NNLO_ttbar_tautau_150M500.merge.DAOD_HIGG4D2.e5625_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.307499.Sherpa_221_NNPDF30NNLO_ttbar_tautau_500M1000.merge.DAOD_HIGG4D2.e5625_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.307500.Sherpa_221_NNPDF30NNLO_ttbar_tautau_1000M2000.merge.DAOD_HIGG4D2.e5625_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.307501.Sherpa_221_NNPDF30NNLO_ttbar_tautau_2000M3000.merge.DAOD_HIGG4D2.e5625_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.307502.Sherpa_221_NNPDF30NNLO_ttbar_tautau_M3000.merge.DAOD_HIGG4D2.e5625_s2726_r7772_r7676_p3017/");*/

/*SH::scanRucio( sh,"mc15_13TeV.361063.Sherpa_CT10_llll.merge.DAOD_HIGG4D2.e3836_s2608_s2183_r7725_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.361064.Sherpa_CT10_lllvSFMinus.merge.DAOD_HIGG4D2.e3836_s2608_s2183_r7725_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.361065.Sherpa_CT10_lllvOFMinus.merge.DAOD_HIGG4D2.e3836_s2608_s2183_r7725_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.361066.Sherpa_CT10_lllvSFPlus.merge.DAOD_HIGG4D2.e3836_s2608_s2183_r7725_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.361067.Sherpa_CT10_lllvOFPlus.merge.DAOD_HIGG4D2.e3836_s2608_s2183_r7725_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.361091.Sherpa_CT10_WplvWmqq_SHv21_improved.merge.DAOD_HIGG4D2.e4607_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.361092.Sherpa_CT10_WpqqWmlv_SHv21_improved.merge.DAOD_HIGG4D2.e4607_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.361093.Sherpa_CT10_WlvZqq_SHv21_improved.merge.DAOD_HIGG4D2.e4607_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.361094.Sherpa_CT10_WqqZll_SHv21_improved.merge.DAOD_HIGG4D2.e4607_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.361095.Sherpa_CT10_WqqZvv_SHv21_improved.merge.DAOD_HIGG4D2.e4607_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.361096.Sherpa_CT10_ZqqZll_SHv21_improved.merge.DAOD_HIGG4D2.e4607_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.361097.Sherpa_CT10_ZqqZvv_SHv21_improved.merge.DAOD_HIGG4D2.e4607_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303014.Sherpa_CT10_VV_evev_50M150.merge.DAOD_HIGG4D2.e4188_s2608_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.303015.Sherpa_CT10_VV_evev_150M500.merge.DAOD_HIGG4D2.e4188_s2608_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.303016.Sherpa_CT10_VV_evev_500M1000.merge.DAOD_HIGG4D2.e4188_s2608_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.303017.Sherpa_CT10_VV_evev_1000M2000.merge.DAOD_HIGG4D2.e4188_s2608_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.303018.Sherpa_CT10_VV_evev_2000M3000.merge.DAOD_HIGG4D2.e4188_s2608_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.303019.Sherpa_CT10_VV_evev_3000M4000.merge.DAOD_HIGG4D2.e4188_s2608_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.303020.Sherpa_CT10_VV_evev_4000M5000.merge.DAOD_HIGG4D2.e4188_s2608_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.303021.Sherpa_CT10_VV_evev_5000M.merge.DAOD_HIGG4D2.e4188_s2608_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.303046.Sherpa_CT10_VV_muvmuv_50M150.merge.DAOD_HIGG4D2.e4188_s2608_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.303047.Sherpa_CT10_VV_muvmuv_150M500.merge.DAOD_HIGG4D2.e4188_s2608_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.303048.Sherpa_CT10_VV_muvmuv_500M1000.merge.DAOD_HIGG4D2.e4188_s2608_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.303049.Sherpa_CT10_VV_muvmuv_1000M2000.merge.DAOD_HIGG4D2.e4188_s2608_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.303050.Sherpa_CT10_VV_muvmuv_2000M3000.merge.DAOD_HIGG4D2.e4188_s2608_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.303051.Sherpa_CT10_VV_muvmuv_3000M4000.merge.DAOD_HIGG4D2.e4188_s2608_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.303052.Sherpa_CT10_VV_muvmuv_4000M5000.merge.DAOD_HIGG4D2.e4188_s2608_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.303053.Sherpa_CT10_VV_muvmuv_5000M.merge.DAOD_HIGG4D2.e4188_s2608_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.303488.Sherpa_CT10_VV_evmuv_0M150.merge.DAOD_HIGG4D2.e4318_s2608_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303489.Sherpa_CT10_VV_evmuv_150M500.merge.DAOD_HIGG4D2.e4318_s2608_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303490.Sherpa_CT10_VV_evmuv_500M1000.merge.DAOD_HIGG4D2.e4318_s2608_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303491.Sherpa_CT10_VV_evmuv_1000M2000.merge.DAOD_HIGG4D2.e4318_s2608_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303492.Sherpa_CT10_VV_evmuv_2000M3000.merge.DAOD_HIGG4D2.e4318_s2608_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303493.Sherpa_CT10_VV_evmuv_3000M4000.merge.DAOD_HIGG4D2.e4318_s2608_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303494.Sherpa_CT10_VV_evmuv_4000M5000.merge.DAOD_HIGG4D2.e4318_s2608_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303495.Sherpa_CT10_VV_evmuv_M5000.merge.DAOD_HIGG4D2.e4318_s2608_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.304933.Sherpa_CT10_VV_evtauv_0M150.merge.DAOD_HIGG4D2.e4780_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.304934.Sherpa_CT10_VV_evtauv_150M500.merge.DAOD_HIGG4D2.e4780_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.304935.Sherpa_CT10_VV_evtauv_500M1000.merge.DAOD_HIGG4D2.e4780_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.304936.Sherpa_CT10_VV_evtauv_1000M2000.merge.DAOD_HIGG4D2.e4780_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.304937.Sherpa_CT10_VV_evtauv_2000M3000.merge.DAOD_HIGG4D2.e4780_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.304938.Sherpa_CT10_VV_evtauv_3000M4000.merge.DAOD_HIGG4D2.e4780_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.304939.Sherpa_CT10_VV_evtauv_4000M5000.merge.DAOD_HIGG4D2.e4780_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.304940.Sherpa_CT10_VV_evtauv_M5000.merge.DAOD_HIGG4D2.e4780_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.304941.Sherpa_CT10_VV_muvtauv_0M150.merge.DAOD_HIGG4D2.e4780_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.304942.Sherpa_CT10_VV_muvtauv_150M500.merge.DAOD_HIGG4D2.e4780_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.304943.Sherpa_CT10_VV_muvtauv_500M1000.merge.DAOD_HIGG4D2.e4780_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.304944.Sherpa_CT10_VV_muvtauv_1000M2000.merge.DAOD_HIGG4D2.e4780_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.304945.Sherpa_CT10_VV_muvtauv_2000M3000.merge.DAOD_HIGG4D2.e4780_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.304946.Sherpa_CT10_VV_muvtauv_3000M4000.merge.DAOD_HIGG4D2.e4780_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.304947.Sherpa_CT10_VV_muvtauv_4000M5000.merge.DAOD_HIGG4D2.e4780_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.304948.Sherpa_CT10_VV_muvtauv_M5000.merge.DAOD_HIGG4D2.e4780_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.304949.Sherpa_CT10_VV_tauvtauv_0M150.merge.DAOD_HIGG4D2.e4780_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.304950.Sherpa_CT10_VV_tauvtauv_150M500.merge.DAOD_HIGG4D2.e4780_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.304951.Sherpa_CT10_VV_tauvtauv_500M1000.merge.DAOD_HIGG4D2.e4780_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.304952.Sherpa_CT10_VV_tauvtauv_1000M2000.merge.DAOD_HIGG4D2.e4780_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.304953.Sherpa_CT10_VV_tauvtauv_2000M3000.merge.DAOD_HIGG4D2.e4780_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.304954.Sherpa_CT10_VV_tauvtauv_3000M4000.merge.DAOD_HIGG4D2.e4780_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.304955.Sherpa_CT10_VV_tauvtauv_4000M5000.merge.DAOD_HIGG4D2.e4780_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.304956.Sherpa_CT10_VV_tauvtauv_M5000.merge.DAOD_HIGG4D2.e4780_s2726_r7772_r7676_p2949/");

SH::scanRucio( sh,"mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.merge.DAOD_HIGG4D2.e3698_s2608_s2183_r7725_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.410013.PowhegPythiaEvtGen_P2012_Wt_inclusive_top.merge.DAOD_HIGG4D2.e3753_s2608_s2183_r7725_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.410014.PowhegPythiaEvtGen_P2012_Wt_inclusive_antitop.merge.DAOD_HIGG4D2.e3753_s2608_s2183_r7725_r7676_p2949/");*/

/*SH::scanRucio( sh,"mc15_13TeV.364156.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV0_70_CVetoBVeto.merge.DAOD_HIGG4D2.e5340_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.364157.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV0_70_CFilterBVeto.merge.DAOD_HIGG4D2.e5340_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.364158.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV0_70_BFilter.merge.DAOD_HIGG4D2.e5340_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.364159.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV70_140_CVetoBVeto.merge.DAOD_HIGG4D2.e5340_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.364160.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV70_140_CFilterBVeto.merge.DAOD_HIGG4D2.e5340_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.364161.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV70_140_BFilter.merge.DAOD_HIGG4D2.e5340_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.364162.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV140_280_CVetoBVeto.merge.DAOD_HIGG4D2.e5340_s2726_r7772_r7676_p3017/"); 
SH::scanRucio( sh,"mc15_13TeV.364163.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV140_280_CFilterBVeto.merge.DAOD_HIGG4D2.e5340_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.364164.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV140_280_BFilter.merge.DAOD_HIGG4D2.e5340_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.364165.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV280_500_CVetoBVeto.merge.DAOD_HIGG4D2.e5340_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.364166.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV280_500_CFilterBVeto.merge.DAOD_HIGG4D2.e5340_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.364167.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV280_500_BFilter.merge.DAOD_HIGG4D2.e5340_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.364168.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV500_1000.merge.DAOD_HIGG4D2.e5340_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.364169.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV1000_E_CMS.merge.DAOD_HIGG4D2.e5340_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.364170.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV0_70_CVetoBVeto.merge.DAOD_HIGG4D2.e5340_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.364171.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV0_70_CFilterBVeto.merge.DAOD_HIGG4D2.e5340_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.364172.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV0_70_BFilter.merge.DAOD_HIGG4D2.e5340_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.364173.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV70_140_CVetoBVeto.merge.DAOD_HIGG4D2.e5340_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.364174.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV70_140_CFilterBVeto.merge.DAOD_HIGG4D2.e5340_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.364175.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV70_140_BFilter.merge.DAOD_HIGG4D2.e5340_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.364176.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV140_280_CVetoBVeto.merge.DAOD_HIGG4D2.e5340_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.364177.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV140_280_CFilterBVeto.merge.DAOD_HIGG4D2.e5340_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.364178.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV140_280_BFilter.merge.DAOD_HIGG4D2.e5340_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.364179.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV280_500_CVetoBVeto.merge.DAOD_HIGG4D2.e5340_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.364180.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV280_500_CFilterBVeto.merge.DAOD_HIGG4D2.e5340_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.364181.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV280_500_BFilter.merge.DAOD_HIGG4D2.e5340_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.364182.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV500_1000.merge.DAOD_HIGG4D2.e5340_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.364183.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV1000_E_CMS.merge.DAOD_HIGG4D2.e5340_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.364184.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV0_70_CVetoBVeto.merge.DAOD_HIGG4D2.e5340_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.364185.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV0_70_CFilterBVeto.merge.DAOD_HIGG4D2.e5340_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.364186.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV0_70_BFilter.merge.DAOD_HIGG4D2.e5340_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.364187.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV70_140_CVetoBVeto.merge.DAOD_HIGG4D2.e5340_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.364188.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV70_140_CFilterBVeto.merge.DAOD_HIGG4D2.e5340_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.364189.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV70_140_BFilter.merge.DAOD_HIGG4D2.e5340_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.364190.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV140_280_CVetoBVeto.merge.DAOD_HIGG4D2.e5340_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.364191.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV140_280_CFilterBVeto.merge.DAOD_HIGG4D2.e5340_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.364192.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV140_280_BFilter.merge.DAOD_HIGG4D2.e5340_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.364193.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV280_500_CVetoBVeto.merge.DAOD_HIGG4D2.e5340_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.364194.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV280_500_CFilterBVeto.merge.DAOD_HIGG4D2.e5340_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.364195.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV280_500_BFilter.merge.DAOD_HIGG4D2.e5340_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.364196.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV500_1000.merge.DAOD_HIGG4D2.e5340_s2726_r7772_r7676_p3017/");
SH::scanRucio( sh,"mc15_13TeV.364197.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV1000_E_CMS.merge.DAOD_HIGG4D2.e5340_s2726_r7772_r7676_p3017/"); */

//Signal samples
/*SH::scanRucio( sh,"mc15_13TeV.301954.Pythia8EvtGen_A14NNPDF23LO_Zprime0500emu.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301955.Pythia8EvtGen_A14NNPDF23LO_Zprime0600emu.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301956.Pythia8EvtGen_A14NNPDF23LO_Zprime0700emu.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301957.Pythia8EvtGen_A14NNPDF23LO_Zprime0800emu.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301958.Pythia8EvtGen_A14NNPDF23LO_Zprime0900emu.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301959.Pythia8EvtGen_A14NNPDF23LO_Zprime1000emu.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301960.Pythia8EvtGen_A14NNPDF23LO_Zprime1100emu.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301961.Pythia8EvtGen_A14NNPDF23LO_Zprime1200emu.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301962.Pythia8EvtGen_A14NNPDF23LO_Zprime1300emu.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301963.Pythia8EvtGen_A14NNPDF23LO_Zprime1400emu.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301964.Pythia8EvtGen_A14NNPDF23LO_Zprime1500emu.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301965.Pythia8EvtGen_A14NNPDF23LO_Zprime1600emu.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301966.Pythia8EvtGen_A14NNPDF23LO_Zprime1700emu.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301967.Pythia8EvtGen_A14NNPDF23LO_Zprime1800emu.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301968.Pythia8EvtGen_A14NNPDF23LO_Zprime1900emu.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301969.Pythia8EvtGen_A14NNPDF23LO_Zprime2000emu.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301970.Pythia8EvtGen_A14NNPDF23LO_Zprime2200emu.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301971.Pythia8EvtGen_A14NNPDF23LO_Zprime2400emu.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301972.Pythia8EvtGen_A14NNPDF23LO_Zprime2600emu.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301973.Pythia8EvtGen_A14NNPDF23LO_Zprime2800emu.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301974.Pythia8EvtGen_A14NNPDF23LO_Zprime3000emu.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301975.Pythia8EvtGen_A14NNPDF23LO_Zprime3500emu.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301976.Pythia8EvtGen_A14NNPDF23LO_Zprime4000emu.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301977.Pythia8EvtGen_A14NNPDF23LO_Zprime4500emu.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301978.Pythia8EvtGen_A14NNPDF23LO_Zprime5000emu.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");

SH::scanRucio( sh,"mc15_13TeV.301979.Pythia8EvtGen_A14NNPDF23LO_Zprime0500etau.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301980.Pythia8EvtGen_A14NNPDF23LO_Zprime0600etau.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301981.Pythia8EvtGen_A14NNPDF23LO_Zprime0700etau.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301982.Pythia8EvtGen_A14NNPDF23LO_Zprime0800etau.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301983.Pythia8EvtGen_A14NNPDF23LO_Zprime0900etau.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301984.Pythia8EvtGen_A14NNPDF23LO_Zprime1000etau.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301985.Pythia8EvtGen_A14NNPDF23LO_Zprime1100etau.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301986.Pythia8EvtGen_A14NNPDF23LO_Zprime1200etau.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301987.Pythia8EvtGen_A14NNPDF23LO_Zprime1300etau.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301988.Pythia8EvtGen_A14NNPDF23LO_Zprime1400etau.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301989.Pythia8EvtGen_A14NNPDF23LO_Zprime1500etau.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301990.Pythia8EvtGen_A14NNPDF23LO_Zprime1600etau.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301991.Pythia8EvtGen_A14NNPDF23LO_Zprime1700etau.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301992.Pythia8EvtGen_A14NNPDF23LO_Zprime1800etau.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301993.Pythia8EvtGen_A14NNPDF23LO_Zprime1900etau.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301994.Pythia8EvtGen_A14NNPDF23LO_Zprime2000etau.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301995.Pythia8EvtGen_A14NNPDF23LO_Zprime2200etau.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301996.Pythia8EvtGen_A14NNPDF23LO_Zprime2400etau.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301997.Pythia8EvtGen_A14NNPDF23LO_Zprime2600etau.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301998.Pythia8EvtGen_A14NNPDF23LO_Zprime2800etau.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.301999.Pythia8EvtGen_A14NNPDF23LO_Zprime3000etau.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.302000.Pythia8EvtGen_A14NNPDF23LO_Zprime3500etau.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.302001.Pythia8EvtGen_A14NNPDF23LO_Zprime4000etau.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.302002.Pythia8EvtGen_A14NNPDF23LO_Zprime4500etau.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.302003.Pythia8EvtGen_A14NNPDF23LO_Zprime5000etau.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");

SH::scanRucio( sh,"mc15_13TeV.302004.Pythia8EvtGen_A14NNPDF23LO_Zprime0500mutau.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.302005.Pythia8EvtGen_A14NNPDF23LO_Zprime0600mutau.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.302006.Pythia8EvtGen_A14NNPDF23LO_Zprime0700mutau.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.302007.Pythia8EvtGen_A14NNPDF23LO_Zprime0800mutau.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.302008.Pythia8EvtGen_A14NNPDF23LO_Zprime0900mutau.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.302009.Pythia8EvtGen_A14NNPDF23LO_Zprime1000mutau.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.302010.Pythia8EvtGen_A14NNPDF23LO_Zprime1100mutau.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.302011.Pythia8EvtGen_A14NNPDF23LO_Zprime1200mutau.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.302012.Pythia8EvtGen_A14NNPDF23LO_Zprime1300mutau.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.302013.Pythia8EvtGen_A14NNPDF23LO_Zprime1400mutau.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.302014.Pythia8EvtGen_A14NNPDF23LO_Zprime1500mutau.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.302015.Pythia8EvtGen_A14NNPDF23LO_Zprime1600mutau.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.302016.Pythia8EvtGen_A14NNPDF23LO_Zprime1700mutau.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.302017.Pythia8EvtGen_A14NNPDF23LO_Zprime1800mutau.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.302018.Pythia8EvtGen_A14NNPDF23LO_Zprime1900mutau.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.302019.Pythia8EvtGen_A14NNPDF23LO_Zprime2000mutau.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.302020.Pythia8EvtGen_A14NNPDF23LO_Zprime2200mutau.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.302021.Pythia8EvtGen_A14NNPDF23LO_Zprime2400mutau.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.302022.Pythia8EvtGen_A14NNPDF23LO_Zprime2600mutau.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.302023.Pythia8EvtGen_A14NNPDF23LO_Zprime2800mutau.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.302024.Pythia8EvtGen_A14NNPDF23LO_Zprime3000mutau.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.302025.Pythia8EvtGen_A14NNPDF23LO_Zprime3500mutau.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.302026.Pythia8EvtGen_A14NNPDF23LO_Zprime4000mutau.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.302027.Pythia8EvtGen_A14NNPDF23LO_Zprime4500mutau.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.302028.Pythia8EvtGen_A14NNPDF23LO_Zprime5000mutau.merge.DAOD_HIGG4D2.e3922_a766_a821_r7676_p2949/");

SH::scanRucio( sh,"mc15_13TeV.303610.QBHPythia8EvtGen_A14CTEQ6L1_QBH_emu_n1_Mth01000.merge.DAOD_HIGG4D2.e4327_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303611.QBHPythia8EvtGen_A14CTEQ6L1_QBH_emu_n1_Mth01500.merge.DAOD_HIGG4D2.e4327_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303612.QBHPythia8EvtGen_A14CTEQ6L1_QBH_emu_n1_Mth02000.merge.DAOD_HIGG4D2.e4327_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303613.QBHPythia8EvtGen_A14CTEQ6L1_QBH_emu_n1_Mth02500.merge.DAOD_HIGG4D2.e4327_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303614.QBHPythia8EvtGen_A14CTEQ6L1_QBH_emu_n1_Mth03000.merge.DAOD_HIGG4D2.e4327_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303615.QBHPythia8EvtGen_A14CTEQ6L1_QBH_emu_n1_Mth03500.merge.DAOD_HIGG4D2.e4327_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303616.QBHPythia8EvtGen_A14CTEQ6L1_QBH_emu_n1_Mth04000.merge.DAOD_HIGG4D2.e4327_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303617.QBHPythia8EvtGen_A14CTEQ6L1_QBH_emu_n1_Mth04500.merge.DAOD_HIGG4D2.e4327_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303618.QBHPythia8EvtGen_A14CTEQ6L1_QBH_emu_n1_Mth05000.merge.DAOD_HIGG4D2.e4327_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303619.QBHPythia8EvtGen_A14CTEQ6L1_QBH_emu_n1_Mth05500.merge.DAOD_HIGG4D2.e4327_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303620.QBHPythia8EvtGen_A14CTEQ6L1_QBH_emu_n1_Mth06000.merge.DAOD_HIGG4D2.e4327_a766_a821_r7676_p2949/");

SH::scanRucio( sh,"mc15_13TeV.303577.QBHPythia8EvtGen_A14CTEQ6L1_QBH_emu_n6_Mth03000.merge.DAOD_HIGG4D2.e4327_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303578.QBHPythia8EvtGen_A14CTEQ6L1_QBH_emu_n6_Mth03500.merge.DAOD_HIGG4D2.e4327_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303579.QBHPythia8EvtGen_A14CTEQ6L1_QBH_emu_n6_Mth04000.merge.DAOD_HIGG4D2.e4327_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303580.QBHPythia8EvtGen_A14CTEQ6L1_QBH_emu_n6_Mth04500.merge.DAOD_HIGG4D2.e4327_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303581.QBHPythia8EvtGen_A14CTEQ6L1_QBH_emu_n6_Mth05000.merge.DAOD_HIGG4D2.e4327_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303582.QBHPythia8EvtGen_A14CTEQ6L1_QBH_emu_n6_Mth05500.merge.DAOD_HIGG4D2.e4327_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303583.QBHPythia8EvtGen_A14CTEQ6L1_QBH_emu_n6_Mth06000.merge.DAOD_HIGG4D2.e4327_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303584.QBHPythia8EvtGen_A14CTEQ6L1_QBH_emu_n6_Mth06500.merge.DAOD_HIGG4D2.e4327_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303585.QBHPythia8EvtGen_A14CTEQ6L1_QBH_emu_n6_Mth07000.merge.DAOD_HIGG4D2.e4327_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303586.QBHPythia8EvtGen_A14CTEQ6L1_QBH_emu_n6_Mth07500.merge.DAOD_HIGG4D2.e4327_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303587.QBHPythia8EvtGen_A14CTEQ6L1_QBH_emu_n6_Mth08000.merge.DAOD_HIGG4D2.e4327_a766_a821_r7676_p2949/");

SH::scanRucio( sh,"mc15_13TeV.303621.QBHPythia8EvtGen_A14CTEQ6L1_QBH_etau_n1_Mth01000.merge.DAOD_HIGG4D2.e4327_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303622.QBHPythia8EvtGen_A14CTEQ6L1_QBH_etau_n1_Mth01500.merge.DAOD_HIGG4D2.e4327_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303623.QBHPythia8EvtGen_A14CTEQ6L1_QBH_etau_n1_Mth02000.merge.DAOD_HIGG4D2.e4327_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303624.QBHPythia8EvtGen_A14CTEQ6L1_QBH_etau_n1_Mth02500.merge.DAOD_HIGG4D2.e4327_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303625.QBHPythia8EvtGen_A14CTEQ6L1_QBH_etau_n1_Mth03000.merge.DAOD_HIGG4D2.e4327_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303626.QBHPythia8EvtGen_A14CTEQ6L1_QBH_etau_n1_Mth03500.merge.DAOD_HIGG4D2.e4327_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303627.QBHPythia8EvtGen_A14CTEQ6L1_QBH_etau_n1_Mth04000.merge.DAOD_HIGG4D2.e4327_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303628.QBHPythia8EvtGen_A14CTEQ6L1_QBH_etau_n1_Mth04500.merge.DAOD_HIGG4D2.e4327_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303629.QBHPythia8EvtGen_A14CTEQ6L1_QBH_etau_n1_Mth05000.merge.DAOD_HIGG4D2.e4327_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303630.QBHPythia8EvtGen_A14CTEQ6L1_QBH_etau_n1_Mth05500.merge.DAOD_HIGG4D2.e4327_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303631.QBHPythia8EvtGen_A14CTEQ6L1_QBH_etau_n1_Mth06000.merge.DAOD_HIGG4D2.e4327_s2726_r7772_r7676_p2949/");

SH::scanRucio( sh,"mc15_13TeV.303588.QBHPythia8EvtGen_A14CTEQ6L1_QBH_etau_n6_Mth03000.merge.DAOD_HIGG4D2.e4327_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303589.QBHPythia8EvtGen_A14CTEQ6L1_QBH_etau_n6_Mth03500.merge.DAOD_HIGG4D2.e4327_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303590.QBHPythia8EvtGen_A14CTEQ6L1_QBH_etau_n6_Mth04000.merge.DAOD_HIGG4D2.e4327_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303591.QBHPythia8EvtGen_A14CTEQ6L1_QBH_etau_n6_Mth04500.merge.DAOD_HIGG4D2.e4327_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303592.QBHPythia8EvtGen_A14CTEQ6L1_QBH_etau_n6_Mth05000.merge.DAOD_HIGG4D2.e4327_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303593.QBHPythia8EvtGen_A14CTEQ6L1_QBH_etau_n6_Mth05500.merge.DAOD_HIGG4D2.e4327_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303594.QBHPythia8EvtGen_A14CTEQ6L1_QBH_etau_n6_Mth06000.merge.DAOD_HIGG4D2.e4327_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303595.QBHPythia8EvtGen_A14CTEQ6L1_QBH_etau_n6_Mth06500.merge.DAOD_HIGG4D2.e4327_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303596.QBHPythia8EvtGen_A14CTEQ6L1_QBH_etau_n6_Mth07000.merge.DAOD_HIGG4D2.e4327_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303597.QBHPythia8EvtGen_A14CTEQ6L1_QBH_etau_n6_Mth07500.merge.DAOD_HIGG4D2.e4327_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303598.QBHPythia8EvtGen_A14CTEQ6L1_QBH_etau_n6_Mth08000.merge.DAOD_HIGG4D2.e4327_s2726_r7772_r7676_p2949/");

SH::scanRucio( sh,"mc15_13TeV.303632.QBHPythia8EvtGen_A14CTEQ6L1_QBH_mutau_n1_Mth01000.merge.DAOD_HIGG4D2.e4327_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303633.QBHPythia8EvtGen_A14CTEQ6L1_QBH_mutau_n1_Mth01500.merge.DAOD_HIGG4D2.e4327_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303634.QBHPythia8EvtGen_A14CTEQ6L1_QBH_mutau_n1_Mth02000.merge.DAOD_HIGG4D2.e4327_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303635.QBHPythia8EvtGen_A14CTEQ6L1_QBH_mutau_n1_Mth02500.merge.DAOD_HIGG4D2.e4327_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303636.QBHPythia8EvtGen_A14CTEQ6L1_QBH_mutau_n1_Mth03000.merge.DAOD_HIGG4D2.e4327_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303637.QBHPythia8EvtGen_A14CTEQ6L1_QBH_mutau_n1_Mth03500.merge.DAOD_HIGG4D2.e4327_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303638.QBHPythia8EvtGen_A14CTEQ6L1_QBH_mutau_n1_Mth04000.merge.DAOD_HIGG4D2.e4327_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303639.QBHPythia8EvtGen_A14CTEQ6L1_QBH_mutau_n1_Mth04500.merge.DAOD_HIGG4D2.e4327_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303640.QBHPythia8EvtGen_A14CTEQ6L1_QBH_mutau_n1_Mth05000.merge.DAOD_HIGG4D2.e4327_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303641.QBHPythia8EvtGen_A14CTEQ6L1_QBH_mutau_n1_Mth05500.merge.DAOD_HIGG4D2.e4327_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303642.QBHPythia8EvtGen_A14CTEQ6L1_QBH_mutau_n1_Mth06000.merge.DAOD_HIGG4D2.e4327_s2726_r7772_r7676_p2949/");

SH::scanRucio( sh,"mc15_13TeV.303599.QBHPythia8EvtGen_A14CTEQ6L1_QBH_mutau_n6_Mth03000.merge.DAOD_HIGG4D2.e4327_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303600.QBHPythia8EvtGen_A14CTEQ6L1_QBH_mutau_n6_Mth03500.merge.DAOD_HIGG4D2.e4327_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303601.QBHPythia8EvtGen_A14CTEQ6L1_QBH_mutau_n6_Mth04000.merge.DAOD_HIGG4D2.e4327_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303602.QBHPythia8EvtGen_A14CTEQ6L1_QBH_mutau_n6_Mth04500.merge.DAOD_HIGG4D2.e4327_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303603.QBHPythia8EvtGen_A14CTEQ6L1_QBH_mutau_n6_Mth05000.merge.DAOD_HIGG4D2.e4327_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303604.QBHPythia8EvtGen_A14CTEQ6L1_QBH_mutau_n6_Mth05500.merge.DAOD_HIGG4D2.e4327_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303605.QBHPythia8EvtGen_A14CTEQ6L1_QBH_mutau_n6_Mth06000.merge.DAOD_HIGG4D2.e4327_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303606.QBHPythia8EvtGen_A14CTEQ6L1_QBH_mutau_n6_Mth06500.merge.DAOD_HIGG4D2.e4327_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303607.QBHPythia8EvtGen_A14CTEQ6L1_QBH_mutau_n6_Mth07000.merge.DAOD_HIGG4D2.e4327_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303608.QBHPythia8EvtGen_A14CTEQ6L1_QBH_mutau_n6_Mth07500.merge.DAOD_HIGG4D2.e4327_s2726_r7772_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.303609.QBHPythia8EvtGen_A14CTEQ6L1_QBH_mutau_n6_Mth08000.merge.DAOD_HIGG4D2.e4327_s2726_r7772_r7676_p2949/");

SH::scanRucio( sh,"mc15_13TeV.402970.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_emu_m500.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.402971.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_emu_m600.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.402972.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_emu_m700.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.402973.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_emu_m800.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.402974.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_emu_m900.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.402975.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_emu_m1000.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.402976.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_emu_m1100.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.402977.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_emu_m1200.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.402978.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_emu_m1300.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.402979.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_emu_m1400.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.402980.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_emu_m1500.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.402981.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_emu_m1600.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.402982.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_emu_m1700.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.402983.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_emu_m1800.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.402984.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_emu_m1900.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.402985.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_emu_m2000.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.402986.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_emu_m2200.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.402987.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_emu_m2400.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.402988.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_emu_m2600.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.402989.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_emu_m2800.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.402990.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_emu_m3000.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.402991.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_emu_m3500.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.402992.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_emu_m4000.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.402993.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_emu_m4500.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.402994.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_emu_m5000.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");

SH::scanRucio( sh,"mc15_13TeV.402995.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_etau_tauhad_m500.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.402996.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_etau_tauhad_m600.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.402997.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_etau_tauhad_m700.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.402998.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_etau_tauhad_m800.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.402999.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_etau_tauhad_m900.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.403000.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_etau_tauhad_m1000.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.403001.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_etau_tauhad_m1100.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.403002.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_etau_tauhad_m1200.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.403003.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_etau_tauhad_m1300.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.403004.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_etau_tauhad_m1400.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.403005.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_etau_tauhad_m1500.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.403006.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_etau_tauhad_m1600.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.403007.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_etau_tauhad_m1700.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.403008.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_etau_tauhad_m1800.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.403009.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_etau_tauhad_m1900.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.403010.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_etau_tauhad_m2000.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.403011.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_etau_tauhad_m2200.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.403012.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_etau_tauhad_m2400.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.403013.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_etau_tauhad_m2600.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.403014.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_etau_tauhad_m2800.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.403015.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_etau_tauhad_m3000.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.403016.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_etau_tauhad_m3500.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.403017.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_etau_tauhad_m4000.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.403018.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_etau_tauhad_m4500.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.403019.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_etau_tauhad_m5000.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");

SH::scanRucio( sh,"mc15_13TeV.403020.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_mutau_tauhad_m500.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.403021.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_mutau_tauhad_m600.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.403022.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_mutau_tauhad_m700.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.403023.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_mutau_tauhad_m800.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.403024.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_mutau_tauhad_m900.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.403025.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_mutau_tauhad_m1000.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.403026.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_mutau_tauhad_m1100.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.403027.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_mutau_tauhad_m1200.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.403028.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_mutau_tauhad_m1300.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.403029.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_mutau_tauhad_m1400.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.403030.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_mutau_tauhad_m1500.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.403031.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_mutau_tauhad_m1600.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.403032.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_mutau_tauhad_m1700.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.403033.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_mutau_tauhad_m1800.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.403034.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_mutau_tauhad_m1900.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.403035.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_mutau_tauhad_m2000.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.403036.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_mutau_tauhad_m2200.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.403037.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_mutau_tauhad_m2400.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.403038.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_mutau_tauhad_m2600.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.403039.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_mutau_tauhad_m2800.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.403040.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_mutau_tauhad_m3000.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.403041.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_mutau_tauhad_m3500.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.403042.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_mutau_tauhad_m4000.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.403043.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_mutau_tauhad_m4500.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");
SH::scanRucio( sh,"mc15_13TeV.403044.MadGraphPythia8EvtGen_A14NNPDF23LO_SVT_mutau_tauhad_m5000.merge.DAOD_HIGG4D2.e5466_a766_a821_r7676_p2949/");*/


  // Set the name of the input TTree. It's always "CollectionTree"
  // for xAOD files.
  sh.setMetaString( "nc_tree", "CollectionTree" ); 
 
  // Print what we found:
  sh.print();

  // Create an EventLoop job:
  EL::Job job;
  job.sampleHandler( sh );
  //job.options()->setDouble (EL::Job::optMaxEvents, 500);

  // Choose sys variation appropriately
  /*if (Arg_Sys=="Nom") sys_name = "";
  else if (Arg_Sys=="Ele_Res_Up") sys_name = "EG_RESOLUTION_ALL__1up";
  else if (Arg_Sys=="Ele_Res_Down") sys_name = "EG_RESOLUTION_ALL__1down";
  else if (Arg_Sys=="Ele_Scale_Up") sys_name = "EG_SCALE_ALL__1up";
  else if (Arg_Sys=="Ele_Scale_Down") sys_name = "EG_SCALE_ALL__1down";
  else if (Arg_Sys=="Ele_Eff_IDUp") sys_name = "EL_EFF_ID_TotalCorrUncertainty__1up";
  else if (Arg_Sys=="Ele_Eff_IDDown") sys_name = "EL_EFF_ID_TotalCorrUncertainty__1down";
  else if (Arg_Sys=="Ele_Eff_IsoUp") sys_name = "EL_EFF_Iso_TotalCorrUncertainty__1up";
  else if (Arg_Sys=="Ele_Eff_IsoDown") sys_name = "EL_EFF_Iso_TotalCorrUncertainty__1down";
  else if (Arg_Sys=="Ele_Eff_TrigUp") sys_name = "EL_EFF_Trigger_TotalCorrUncertainty__1up";
  else if (Arg_Sys=="Ele_Eff_TrigDown") sys_name = "EL_EFF_Trigger_TotalCorrUncertainty__1down";
  else if (Arg_Sys=="Ele_Eff_RecoUp") sys_name = "EL_EFF_Reco_TotalCorrUncertainty__1up";
  else if (Arg_Sys=="Ele_Eff_RecoDown") sys_name = "EL_EFF_Reco_TotalCorrUncertainty__1down";
  else if (Arg_Sys=="Mu_ResID_Up") sys_name = "MUONS_ID__1up";
  else if (Arg_Sys=="Mu_ResID_Down") sys_name = "MUONS_ID__1down";
  else if (Arg_Sys=="Mu_ResMS_Up") sys_name = "MUONS_MS__1up";
  else if (Arg_Sys=="Mu_ResMS_Down") sys_name = "MUONS_MS__1down";
  else if (Arg_Sys=="Mu_Scale_Up") sys_name = "MUONS_SCALE__1up";
  else if (Arg_Sys=="Mu_Scale_Down") sys_name = "MUONS_SCALE__1down";
  else if (Arg_Sys=="Mu_Eff_IDUpSys") sys_name = "MUON_EFF_SYS__1up";
  else if (Arg_Sys=="Mu_Eff_IDDownSys") sys_name = "MUON_EFF_SYS__1down";
  else if (Arg_Sys=="Mu_Eff_IDUpStat") sys_name = "MUON_EFF_STAT__1up";
  else if (Arg_Sys=="Mu_Eff_IDDownStat") sys_name = "MUON_EFF_STAT__1down";
  else if (Arg_Sys=="Mu_Eff_IsoUpStat") sys_name = "MUON_ISO_STAT__1up";
  else if (Arg_Sys=="Mu_Eff_IsoDownStat") sys_name = "MUON_ISO_STAT__1down";
  else if (Arg_Sys=="Mu_Eff_IsoUpSys") sys_name = "MUON_ISO_SYS__1up";
  else if (Arg_Sys=="Mu_Eff_IsoDownSys") sys_name = "MUON_ISO_SYS__1down";
  else if (Arg_Sys=="Mu_Eff_TrigUpSys") sys_name = "MUON_EFF_TrigSystUncertainty__1up";
  else if (Arg_Sys=="Mu_Eff_TrigDownSys") sys_name = "MUON_EFF_TrigSystUncertainty__1down";
  else if (Arg_Sys=="Mu_Eff_TrigUpStat") sys_name = "MUON_EFF_TrigStatUncertainty__1up";
  else if (Arg_Sys=="Mu_Eff_TrigDownStat") sys_name = "MUON_EFF_TrigStatUncertainty__1down";*/

  // Set Job Options
  std::string Sys = "Nom";
  bool Corrections = true;
  bool FastSim = false;
  bool Debug = false;
  bool isData = false;
  std::string MCType = "mc16a";

  // Add our analysis to the job:
  MyxAODAnalysis* alg = new MyxAODAnalysis();
  alg->Arg_Sys = Sys;
  alg->ApplyCorrections = Corrections;
  alg->m_FastSim = FastSim;
  alg->m_debug = Debug;
  alg->m_isData = isData;
  alg->m_MCtype = MCType;
  job.algsAdd( alg );

  // Run the job using the local/direct driver:
  //EL::DirectDriver driver;
  EL::PrunDriver driver;
  driver.options()->setString("nc_outputSampleName", "user.mcanobre.LFVTestMCRet_"+Sys+"_%in:name[2]%");
  driver.options()->setDouble(EL::Job::optGridMaxNFilesPerJob, 1);
  //driver.options()->setString(EL::Job::optGridNFilesPerJob, 10);
  //driver.options()->setDouble(EL::Job::optGridExpress,1);
  //driver.options()->setDouble(EL::Job::optGridMergeOutput, false);

  driver.submitOnly( job, submitDir );
  
  return 0;
}
