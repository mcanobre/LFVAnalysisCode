#include "xAODRootAccess/Init.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include "SampleHandler/DiskListLocal.h"
#include <TSystem.h>

#include "MyAnalysis/MyxAODAnalysis.h"

int main( int argc, char* argv[] ) {

  // Take the submit directory from the input if provided:
  std::string submitDir = "submitDir";
  if( argc > 1 ) submitDir = argv[ 1 ];

  // Set up the job for xAOD access:
  xAOD::Init().ignore();

  // Construct the samples to run on:
  SH::SampleHandler sh;

  // use SampleHandler to scan all of the subdirectories of a directory for particular MC single file:
  const char* inputFilePath = gSystem->ExpandPathName ("/afs/cern.ch/work/m/mcanobre/LFV/Marc/run"); 
  //const char* inputFilePath = gSystem->ExpandPathName ("/afs/cern.ch/work/m/mcanobre/DYtautau");
 
  //const char* inputFilePath = gSystem->ExpandPathName ("/afs/cern.ch/user/m/mcanobre/public");
  //SH::ScanDir().sampleDepth(1).samplePattern("DAOD_HIGG4D2.07561196._000002.pool.root.1").scan(sh, inputFilePath);

  SH::DiskListLocal list (inputFilePath);                                                                                                                                                                       
  //SH::scanDir (sh, list, "DAOD*.pool.root.1"); // specifying one particular file for testing       
  SH::scanDir (sh, list, "*.root.1");

  // Set the name of the input TTree. It's always "CollectionTree"
  // for xAOD files.
  sh.setMetaString( "nc_tree", "CollectionTree" );

  // Print what we found:
  sh.print();

  // Create an EventLoop job:
  EL::Job job;
  job.sampleHandler( sh );
  //job.options()->setDouble (EL::Job::optMaxEvents, 500);

  // Choose sys variation appropriately
  /*if (Arg_Sys=="Nom") sys_name = "";
  else if (Arg_Sys=="Ele_Res_Up") sys_name = "EG_RESOLUTION_ALL__1up";
  else if (Arg_Sys=="Ele_Res_Down") sys_name = "EG_RESOLUTION_ALL__1down";
  else if (Arg_Sys=="Ele_Scale_Up") sys_name = "EG_SCALE_ALL__1up";
  else if (Arg_Sys=="Ele_Scale_Down") sys_name = "EG_SCALE_ALL__1down";
  else if (Arg_Sys=="Ele_Eff_IDUp") sys_name = "EL_EFF_ID_TotalCorrUncertainty__1up";
  else if (Arg_Sys=="Ele_Eff_IDDown") sys_name = "EL_EFF_ID_TotalCorrUncertainty__1down";
  else if (Arg_Sys=="Ele_Eff_IsoUp") sys_name = "EL_EFF_Iso_TotalCorrUncertainty__1up";
  else if (Arg_Sys=="Ele_Eff_IsoDown") sys_name = "EL_EFF_Iso_TotalCorrUncertainty__1down";
  else if (Arg_Sys=="Ele_Eff_TrigUp") sys_name = "EL_EFF_Trigger_TotalCorrUncertainty__1up";
  else if (Arg_Sys=="Ele_Eff_TrigDown") sys_name = "EL_EFF_Trigger_TotalCorrUncertainty__1down";
  else if (Arg_Sys=="Ele_Eff_RecoUp") sys_name = "EL_EFF_Reco_TotalCorrUncertainty__1up";
  else if (Arg_Sys=="Ele_Eff_RecoDown") sys_name = "EL_EFF_Reco_TotalCorrUncertainty__1down";
  else if (Arg_Sys=="Mu_ResID_Up") sys_name = "MUONS_ID__1up";
  else if (Arg_Sys=="Mu_ResID_Down") sys_name = "MUONS_ID__1down";
  else if (Arg_Sys=="Mu_ResMS_Up") sys_name = "MUONS_MS__1up";
  else if (Arg_Sys=="Mu_ResMS_Down") sys_name = "MUONS_MS__1down";
  else if (Arg_Sys=="Mu_Scale_Up") sys_name = "MUONS_SCALE__1up";
  else if (Arg_Sys=="Mu_Scale_Down") sys_name = "MUONS_SCALE__1down";
  else if (Arg_Sys=="Mu_Eff_IDUpSys") sys_name = "MUON_EFF_SYS__1up";
  else if (Arg_Sys=="Mu_Eff_IDDownSys") sys_name = "MUON_EFF_SYS__1down";
  else if (Arg_Sys=="Mu_Eff_IDUpStat") sys_name = "MUON_EFF_STAT__1up";
  else if (Arg_Sys=="Mu_Eff_IDDownStat") sys_name = "MUON_EFF_STAT__1down";
  else if (Arg_Sys=="Mu_Eff_IsoUpStat") sys_name = "MUON_ISO_STAT__1up";
  else if (Arg_Sys=="Mu_Eff_IsoDownStat") sys_name = "MUON_ISO_STAT__1down";
  else if (Arg_Sys=="Mu_Eff_IsoUpSys") sys_name = "MUON_ISO_SYS__1up";
  else if (Arg_Sys=="Mu_Eff_IsoDownSys") sys_name = "MUON_ISO_SYS__1down";
  else if (Arg_Sys=="Mu_Eff_TrigUpSys") sys_name = "MUON_EFF_TrigSystUncertainty__1up";
  else if (Arg_Sys=="Mu_Eff_TrigDownSys") sys_name = "MUON_EFF_TrigSystUncertainty__1down";
  else if (Arg_Sys=="Mu_Eff_TrigUpStat") sys_name = "MUON_EFF_TrigStatUncertainty__1up";
  else if (Arg_Sys=="Mu_Eff_TrigDownStat") sys_name = "MUON_EFF_TrigStatUncertainty__1down";
Systematic: TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1down
Systematic: TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1up
Systematic: TAUS_TRUEHADTAU_EFF_JETID_TOTAL__1down
Systematic: TAUS_TRUEHADTAU_EFF_JETID_TOTAL__1up
Systematic: TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down
Systematic: TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up
Systematic: TAUS_TRUEHADTAU_SME_TES_TOTAL__1down
Systematic: TAUS_TRUEHADTAU_SME_TES_TOTAL__1up

*/

  // Set Job Options
  std::string Sys = "Nom";
  bool Corrections = true;
  bool FastSim = false;
  bool Debug = true;
  bool isData = false;
  std::string MCType = "mc16a";

  // Add our analysis to the job:
  MyxAODAnalysis* alg = new MyxAODAnalysis();
  alg->Arg_Sys = Sys;
  alg->ApplyCorrections = Corrections;
  alg->m_FastSim = FastSim;
  alg->m_debug = Debug;
  alg->m_isData = isData;
  alg->m_MCtype = MCType;
  job.algsAdd( alg );

  // Run the job using the local/direct driver:
  EL::DirectDriver driver;
  driver.submit( job, submitDir );

  return 0;
}
