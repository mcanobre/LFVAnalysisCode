#include <MyAnalysis/MyxAODAnalysis.h>

using namespace std;

// this is needed to distribute the algorithm to the workers
ClassImp(MyxAODAnalysis)

EL::StatusCode MyxAODAnalysis :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  event = wk()->xaodEvent();

  // Reset output tree variables
  ResetVariables();

  // Define Global variables
  int mc_channel_number = 0.0;
  double BornMass = 0.0;
  const xAOD::TruthEventContainer* xTruthEventContainer = NULL;
  const xAOD::TruthParticleContainer* xTruthParticleContainer = NULL;
  double GeV = 0.001;
  TLorentzVector Electron(1,1,1,1), Muon(1,1,1,1), LooseElectron(1,1,1,1), LooseMuon(1,1,1,1), Propagator(1,1,1,1), BornPropagator(1,1,1,1), BornElectron(1,1,1,1), BornMuon(1,1,1,1), Tau(1,1,1,1), BornTau(1,1,1,1), Neutrino(1,1,1,1);
  TLorentzVector BornElectron1(1,1,1,1), BornMuon1(1,1,1,1), BornTau1(1,1,1,1), BornElectron2(1,1,1,1), BornMuon2(1,1,1,1), BornTau2(1,1,1,1);
  const double MuonPDGMass = 105.658367e-3; 
  double EventWeight = 1.0;
  double ChargeMuon = 0; double ChargeElectron = 0; double ChargeTau = 0;
  double Luminosity = 1.0; // Luminosity in fb^-1 to normalise to if isMC==true
  int NumberGoodMuons = 0, NumberGoodElectrons = 0, NumberGoodLooseMuons = 0, NumberGoodLooseElectrons = 0, NumberGoodTaus = 0, NumberGoodLooseTaus = 0;
  double nVtx = 0;
  bool primVtx = false;
  bool BadJetEvent = false;
  const double pi = TMath::Pi();
  double Electron_ID_SF = 1.0, Electron_Reco_SF = 1.0, Electron_Trig_SF = 1.0, Electron_Iso_SF = 1.0,  Muon_Eff_SF = 1.0, Muon_Track_SF = 1.0, Muon_TrigEff_SF = 1.0, Muon_IsoEff_SF = 1.0;
  double el_n = 0.0, mu_n = 0.0;
  float xSec = 0.0, events = 0.0;
  float PileUpWeight = 1.0, MCWeight = 1.0; double bornDeltaPhi=0.0;
  double Dphi = 0.0;
  m_electronMuon = false; 
  m_electronTau = false; 
  m_muonTau = false; 
  Muon_Phi.clear();      
  Muon_Eta.clear();  
  LooseMuon_Phi.clear();      
  LooseMuon_Eta.clear();      
  Electron_Phi.clear();      
  Electron_Eta.clear();      
  LooseElectron_Phi.clear();      
  LooseElectron_Eta.clear();      
  Tau_Phi.clear();       
  Tau_Eta.clear();       

  //Count Events and print info every 1000 events
  //if( (m_eventCounter % 100) == 0 ) Info("execute()", "Event number = %i", m_eventCounter );
  m_eventCounter++;

  // Event Information and GRL

  // Retrieve Event information
  const xAOD::EventInfo* eventInfo = 0;
  CHECK("execute()",event->retrieve( eventInfo, "EventInfo" ));

  /////// Retrieve containers ////////////////////////////
 
  // Vertex Container Retrieval
  const xAOD::VertexContainer* vxContainer = 0;
  CHECK("execute()",event->retrieve( vxContainer, "PrimaryVertices" ));

  // Reco Electron Container Retrieval
  const xAOD::ElectronContainer* els = 0;
  CHECK("execute()",event->retrieve( els, "Electrons" ));

  // get muon container of interest
  const xAOD::MuonContainer* muons = 0;
  CHECK("execute()",event->retrieve( muons, "Muons" ));

  // Get Jet container
  const xAOD::JetContainer* jets = 0;
  CHECK("execute()",event->retrieve( jets, "AntiKt4EMTopoJets" ));
 
  // Get Tau container
  const xAOD::TauJetContainer* taus = 0;
  CHECK("execute()",event->retrieve( taus, "TauJets" ));

  const xAOD::JetContainer* m_thjetCont = 0;
  if( !m_isData ){CHECK("execute()", event->retrieve( m_thjetCont, "AntiKt4TruthJets" ));} 

  // Missing Energy
  m_metCont = 0;
  CHECK( "execute()", event->retrieve( m_metCont, "MET_Core_AntiKt4EMTopo" ) );
  m_metMapCont = 0;
  CHECK( "execute()", event->retrieve( m_metMapCont, "METAssoc_AntiKt4EMTopo" ) );

  m_MetCorrMuons           = new xAOD::MuonContainer(SG::VIEW_ELEMENTS);  
  m_MetCorrElectrons       = new xAOD::ElectronContainer(SG::VIEW_ELEMENTS);
  m_MetCorrTaus            = new xAOD::TauJetContainer(SG::VIEW_ELEMENTS);
  m_MetCorrPhotons         = new xAOD::PhotonContainer(SG::VIEW_ELEMENTS);
  
  // Photon container
  m_phCont = 0;
  CHECK( "execute()", event->retrieve( m_phCont, "Photons" ) );

  if( m_debug ) std::cout<<"About to apply OLR "<<std::endl;
 
  // check if the event is data or MC (many tools are applied either to data or MC)
  bool isMC = false;
  // check if the event is MC
  if( eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ){ 
    isMC = true; // can do something with this later
     //AthAnalysisHelper::retrieveMetadata("/Generation/Parameters","HepMCWeightNames",m_variations_364156);
  }   
   
  int rNum = 0.0;

  el_n = els->size(); mu_n = muons->size();  int tau_n = taus->size();   int NumJets = jets->size();  int NumPhotons = m_phCont->size();

  h_CutFlow->Fill(0);
  h_TauCutFlow->Fill(0.0, tau_n );
  h_MuonCutFlow->Fill(0.0, mu_n );
  h_ElectronCutFlow->Fill(0.0, el_n );
  h_PhotonCutFlow->Fill(0.0, NumPhotons );
  h_JetCutFlow->Fill(0.0, NumJets );

  //if data check if event passes GRL
  if(!isMC){ // it's data! 
    if( ! m_grl->passRunLB( eventInfo->runNumber(), eventInfo->lumiBlock() ) ){
      return EL::StatusCode::SUCCESS;  // go to next event
     }
  } // end if not MC
  else{
    mc_channel_number = eventInfo->mcChannelNumber();
    events  = mcInfo->GetNumberEventsSample( mc_channel_number  );   
    xSec = mcInfo->GetSampleCrossSection( mc_channel_number ); 
    if( m_debug ) std::cout<<"Cross section "<<xSec<<" number generated events "<<events<<" normalisation "<<EventWeight<<std::endl;
    //m_Pileup->apply(*eventInfo,true);
    //PileUpWeight = eventInfo->auxdata< Float_t >( "PileupWeight" );
    //rNum = m_Pileup->getRandomRunNumber(*eventInfo,true);
    eventInfo->auxdecor<unsigned int>("RandomRunNumber") = m_Pileup->getRandomRunNumber( *eventInfo, true );
    if( m_debug ) Info( "PileupWeight()" , "PUWeight = %f ", PileUpWeight );
    if( m_debug ) std::cout<<"Generated Run number "<<rNum<<std::endl;
    MCWeight = eventInfo->mcEventWeights().at(0); 
    if( m_debug ) Info( "MCWeight()" , "MCWeight = %f ", MCWeight );
    EventWeight*=(PileUpWeight*MCWeight);
    // Retrieve Truth container                                                           
    CHECK("execute()", event->retrieve( xTruthEventContainer, "TruthEvents" ) );
    CHECK("execute()", event->retrieve( xTruthParticleContainer, "TruthParticles" ) );
    EventWeight*=mcInfo->GetSampleNormalization( mc_channel_number,Luminosity, xTruthParticleContainer );
    normalisation = mcInfo->GetSampleNormalization( mc_channel_number,Luminosity, xTruthParticleContainer );
    if( (eventInfo->mcChannelNumber()>=301560 && eventInfo->mcChannelNumber()<=301579) || (eventInfo->mcChannelNumber()>=301540 && eventInfo->mcChannelNumber()<=301559) || (eventInfo->mcChannelNumber()>=303437 && eventInfo->mcChannelNumber()<=303455) || (eventInfo->mcChannelNumber()>=301000 && eventInfo->mcChannelNumber()<=301018) || (eventInfo->mcChannelNumber()>=301020 && eventInfo->mcChannelNumber()<=301038) || (eventInfo->mcChannelNumber()>=301040 && eventInfo->mcChannelNumber()<=301058) || (eventInfo->mcChannelNumber()>=361106 && eventInfo->mcChannelNumber()<=361108) ){ 
    KFactor = mcInfo->GetDYkFactor( eventInfo->mcChannelNumber(), xTruthParticleContainer ); EventWeight*=KFactor; 
    } // end of if statement 
    if( (eventInfo->mcChannelNumber()>=402970 && eventInfo->mcChannelNumber()<=403044) ){ KFactor = mcInfo->GetRPVkFactor( eventInfo->mcChannelNumber() ); EventWeight*=KFactor; }
  } // end of if Monte Carlo

    if( m_debug ) Info( "Status" , "Got past the truth retrieval" );

  // Average Int per Bunc Crossing
  avgIntxCross = eventInfo->averageInteractionsPerCrossing();
  actIntxCross = eventInfo->actualInteractionsPerCrossing();

  h_muAvg->Fill( avgIntxCross );
  h_muAct->Fill( actIntxCross );
  h_muAvg_Reweight->Fill( avgIntxCross, PileUpWeight );
  h_muAct_Reweight->Fill( actIntxCross, PileUpWeight );

  /////////// Systematics settings ////////////////////////////

  if (Arg_Sys=="Nom") sys_name = "";
  else if (Arg_Sys=="Ele_Res_Up") sys_name = "EG_RESOLUTION_ALL__1up";
  else if (Arg_Sys=="Ele_Res_Down") sys_name = "EG_RESOLUTION_ALL__1down";
  else if (Arg_Sys=="Ele_Scale_Up") sys_name = "EG_SCALE_ALL__1up";
  else if (Arg_Sys=="Ele_Scale_Down") sys_name = "EG_SCALE_ALL__1down";
  else if (Arg_Sys=="Ele_Eff_IDUp") sys_name = "EL_EFF_ID_TotalCorrUncertainty__1up";
  else if (Arg_Sys=="Ele_Eff_IDDown") sys_name = "EL_EFF_ID_TotalCorrUncertainty__1down";
  else if (Arg_Sys=="Ele_Eff_IsoUp") sys_name = "EL_EFF_Iso_TotalCorrUncertainty__1up";
  else if (Arg_Sys=="Ele_Eff_IsoDown") sys_name = "EL_EFF_Iso_TotalCorrUncertainty__1down";
  else if (Arg_Sys=="Ele_Eff_TrigUp") sys_name = "EL_EFF_Trigger_TotalCorrUncertainty__1up";
  else if (Arg_Sys=="Ele_Eff_TrigDown") sys_name = "EL_EFF_Trigger_TotalCorrUncertainty__1down";
  else if (Arg_Sys=="Ele_Eff_RecoUp") sys_name = "EL_EFF_Reco_TotalCorrUncertainty__1up";
  else if (Arg_Sys=="Ele_Eff_RecoDown") sys_name = "EL_EFF_Reco_TotalCorrUncertainty__1down";
  else if (Arg_Sys=="Mu_ResID_Up") sys_name = "MUONS_ID__1up";
  else if (Arg_Sys=="Mu_ResID_Down") sys_name = "MUONS_ID__1down";
  else if (Arg_Sys=="Mu_ResMS_Up") sys_name = "MUONS_MS__1up";
  else if (Arg_Sys=="Mu_ResMS_Down") sys_name = "MUONS_MS__1down";
  else if (Arg_Sys=="Mu_Scale_Up") sys_name = "MUONS_SCALE__1up";
  else if (Arg_Sys=="Mu_Scale_Down") sys_name = "MUONS_SCALE__1down";
  else if (Arg_Sys=="Mu_Eff_IDUpSys") sys_name = "MUON_EFF_SYS__1up";
  else if (Arg_Sys=="Mu_Eff_IDDownSys") sys_name = "MUON_EFF_SYS__1down";
  else if (Arg_Sys=="Mu_Eff_IDUpStat") sys_name = "MUON_EFF_STAT__1up";
  else if (Arg_Sys=="Mu_Eff_IDDownStat") sys_name = "MUON_EFF_STAT__1down";
  else if (Arg_Sys=="Mu_Eff_IsoUpStat") sys_name = "MUON_ISO_STAT__1up";
  else if (Arg_Sys=="Mu_Eff_IsoDownStat") sys_name = "MUON_ISO_STAT__1down";
  else if (Arg_Sys=="Mu_Eff_IsoUpSys") sys_name = "MUON_ISO_SYS__1up";
  else if (Arg_Sys=="Mu_Eff_IsoDownSys") sys_name = "MUON_ISO_SYS__1down";
  else if (Arg_Sys=="Mu_Eff_TrigUpSys") sys_name = "MUON_EFF_TrigSystUncertainty__1up";
  else if (Arg_Sys=="Mu_Eff_TrigDownSys") sys_name = "MUON_EFF_TrigSystUncertainty__1down";
  else if (Arg_Sys=="Mu_Eff_TrigUpStat") sys_name = "MUON_EFF_TrigStatUncertainty__1up";
  else if (Arg_Sys=="Mu_Eff_TrigDownStat") sys_name = "MUON_EFF_TrigStatUncertainty__1down";
  else if (Arg_Sys=="Tau_Sme_TesTotUp") sys_name = "TAUS_TRUEHADTAU_SME_TES_TOTAL__1up";
  else if (Arg_Sys=="Tau_Sme_TesTotDown") sys_name = "TAUS_TRUEHADTAU_SME_TES_TOTAL__1down"; 
  else if (Arg_Sys=="Tau_ELEOR_Up") sys_name = "TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1up";
  else if (Arg_Sys=="Tau_ELEOR_Down") sys_name = "TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1down";
  else if (Arg_Sys=="Tau_JETID_Up") sys_name = "TAUS_TRUEHADTAU_EFF_JETID_TOTAL__1up";
  else if (Arg_Sys=="Tau_JETID_Down") sys_name = "TAUS_TRUEHADTAU_EFF_JETID_TOTAL__1down";
  else if (Arg_Sys=="Tau_EFF_Up") sys_name = "TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up";
  else if (Arg_Sys=="Tau_EFF_Down") sys_name = "TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down";
  else{
    return EL::StatusCode::FAILURE;
  }

  //------------
  // TRUTH PARTICLES
  //------------

  float x1 = 0.0, x2 = 0.0, q = 0.0;
  int Quark1Flavour = 0, Quark2Flavour = 0, pdfID1=0, pdfID2=0;

  bool SignalSample = false;
  xAOD::TruthEventContainer::const_iterator itr;
  //for (itr = xTruthEventContainer->begin(); itr!=xTruthEventContainer->end(); ++itr){
  if( isMC && mc_channel_number != 410000.0 ){

  	isKeepEvent = m_mcOver->KeepEvent( xTruthParticleContainer, eventInfo->mcChannelNumber() );
  	//if( !isKeepEvent ) return EL::StatusCode::SUCCESS;

  	for( auto itr: *xTruthEventContainer ){

         	(itr)->pdfInfoParameter(pdfID1, xAOD::TruthEvent::PDFID1);
         	(itr)->pdfInfoParameter(pdfID2, xAOD::TruthEvent::PDFID2);
	 	(itr)->pdfInfoParameter(Quark1Flavour, xAOD::TruthEvent::PDGID1);
	 	(itr)->pdfInfoParameter(Quark2Flavour, xAOD::TruthEvent::PDGID2);
	 	(itr)->pdfInfoParameter(x1, xAOD::TruthEvent::X1);
	 	(itr)->pdfInfoParameter(x2, xAOD::TruthEvent::X2);
	 	(itr)->pdfInfoParameter(q, xAOD::TruthEvent::Q);

		int index_PDF_MMHT2014 = m_variations[mc_channel_number]["MUR1_MUF1_PDF25300"];
		int index_PDF_CT14NNLO = m_variations[mc_channel_number]["MUR1_MUF1_PDF13000"];
		int index_PDF_NNPDF30 = m_variations[mc_channel_number]["MUR1_MUF1_PDF261000"];
		int index_PDF_AlphasUP = m_variations[mc_channel_number]["MUR1_MUF1_PDF269000"];
		int index_PDF_AlphasDOWN = m_variations[mc_channel_number]["MUR1_MUF1_PDF270000"]; 

		PDF_MMHT2014 = itr->weights()[index_PDF_MMHT2014];
		PDF_CT14NNLO = itr->weights()[index_PDF_CT14NNLO];
		PDF_NNPDF30 = itr->weights()[index_PDF_NNPDF30];
		PDF_AlphasUP = itr->weights()[index_PDF_AlphasUP];
		PDF_AlphasDOWN = itr->weights()[index_PDF_AlphasDOWN];		


		if( m_debug ) std::cout<<"PDF Weights : "<<PDF_MMHT2014<<" "<<PDF_CT14NNLO<<"  "<<PDF_NNPDF30<<"  "<<PDF_AlphasUP<<"   "<<PDF_AlphasDOWN<<std::endl;

  	} // end of event loop
  } // end of isMC

  // Tree variables
  PDF_X1=x1;
  PDF_X2=x2;
  PDF_Q2=q*q;
  PDFID1=pdfID1;
  PDFID2=pdfID2;
  PDGID1=Quark1Flavour;
  PDGID2=Quark2Flavour;
  //if(isMC){ T2MT->setTruthParticleContainer(xTruthParticleContainer);}

  bool FoundLepton1 = false, FoundLepton2 = false; TLorentzVector LeptonEvent(0,0,0,0),JetW(0,0,0,0);
  if( isMC && ( ( mc_channel_number >= 402970.0 && mc_channel_number < 402995.0 ) || ( mc_channel_number >= 303437.0 && mc_channel_number < 303457.0 ) || (mc_channel_number == 361106.0 ) || (mc_channel_number == 361107.0 ) || (mc_channel_number == 361108.0 ) || (mc_channel_number >= 301540.0 && mc_channel_number < 3019580.0) || (mc_channel_number > 301941.0 && mc_channel_number < 301979.0) || (mc_channel_number > 303576.0 && mc_channel_number < 303621.0) || (mc_channel_number > 303479.0 && mc_channel_number < 303496.0) || (mc_channel_number >= 361100.0 && mc_channel_number <= 361105.0) ||  (mc_channel_number >= 364156.0 && mc_channel_number <= 364197.0) ) ){
  
	    SignalSample=true;

	    //std::cout<<"*********************New Event********************************"<<std::endl;
            for( auto truth = xTruthParticleContainer->begin(); truth!=xTruthParticleContainer->end(); ++truth ){


			//std::cout<<"Particle "<<(*truth)->pdgId()<<" with status "<<(*truth)->status()<<std::endl;

		       // Electrons	
		       if( fabs( (*truth)->pdgId() ) == 11 && ( (*truth)->status() == 23 || (*truth)->status() == 3 || (*truth)->status() == 1 ) ){ 

					if( (*truth)->pdgId() == 11  )FoundLepton1=true;
					if( (*truth)->pdgId() == -11 )FoundLepton2=true;
					BornElectron = (*truth)->p4(); 
					if( BornElectron.Pt() > BornElectron1.Pt() ){ BornElectron2=BornElectron1; BornElectron1=BornElectron; FoundLepton1=true;  } 
					else if( BornElectron.Pt() > BornElectron2.Pt() ){ BornElectron2=BornElectron; }
			} // end of if statement

		       // Muons
		       if( fabs( (*truth)->pdgId() ) == 13 && ( (*truth)->status() == 23 || (*truth)->status() == 3 || (*truth)->status() == 1 ) ){ 

					if( (*truth)->pdgId() == 13  )FoundLepton1=true;
					if( (*truth)->pdgId() == -13 )FoundLepton2=true;
					BornMuon = (*truth)->p4(); 
					if( BornMuon.Pt() > BornMuon1.Pt() ){ BornMuon2=BornMuon1; BornMuon1=BornMuon;  } 
					else if( BornMuon.Pt() > BornMuon2.Pt() ){ BornMuon2=BornMuon; }
			} // end of if statement

		       // Taus
		       if( fabs( (*truth)->pdgId() ) == 15 && ( (*truth)->status() == 23 || (*truth)->status() == 3 || (*truth)->status() == 1 ) ){ 

					if( (*truth)->pdgId() == 15  )FoundLepton1=true;
					if( (*truth)->pdgId() == -15 )FoundLepton2=true;
					BornTau = (*truth)->p4(); 
					if( BornTau.Pt() > BornTau1.Pt() ){ BornTau2=BornTau1; BornTau1=BornTau;  } 
					else if( BornTau.Pt() > BornTau2.Pt() ){ BornTau2=BornTau; }
			} // end of if statement
		
		       if( fabs( (*truth)->pdgId() ) >= 1 && fabs( (*truth)->pdgId() ) <= 6 ){ if( (*truth)->pt() > JetW.Pt() )  JetW = (*truth)->p4(); }
		       if( (*truth)->m() > LeptonEvent.Mag()  ){ LeptonEvent = (*truth)->p4();}

	     } // end of truth event loop

             if( (mc_channel_number > 303479.0 && mc_channel_number < 303496.0) )  BornPropagator = BornElectron1 + BornMuon1;
    	     if( ( mc_channel_number >= 301954.0 && mc_channel_number < 301979.0 ) || ( mc_channel_number >= 402970.0 && mc_channel_number < 402995.0 ) ) BornPropagator = BornElectron1 + BornMuon1;
    	     if( mc_channel_number >= 301979.0 && mc_channel_number < 302004.0 ) BornPropagator = BornElectron1 + BornTau1;
    	     if( mc_channel_number >= 302004.0 && mc_channel_number < 302029.0 ) BornPropagator = BornTau1 + BornMuon1;
	     if( mc_channel_number == 361106.0 || ( mc_channel_number >= 301540.0 && mc_channel_number < 301560.0 ) ){ BornPropagator = BornElectron1 + BornElectron2; if( !FoundLepton1 || !FoundLepton2 ){ return EL::StatusCode::SUCCESS; }  }
	     if( mc_channel_number == 361107.0 || ( mc_channel_number >= 301560.0 && mc_channel_number < 301580.0 ) ){ BornPropagator = BornMuon1 + BornMuon2; if( !FoundLepton1 || !FoundLepton2 ){ return EL::StatusCode::SUCCESS; } }
	     if( mc_channel_number == 361108.0 || ( mc_channel_number >= 303437.0 && mc_channel_number < 303457.0 ) ){ BornPropagator = BornTau1 + BornTau2; if( !FoundLepton1 || !FoundLepton2 ){ return EL::StatusCode::SUCCESS; } }
             if( mc_channel_number >= 364156.0 && mc_channel_number <= 364197.0 ){ BornPropagator = LeptonEvent;}

  } // end of truth loop for overlap removal

  bornDeltaPhi = BornElectron.Phi() - BornMuon.Phi();
  if( fabs(bornDeltaPhi)>pi ) {
    if( bornDeltaPhi>0 ){
      bornDeltaPhi = 2*pi-bornDeltaPhi;
    }
    else{
      bornDeltaPhi = 2*pi+bornDeltaPhi;
    }
  } // end of BornDeltaPhi calculation 

  h_BornMass->Fill( GeV*BornPropagator.Mag(), EventWeight );
  h_BornMassLinear->Fill( GeV*BornPropagator.Mag(), EventWeight );
  if( m_debug ) std::cout<<"Born Mass "<<GeV*BornPropagator.Mag()<<std::endl;

  if(m_debug) std::cout<<"eventNumber "<<(int)eventInfo->eventNumber()<<std::endl;

  // Fill Tree variables
  eventNumber = eventInfo->eventNumber();
  run   = eventInfo->runNumber();
  lbn   = eventInfo->lumiBlock();
  mcChannelNumber = mc_channel_number;
  if( isMC ) mcweight     = eventInfo->mcEventWeights().at(0);
  pileupweight = PileUpWeight;
  xsec         = xSec;
  bornMass  = BornPropagator.Mag()*GeV;
  bornDilepPt = BornPropagator.Pt();       
  bornDilepEta = BornPropagator.Eta();      
  bornDilepPhi = BornPropagator.Phi();      
  bornDilepDeltaPhi = bornDeltaPhi;
  NumberEventsSample = events;    

  if( run<=311481 )     m_year = DataYear::Year2016;
  else if( run<=340453) m_year = DataYear::Year2017;
  else                        m_year = DataYear::Year2018;

    //////////////////////////////////////////////////////////////////                                                                                                                                               
  // Event pre-selection                                                                                                                                                                                           
  /////////////////////////////////////////////////////////////////   

  //------------------------------------------------------------                                                                                                                                                  
  // Apply event cleaning to remove events due to                                                                                                                                                                 
  // problematic regions of the detector, and incomplete events.                                                                                                                                                  
  // Apply to data.                                                                                                                                                                                               
  //------------------------------------------------------------  
  // reject event if:                                                                                                                                                                                              
  if(!isMC){
    if( (eventInfo->errorState(xAOD::EventInfo::LAr)==xAOD::EventInfo::Error ) || (eventInfo->errorState(xAOD::EventInfo::Tile)==xAOD::EventInfo::Error ) || (eventInfo->isEventFlagBitSet(xAOD::EventInfo::Core, 18) )  )  return EL::StatusCode::SUCCESS; // go to the next event                                                                                                                                                 
  } // end if the event is data                                                                                                                                                                                    
  m_numCleanEvents++;
  h_CutFlow->Fill(1);
  h_MuonCutFlow->Fill(1.0, mu_n );
  h_ElectronCutFlow->Fill(1.0, el_n );
  h_PhotonCutFlow->Fill(1.0, NumPhotons );
  h_JetCutFlow->Fill(1.0, NumJets );
  h_TauCutFlow->Fill(1.0, tau_n );

  // Primary Vertex                                                                                                                                                                                             
  for (unsigned int i=0; i<vxContainer->size(); i++){
    const xAOD::Vertex* vxcand = vxContainer->at(i);
    if ( vxcand->nTrackParticles() >= 2 && vxcand->vertexType() == xAOD::VxType::PriVtx ){
      primVtx = true;
      nVtx++;
    } // end of if statement
  } // end of vertex loop
  if (!primVtx) return EL::StatusCode::SUCCESS;
  isVertexGood=true;
  h_CutFlow->Fill(2);
  h_MuonCutFlow->Fill(2.0, mu_n );
  h_ElectronCutFlow->Fill(2.0, el_n );
  h_PhotonCutFlow->Fill(2.0, NumPhotons );
  h_JetCutFlow->Fill(2.0, NumJets );
  h_TauCutFlow->Fill(2.0, tau_n );
  
  /////////////////////////////////////////////////////////////////
  // Trigger Selection
  /////////////////////////////////////////////////////////////////

  // Trigger menu output
  //if (m_eventCounter == 1){
  //auto chainGroups = m_TrigDecTool->getChainGroup("HLT.*");
  //  for(auto &trig : chainGroups->getListOfTriggers()) {
  //      std::cout << "  " << trig << std::endl;
  // } // end of loop
  //} // enf of First Event 

  bool TriggerDecision1 = false;
  bool TriggerDecision4 = false;
  bool TriggerDecision5 = false;
  bool TriggerDecision6 = false;
  bool TriggerDecision3 = false;
  bool MuonTriggerPassed = false, ElectronTriggerPassed = false;
  bool MuonTriggerMatched = true, ElectronTriggerMatched = true;

  //2015+2016 triggers muon
  TriggerDecision1 = m_TrigDecTool->isPassed("HLT_mu50");
  //2015 triggers electron
  TriggerDecision3 = m_TrigDecTool->isPassed("HLT_e60_lhmedium");
  TriggerDecision4 = m_TrigDecTool->isPassed("HLT_e120_lhloose");
  //2016 triggers electron	
  TriggerDecision5 = m_TrigDecTool->isPassed("HLT_e60_lhmedium_nod0"); 
  TriggerDecision6 = m_TrigDecTool->isPassed("HLT_e140_lhloose_nod0"); 

  if( isMC ){
	  if( TriggerDecision1 == true  )  MuonTriggerPassed = true; 
	  if( rNum <= 284484 && rNum != 0 ){
	  	if( TriggerDecision3 == true || TriggerDecision4 == true )  ElectronTriggerPassed = true; 
	  } else{ if( TriggerDecision5 == true || TriggerDecision6 == true ){  ElectronTriggerPassed = true;}  };// end of if statement
  } // end of isMC
  else{ 
	  if( TriggerDecision1 == true  )  MuonTriggerPassed = true; 
	  if( run <= 284484 ){
	  	if( TriggerDecision3 == true || TriggerDecision4 == true )  ElectronTriggerPassed = true; 
	  } else{ if( TriggerDecision5 == true || TriggerDecision6 == true ){  ElectronTriggerPassed = true;}  };// end of if statement
  } // end of isData
   

  if( MuonTriggerPassed == false && ElectronTriggerPassed == false ) return EL::StatusCode::SUCCESS;
  if( m_debug ) std::cout<<"Trigger Passed!"<<std::endl;
  h_CutFlow->Fill(3);
  h_MuonCutFlow->Fill(3.0, mu_n );
  h_ElectronCutFlow->Fill(3.0, el_n );
  h_JetCutFlow->Fill(3.0, NumJets );
  h_TauCutFlow->Fill(3.0, tau_n );
  h_PhotonCutFlow->Fill(3.0, NumPhotons );

  filename = GetFileName( (wk()->inputFile())->GetName() );

  //////////////////////////////////////////////////////////////////////
  //
  //              MUON SELECTION
  //
  //////////////////////////////////////////////////////////////////////

  m_MetCorrMuons->clear();
  //Trigger matchingtool takes a vector of IParticles, and a trigger chain expression
  std::vector<const xAOD::IParticle*> myMuons;

  bool MuonMCP_bool = false, MuonPt_bool = false, MuonIso_bool = false, MuonMSHits_bool = false, Muond0_bool = false, Muonz0_bool = false;
  bool BadMuonFlag=false;
  // create a shallow copy of the muons container
  std::pair< xAOD::MuonContainer*, xAOD::ShallowAuxContainer* > muons_shallowCopy = xAOD::shallowCopyContainer( *muons );

  // iterate over our shallow copy
  xAOD::MuonContainer::iterator muonSC_itr = (muons_shallowCopy.first)->begin();
  xAOD::MuonContainer::iterator muonSC_end = (muons_shallowCopy.first)->end();

  xAOD::MuonContainer* muonsCorr = muons_shallowCopy.first;
  int NumberOLRMuons=0, NumberOLRElectrons=0,NumberOLRTaus=0, NumberOLRPhotons=0, NumberOLRJets=0;

  for( auto mu : *muonsCorr ){

    //selectDec(*mu) = false;
    mu->auxdecor<char>("isPreSel") = false;

    if( m_debug ) std::cout<<"Start of Muon Loop"<<std::endl;
    h_MuonCutFlow->Fill(4);

    if( isMC ){
	if( m_muSmear.at(m_year)->applyCorrection(*mu) == CP::CorrectionCode::Error ){
                  // Can have CorrectionCode values of Ok, OutOfValidityRange, or Error. Here only checking for Error.
                  // If OutOfValidityRange is returned no modification is made and the original muon values are taken.
                  Error("execute()", "MuonCalibrationAndSmearingTool returns Error CorrectionCode");
        } // end of error message
    } // end of loop

    if( isMC ){
      if( ApplyCorrections ){

    	for (sysListItr = m_sysList.begin(); sysListItr != m_sysList.end(); ++sysListItr){
    		if (!((*sysListItr).name()==sys_name)) continue;
		//std::cout<<"Sys Name "<<sys_name<<std::endl;
 		if( m_muIsoSF->applySystematicVariation( *sysListItr ) == CP::SystematicCode::Unsupported ) {
         		Error("execute()", "Cannot configure iso muon SF tool for systematic" );
   		} // end check that systematic applied ok

 		if( m_muTrigSF->applySystematicVariation( *sysListItr ) == CP::SystematicCode::Unsupported ) {
         		Error("execute()", "Cannot configure muon trig SF tool for systematic" );
   		} // end check that systematic applied ok

 		if( m_muRecoSF->applySystematicVariation( *sysListItr ) == CP::SystematicCode::Unsupported ) {
         		Error("execute()", "Cannot configure muon reco SF tool for systematic" );
   		} // end check that systematic applied ok

 		if( m_muTTVASF->applySystematicVariation( *sysListItr ) == CP::SystematicCode::Unsupported ) {
         		Error("execute()", "Cannot configure muon reco SF tool for systematic" );
   		} // end check that systematic applied ok

    	} // end for loop over systematics
      } // end of if ApplyCorrections
    } // end of isMC == true

    if( m_debug ) std::cout<<"Skipped corrections"<<std::endl;
  

    //if(m_debug) std::cout<<"Muon Quality "<<m_muonSelection->getQuality(*mu)<<std::endl;

    //Muon pT requirement
    if( (mu)->pt() * 0.001  < 65.0 ) continue;      
    MuonPt_bool = true; h_MuonCutFlow->Fill(5);    

    if( m_debug ) std::cout<<"Passed pt requirement"<<std::endl;

    // Muon CB Requirement                                                                            
    if( mu->muonType() != xAOD::Muon::Combined ) continue;
    h_MuonCutFlow->Fill(6);

    if( m_debug ) std::cout<<"Passed comb muon"<<std::endl;

    // MCP Quality Requirements                                                                     
    if( !(m_muonSelection->accept(*mu)) ) continue;
    MuonMCP_bool = true; h_MuonCutFlow->Fill(7);

    if( m_debug ) std::cout<<"Passed muon sel tool"<<std::endl;

    // Impact parameter requirements
    const xAOD::TrackParticle* trk = mu->trackParticle(xAOD::Muon::TrackParticleType::CombinedTrackParticle);

    if( m_debug ) std::cout<<"Retrieved track particle"<<std::endl;

    float delta_z0=0.0; bool z0_Pass = false;
    float d0_significance=0.0;

    if( trk ){
        // Compute z0
        for (unsigned int i=0; i<vxContainer->size(); i++){
	  const xAOD::Vertex* vtx = vxContainer->at(i);
	  if ( vtx->vertexType() == xAOD::VxType::PriVtx ){
	      delta_z0 = fabs((trk->z0() + trk->vz() - vtx->z())*sin(trk->theta()));
	      if( delta_z0 < 0.5 ) z0_Pass = true;
	  }
        } // end of loop

        double d0 = trk->d0();
	double cov = trk->definingParametersCovMatrix()(0,0);
        double beam = eventInfo->beamPosSigmaX()*eventInfo->beamPosSigmaX() + eventInfo->beamPosSigmaY()*eventInfo->beamPosSigmaY();

       // Compute d0
       d0_significance =  xAOD::TrackingHelpers::d0significance( trk, eventInfo->beamPosSigmaX(), eventInfo->beamPosSigmaY(), eventInfo->beamPosSigmaXY() );
    } // end of if track particle

    if( m_debug ) std::cout<<"Passed track loop"<<std::endl;

    // MS Hits requirements                                                           
    //if( !m_muonSelection->passedHighPtCuts(*mu) ) continue;
    MuonMSHits_bool = true; h_MuonCutFlow->Fill(8);                                                                                                     
 
    if( fabs(d0_significance) > 3.0 ) continue;    Muond0_bool = true; h_MuonCutFlow->Fill(9);
    if( !z0_Pass ) continue; Muonz0_bool = true; h_MuonCutFlow->Fill(10);   

    if( m_debug ) std::cout<<"Passed track requirements"<<std::endl;                   

    h_Muon_Iso_pT_Den->Fill( (mu)->pt()*GeV ); // Eff Plot Iso

    mu->auxdecor<char>("isPreSel") = true;
    //selectDec(*mu) = true;
    m_MetCorrMuons->push_back(mu);
    NumberOLRMuons++;

    if( m_debug ) std::cout<<"Filled MET"<<std::endl;                   
    
    // Fill tree variables
    Mu_Charge.push_back( mu->charge() );   
    Mu_pt.push_back( mu->pt()*0.001 );       
    Mu_eta.push_back( mu->eta() );      
    Mu_phi.push_back( mu->phi() );      
    Mu_d0.push_back( trk->d0() );       
    Mu_d0sig.push_back( d0_significance );    
    Mu_z0.push_back( trk->z0() );       
    Mu_deltaz0.push_back( z0_Pass );  
    Mu_Iso.push_back( m_iso->accept(*mu) );    
    if( isMC ){ 
        Mu_TrigSF.push_back( GetMuonTriggerSF(mu,rNum) );
	Mu_RecoSF.push_back( GetMuonRecoSF(mu) );   
        Mu_IsoSF.push_back( GetMuonIsoSF(mu) );    
        Mu_TrackSF.push_back( GetMuonTrackSF(mu) );    
    } // end of if statement

    Mu_isBad.push_back( m_muonSelection->isBadMuon(*mu) );      
    Mu_isHighPt.push_back( m_muonSelection->accept(*mu) );      
    Mu_isID.push_back( m_muonSelection->accept(*mu) );      
    h_Muon_ID_Eta_Den->Fill( (mu)->eta() ); // Eff plot highpT working point
    h_Muon_ID_pT_Den->Fill( (mu)->pt()*GeV ); // Eff plot hight working point

    h_Muon_ID_Eta_Num->Fill( (mu)->eta() ); // Eff plot highPT working point
    h_Muon_ID_pT_Num->Fill( (mu)->pt()*GeV ); // Eff plot highPT working point 

    if( m_debug ) std::cout<<"Finished muon loop"<<std::endl;                   

  } // end for loop over shallow copied muons

  //delete muons_shallowCopy.first;
  //delete muons_shallowCopy.second;

  //////////////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////////////////                                                                                                                                          
  //                                                                                                                                                                                                              
  //              ELECTRON SELECTION                                                                                                                                                                              
  //                                                                                                                                                                                                              
  //////////////////////////////////////////////////////////////////////  

  //L1 Calo Weights
float EMthresh[50] = {2008.96,1777.98,2053.32,1643.75,2016.56,1930.7,2485.62,2738.57,2712.91,1936.1,
                                  4152.37,1825.52,1783.29,2188.33,2520.03,2545.74,2947.04,3086.35,4090.8,3371.26,
                                  3773.61,4203.79,4813.52,5420.25,4753.5,4923.54,4338.46,4210.73,4182.22,3626.3,
                                  3460.46,3326.42,2916.75,2317.44,2443.85,2144.06,2026.63,1828.14,1685.24,4284.14,
                                   2063.57,2656.55,2617.98,2300.39,2029.07,2053.82,1903.34,2280.31,2139.5,2106.39};

float EMsigma[50] = {488.849,477.262,692.451,667.071,709.393,535.159,807.23,868.081,514.064,568.739,
                                  2680.53,570.615,646.978,346.33,643.672,458.567,551.28,804.826,808.753,1155.14,
                                  1198.67,1388.94,1304.77,1079.12,1658.89,1596.59,1538.42,1526.29,1544.7,1114.52,
                                  1266.86,1225.65,979.65,657.256,517.2,482.163,537.074,458.072,578.225,2583.39,
                                  788.964,625.561,810.542,762.759,632.171,617.855,684.52,618.986,600.584,551.851};

  //vector containing the corrected energy
  std::vector<double> elecE(el_n,0);
  std::vector<double> elecpT(el_n,0);
  double L1CaloSF=1.0,effL1Calo=1.0;

  int Counter = -1;

  bool ElectronAuthor = false, ElectronOQ = false, ElectronPt = false, ElectronEta = false, ElectronID = false, Electrond0 = false, ElectronIso = false;

  // Calibrate Electrons, Fill Electron Vectors,
  std::pair< xAOD::ElectronContainer*, xAOD::ShallowAuxContainer* > els_shallowCopy = xAOD::shallowCopyContainer( *els );
  xAOD::ElectronContainer* elsCorr = els_shallowCopy.first;

  m_MetCorrElectrons->clear();
  //Trigger matchingtool takes a vector of IParticles, and a trigger chain expression
  std::vector<const xAOD::IParticle*> myElectrons;
  
  for( auto el : *elsCorr ) {    

    //selectDec(*el) = false;
    el->auxdecor<char>("isPreSel") = false;

    Counter++;
    h_ElectronCutFlow->Fill(4);   

    if( m_debug ) std::cout<<"Start of Electron loop"<<std::endl;

    // Loop over Recommended Systematics
    if( isMC ){
    	for (sysListItr = m_sysList.begin(); sysListItr != m_sysList.end(); ++sysListItr){
    		if (!((*sysListItr).name()==sys_name)) continue;
		//std::cout<<"Sys Name "<<sys_name<<std::endl;
 		if( m_EleCalibTool->applySystematicVariation( *sysListItr ) == CP::SystematicCode::Unsupported ) {
         		Error("execute()", "Cannot configure electron calibration tool for systematic" );
   		} // end check that systematic applied ok

 		if( m_Iso_SF_Tight->applySystematicVariation( *sysListItr ) == CP::SystematicCode::Unsupported ) {
         		Error("execute()", "Cannot configure electron Iso SF tool for systematic" );
   		} // end check that systematic applied ok

 		if( m_ID_SF_Tight->applySystematicVariation( *sysListItr ) == CP::SystematicCode::Unsupported ) {
         		Error("execute()", "Cannot configure electron ID SF tool for systematic" );
   		} // end check that systematic applied ok

 		if( m_Trig_SF_Tight->applySystematicVariation( *sysListItr ) == CP::SystematicCode::Unsupported ) {
         		Error("execute()", "Cannot configure electron Trig SF tool for systematic" );
   		} // end check that systematic applied ok

 		if( m_Reco_SF->applySystematicVariation( *sysListItr ) == CP::SystematicCode::Unsupported ) {
         		Error("execute()", "Cannot configure electron Reco SF tool for systematic" );
   		} // end check that systematic applied ok

    	} // end for loop over systematics
    } // end of isMC

    if( m_debug ) std::cout<<"Done with systematics"<<std::endl;

    // Calibrate Electrons                                   
    //if( ApplyCorrections ) m_EleCalibTool->applyCorrection(*el);
    m_EleCalibTool->applyCorrection(*el); 

    if( m_debug ) std::cout<<"Applied Correction"<<std::endl;

    // Fill electrons with calibrated energy
    elecE[Counter] = ((el)->e())*GeV;
    elecpT[Counter] = ((el)->pt())*GeV;
    double el_eta = (el)->caloCluster()->etaBE(2);

    if( m_debug ) std::cout<<"Got variables"<<std::endl;
    
    double el_author = ((el)->author());         
    if ( !(el_author == 1 || el_author == 16) ) continue;
    h_ElectronCutFlow->Fill(5); ElectronAuthor = true; 

    if( m_debug) std::cout<<"Author Cut"<<std::endl;

    // pT cut
    if ( !(elecpT[Counter] > 65.0) ) continue;
    h_ElectronCutFlow->Fill(6); ElectronPt = true;

    if( m_debug) std::cout<<"pT cut"<<std::endl;

    // Eta cut
    if (  fabs(el_eta) > 2.47 || ( fabs(el_eta) > 1.37 && fabs(el_eta) < 1.52)  ) continue;
    h_ElectronCutFlow->Fill(7); ElectronEta = true;

    if( m_debug) std::cout<<"Eta cut"<<std::endl;
 
    // Object Quality cut
    bool OQ = (el)->isGoodOQ(xAOD::EgammaParameters::BADCLUSELECTRON);
    if ( !OQ ) continue;
    h_ElectronCutFlow->Fill(8); ElectronOQ = true;

    if( m_debug) std::cout<<"OQ Cut"<<std::endl;

    h_Electron_ID_Eta_Den->Fill( el_eta, EventWeight ); // ID Eff plot eta
    h_Electron_ID_pT_Den->Fill( (el)->pt(),EventWeight ); // ID Eff plot pt

    if( m_debug) std::cout<<"Start of Track variables Cut"<<std::endl;

    // Get Track Particle
    const xAOD::TrackParticle* trk = (el)->trackParticle();
    double d0_significance = 0.0;
    double d0 = trk->d0(); 
    double cov = trk->definingParametersCovMatrix()(0,0);
    double beam = eventInfo->beamPosSigmaX()*eventInfo->beamPosSigmaX()+eventInfo->beamPosSigmaY()*eventInfo->beamPosSigmaY();
    d0_significance = xAOD::TrackingHelpers::d0significance( trk, eventInfo->beamPosSigmaX(), eventInfo->beamPosSigmaY(), eventInfo->beamPosSigmaXY() );

    double delta_z0=0; bool z0_Pass=false;
    for (unsigned int i=0; i<vxContainer->size(); i++){
	  const xAOD::Vertex* vtx = vxContainer->at(i);
	  if ( vtx->vertexType() == xAOD::VxType::PriVtx ){
	      delta_z0 = fabs((trk->z0() + trk->vz() - vtx->z())*sin(trk->theta()));
	    if( delta_z0 < 0.5 ) z0_Pass = true;
	  }
    } // end of loop

    if( m_debug) std::cout<<"End of Track variables Cut"<<std::endl;

    // Likelihood ID cut either medium or tight for 3rd Lepton veto purposes
    if( !((el)->auxdata<char>("LHMedium")) && !((el)->auxdata<char>("LHTight")) /*&& !(m_ElectronIDTool_Loose->accept(*el))*/ ) continue; 
    h_ElectronCutFlow->Fill(9); ElectronID = true; 

    if( m_debug) std::cout<<"LH-ID Cut"<<std::endl;

    h_Electron_ID_Eta_Num->Fill( el_eta );  // ID Eff plot eta
    h_Electron_ID_pT_Num->Fill( elecpT[Counter] ); // ID Eff plot pt

    // do significance cut
    if ( !(fabs(d0_significance) < 5) || !(z0_Pass) ) continue;
    h_ElectronCutFlow->Fill(10); Electrond0 = true;

    if( m_debug) std::cout<<"End of Track variables Cut II"<<std::endl;

    //Use true only if it passes tight ore medium, but might be subject to change if we use LH-Loose as our Loose WP for the QCD estimation in emu
    if( !((el)->auxdata<char>("LHMedium")) && !((el)->auxdata<char>("LHTight")) ) continue;
    //selectDec(*el) = true;
    m_MetCorrElectrons->push_back(el);
    NumberOLRElectrons++;
    
    if( m_debug) std::cout<<"Another LH Cut"<<std::endl;

    el->auxdecor<char>("isPreSel") = true;

    if( m_debug) std::cout<<"Start of filling tree"<<std::endl;

    //Fill Electron Variables
    Ele_OQ.push_back( (el)->isGoodOQ(xAOD::EgammaParameters::BADCLUSELECTRON) );       
    Ele_ID.push_back( (el)->auxdata<char>("LHMedium") );       
    Ele_ID_Tight.push_back( (el)->auxdata<char>("LHTight") );       
    Ele_ID_Loose.push_back( (el)->auxdata<char>("LHLoose") );       
    Ele_Charge.push_back( el->charge() );       
    Ele_pt.push_back( elecpT[Counter] );       

    //Ele_IsoCorr.push_back(m_isoCorrTool->applyCorrection(*el) ); 
    Ele_eta.push_back( el->eta() );      
    Ele_eta_calo.push_back( el_eta );
    Ele_phi.push_back( el->phi() );      
    Ele_d0.push_back( d0 );       
    Ele_d0sig.push_back( d0_significance );     
    Ele_deltaZ0.push_back( z0_Pass );     
    Ele_Iso.push_back( m_iso->accept(*el) );      
    //Ele_Iso_Loose.push_back( iso_ele_track->accept(*el) );      
    if( isMC ){
	    Ele_TrigSF.push_back( GetElectronTrigTightSF(el) );
    	Ele_RecoSF.push_back( GetElectronRecoSF(el) );   
    	Ele_IDSF.push_back( GetElectronIDTightSF(el) );
    	Ele_IsoSF.push_back( GetElectronIsoTightSF(el) );
    } // end of isMC
    if( m_debug) std::cout<<"End of filling tree"<<std::endl;

  } // end of electron loop
    
  if( ElectronAuthor == true ) h_CutFlow->Fill(4);
  if( ElectronPt == true ) h_CutFlow->Fill(5);
  if( ElectronEta == true ) h_CutFlow->Fill(6);
  if( ElectronOQ == true ) h_CutFlow->Fill(7);
  if( ElectronID == true ) h_CutFlow->Fill(8);
  if( Electrond0 == true ) h_CutFlow->Fill(9);
  if( ElectronIso == true ) h_CutFlow->Fill(10);

  /////////////////////////////////////////////////////////////////////
  //              TAU SELECTION
  //////////////////////////////////////////////////////////////////// 

  if( m_debug ) std::cout<<"Reached tau loop"<<std::endl;

  double TauRecoSF = 1.0, TauIDSF = 1.0, TauEleOLRSF = 1.0;

  //MET
  m_MetCorrTaus->clear();

  // create a shallow copy of the muons container
  std::pair< xAOD::TauJetContainer*, xAOD::ShallowAuxContainer* > taus_shallowCopy = xAOD::shallowCopyContainer( *taus );

  // iterate over our shallow copy
  xAOD::TauJetContainer::iterator tauSC_itr = (taus_shallowCopy.first)->begin();
  xAOD::TauJetContainer::iterator tauSC_end = (taus_shallowCopy.first)->end();

  xAOD::TauJetContainer* tausCorr = taus_shallowCopy.first;

  //xAOD::JetContainer::const_iterator jet_itr = m_thjetCont->begin();
  //xAOD::JetContainer::const_iterator jet_end = m_thjetCont->end();

  // SF for taus
  double TauEffSF = 1.0;

  if( m_debug ) std::cout<<"Start of tau loop"<<std::endl;

  int counter=0;
  for ( auto xTau : *tausCorr ){

      //selectDec(*xTau) = false;
      xTau->auxdecor<char>("isPreSel") = false;

      counter++;
      if( m_debug ) std::cout<<"Start of tau loop"<<std::endl;
      //double JetBDT  = xTau->isTau(xAOD::TauJetParameters::JetBDTSigLoose); 
      //double pt      = xTau->BDTJetScore();
      double eta     = fabs(xTau->eta());
      double phi     = xTau->phi();
      double nTracks = xTau->nTracks();
      double charge  = fabs(xTau->charge());

      const xAOD::TruthParticle* xTruthTau; 
      // perform truth matching
      if( m_debug ) std::cout<<"Performing Truth Matching"<<std::endl;
      if( isMC ){ xTruthTau = T2MT->getTruth(*xTau);}
      if( m_debug ) std::cout<<"About to perform smearing"<<std::endl;
      // Corrections
      if( ApplyCorrections && !m_isData ){
	    // List of systematics
    	/*for (sysListItr = m_sysList.begin(); sysListItr != m_sysList.end(); ++sysListItr){
    		if (!((*sysListItr).name()==sys_name)) continue;
		//std::cout<<"Sys Name "<<sys_name<<std::endl;
 		if( TauSmeTool->applySystematicVariation( *sysListItr ) == CP::SystematicCode::Unsupported ) {
			Error("execute()", "Cannot configure TauSmearingTool tool for systematic" );
   		} // end check that systematic applied ok
 
		} */ // end for loop over systematics
        if( m_debug ) std::cout<<"About to perform smearing"<<std::endl;
        
        if( TauSmeTool->applyCorrection(*xTau) == CP::CorrectionCode::Error){ 
	    Error("execute()", "TauSmearingTool returns Error CorrectionCode");
        } // end of error message
      } // end of if ApplyCorrections
      if( m_debug ) std::cout<<"Filling Tree Variables"<<std::endl;

      if( m_debug ) std::cout<<"End of filling Tree Variables"<<std::endl;
      h_TauCutFlow->Fill(4);

      if( xTau->pt()/1000.0 < 65.0 ) continue;
      h_TauCutFlow->Fill(5);

      if( fabs( xTau->eta() ) >= 2.5 ) continue; 
      if( fabs( xTau->eta() ) >= 1.37 && fabs( xTau->eta() ) <= 1.52 ) continue;
      h_TauCutFlow->Fill(6);

      if( charge != 1.0 ) continue;
      h_TauCutFlow->Fill(7);

      if( nTracks != 1.0 && nTracks != 3.0 ) continue;
      h_TauCutFlow->Fill(8);

      // decorate tau with electron llh score
      if( isMC ) TOELLHDecorator->decorate(*xTau);

      Tau_pt.push_back( xTau->pt() );
      Tau_ID.push_back( xTau->isTau(xAOD::TauJetParameters::JetBDTSigLoose) );
      Tau_ID_Med.push_back( xTau->isTau(xAOD::TauJetParameters::JetBDTSigMedium) );
      Tau_phi.push_back( xTau->phi() );
      Tau_eta.push_back( xTau->eta() );
      TauSelector.push_back( TauSelTool_NoOLR->accept( *xTau ) );
      Tau_ntracks.push_back( xTau->nTracks() );
      Tau_charge.push_back( fabs(xTau->charge()) );
      Tau_JetBDT.push_back( xTau->discriminant(xAOD::TauJetParameters::BDTJetScore) );
      TauSelector_WithOLR.push_back( TauSelTool->accept( *xTau ) );

      /*const xAOD::Jet* xTruthJet = xAOD::TauHelpers::getLink<xAOD::Jet>(xTau, "truthJetLink");

      if( xTruthJet != NULL ){  

	  int PDGID=0;
          for( auto truth = xTruthParticleContainer->begin(); truth!=xTruthParticleContainer->end(); ++truth ){
	

	    if( (fabs((*truth)->pdgId()) >= 1 && fabs((*truth)->pdgId()) <= 6) || fabs((*truth)->pdgId()) == 21 || fabs((*truth)->pdgId()) == 11 || fabs((*truth)->pdgId()) == 13 || fabs((*truth)->pdgId()) == 15  ){	

		double DeltaEta=(*truth)->eta()-xTruthJet->eta();
		double DeltaPhi=(*truth)->phi()-xTruthJet->phi();

		double Delta = sqrt( DeltaEta*DeltaEta + DeltaPhi*DeltaPhi  );
	        if( Delta < 0.2 ) PDGID = (*truth)->pdgId();

	    } // end of if jet particle 	 

	} // end of loop for truth particles

	Tau_TruthJet.push_back(PDGID);

      }else{  Tau_TruthJet.push_back(0); }  */

      int PDGID=0; double TruthPt=0.0;
      /*if( isMC ){

       if( xTruthTau != NULL ){

         if( xTruthTau->isMuon() ) Tau_isMuon.push_back(true);
         if( !xTruthTau->isMuon() ) Tau_isMuon.push_back(false);
         if( xTruthTau->isElectron() ) Tau_isElectron.push_back(true);
         if( !xTruthTau->isElectron() ) Tau_isElectron.push_back(false);
	 Tau_TruthParticle.push_back( xTruthTau->pdgId() ); 
	 Tau_TruthMatched.push_back(true);
	 if (xTruthTau->isTau() ){
  
		Tau_isTau.push_back(true);
      		if( (bool)xTruthTau->auxdata<char>("IsHadronicTau")  ) Tau_isHadTau.push_back(true);		
      		if( !(bool)xTruthTau->auxdata<char>("IsHadronicTau")  ) Tau_isHadTau.push_back(false);		

	 } else{  Tau_isTau.push_back(false); Tau_isHadTau.push_back(false); }// end of if isTau	  
       } else{
         Tau_TruthMatched.push_back(false);
	 Tau_TruthParticle.push_back( 0 );
         Tau_isTau.push_back(false);
         Tau_isHadTau.push_back(false);
	 Tau_isMuon.push_back(false);
       	 Tau_isElectron.push_back(false);
       } // end of if statement

       for( ; jet_itr != jet_end; ++jet_itr ) {

		double DeltaEta=(*jet_itr)->eta()-xTau->eta();
		double DeltaPhi=(*jet_itr)->phi()-xTau->phi();

		double Delta = sqrt( DeltaEta*DeltaEta + DeltaPhi*DeltaPhi  );
	        if( Delta < 0.4 && (*jet_itr)->pt() > TruthPt ){ 

			PDGID = (*jet_itr)->getAttribute<int>("ConeTruthLabelID"); 
			TruthPt = (*jet_itr)->pt();
			if( (*jet_itr)->getAttribute<int>("ConeTruthLabelID") == 0 ){

				PDGID = (*jet_itr)->getAttribute<int>("PartonTruthLabelID"); 

			} // end of if statement

		} // end of if statement

	} // end of loop for truth particles

	Tau_TruthJet.push_back( PDGID ); std::cout<<"Label ID: : "<<PDGID << std::endl;

      } // end of isMC */

      if( !xTau->isTau(xAOD::TauJetParameters::JetBDTSigLoose)  ) continue;
      h_TauCutFlow->Fill(9);     

      if( !TauSelTool_NoOLR->accept( *xTau )  ) continue;      
 
      //selectDec(*xTau) = true;
      m_MetCorrTaus->push_back(xTau);

      if( !TauSelTool->accept( *xTau )  ) continue;      
      xTau->auxdecor<char>("isPreSel") = true;
      NumberOLRTaus++;


  } // end of tau loop



  ////////////////////////////////////////////////////////////////////////
  //
  //              JET SELECTION
  //
  //////////////////////////////////////////////////////////////////////
  int nJets=0;

  std::pair< xAOD::JetContainer*, xAOD::ShallowAuxContainer* > jets_shallowCopy = xAOD::shallowCopyContainer( *jets );

  xAOD::JetContainer* JetsCorr = jets_shallowCopy.first;

  if(isMC) m_jetFrwJvtTool->tagTruth(JetsCorr,m_thjetCont);
  
  bool isGoodBJet = false;
  float BTaggingSF = 1.0, BTaggingSFTot = 1.0, JVTSF = 1.0, JetSF = 1.0;
  
  for( auto jet : *JetsCorr ) {

       //selectDec(*jet) = false;
       jet->auxdecor<char>("isPreSel") = false;

       h_JetCutFlow->Fill(4);

       if( m_debug ) std::cout<<"Start of jet Loop"<<std::endl;

       if( m_jetCalibration->applyCalibration(*jet) == CP::CorrectionCode::Error ){
	     		Info( "CheckCorrectedJets()", "JetCalibrationTool returns Error CorrectionCode in applying corrections");
       } // end of if statement

       float newjvt = hjvtagup->updateJvt(*jet);
       jet->auxdecor<float>("Jvt") = newjvt;  

       // JER and uncert
       if( ApplyCorrections ){ 

           if(isMC){
    		
      	   	if( smearJetTool->applyCorrection(*jet) == CP::CorrectionCode::Error ){
	        	Info( "CheckCorrectedJets()", "JetSmearingTool returns Error CorrectionCode in applying corrections");
           	   } // end of if statement

               if( (int)eventInfo->eventNumber() == 14126 ){  std::cout<<"Jet --> smeared+calibrated jet with pT "<<(jet)->pt()*0.001<<" eta "<<jet->eta()<<std::endl;}

		       if( m_jetUnc->applyCorrection(*jet) == CP::CorrectionCode::Error ){
			     Info( "CheckCorrectedJets()", "JetUncertainties returns Error CorrectionCode in applying corrections");
		       } // end of if statement 

               if( (int)eventInfo->eventNumber() == 14126 ){  std::cout<<"Jet --> smeared+calibrated+unc corrected with pT "<<(jet)->pt()*0.001<<" eta "<<jet->eta()<<std::endl;}

	        } // end of if isMC

       } // end if ApplyCorrections
       
  } // end of loop    

  if( m_jetFrwJvtTool->modify(*JetsCorr)!=0 ){
         Info( "CheckCorrectedJets()", "JetForwardJvtTool failed to apply changes to jet container");
  } // end of if statement

  for( auto jet : *JetsCorr ) {

       if( m_debug ) std::cout<<"Applied jet corrections"<<std::endl;     

       // Only jets with a pt > 25  and |eta| < 2.5
       if( (jet)->pt()*0.001 < 25.0 ) continue;
       h_JetCutFlow->Fill(5); 

       if( fabs( (jet)->eta() ) > 2.5 ) continue;
       h_JetCutFlow->Fill(6); 

       jet->auxdecor<char>("isPreSel") = true;
       NumberOLRJets++;
       //if( (int)eventInfo->eventNumber() == 14126 ){  std::cout<<"preselected jet with pT "<<(jet)->pt()*0.001<<" eta "<<jet->eta()<<" jvt "<<newjvt<<std::endl;}

  } // end for loop over jets 

  // Photon Container
  std::pair< xAOD::PhotonContainer*, xAOD::ShallowAuxContainer* > ph_shallowCopy = xAOD::shallowCopyContainer( *m_phCont );
  xAOD::PhotonContainer* phCorr = ph_shallowCopy.first;

  bool clean = true;
  m_MetCorrPhotons->clear();

  if( m_debug ) std::cout<<"Entering photon Loop"<<std::endl;
  for( auto ph: *phCorr) {

    //selectDec(*ph) = false;
    ph->auxdecor<char>("isPreSel") = false ;

    // Calibrate Photons
    m_EleCalibTool->applyCorrection(*ph); 

    if( m_debug ) std::cout<<"Calibrated photon"<<std::endl;

    if( ph->pt() < 10000.0 ) continue;
    h_PhotonCutFlow->Fill(4);

    if( m_debug ) std::cout<<"Passed pT"<<std::endl;

    if( fabs(ph->caloCluster()->etaBE(2)) > 1.37 && fabs(ph->caloCluster()->etaBE(2)) < 1.52 ) continue; 
    if( fabs(ph->caloCluster()->etaBE(2)) > 2.47 ) continue;
    h_PhotonCutFlow->Fill(5);

    if( m_debug ) std::cout<<"Passed Eta"<<std::endl;

    if( ph->author() != xAOD::EgammaParameters::AuthorPhoton &&  ph->author() != xAOD::EgammaParameters::AuthorAmbiguous ) continue;
    h_PhotonCutFlow->Fill(6);

    if( m_debug ) std::cout<<"Passed author"<<std::endl;

    if( !(ph->isGoodOQ(xAOD::EgammaParameters::BADCLUSPHOTON)) ) continue;
    h_PhotonCutFlow->Fill(7);

    if( m_debug ) std::cout<<"Bad cluster photon"<<std::endl;

    //bool clean      = PhotonHelpers::passOQquality(ph); 
    //bool cleanDel   = PhotonHelpers::passOQqualityDelayed(ph);
    //if( !clean    ){ ph->auxdecor<char>("isPreSel") = false; continue; }  
    //if( !cleanDel ){ ph->auxdecor<char>("isPreSel") = false; continue; }

    //bool cleanPh      = false;
    //bool cleanDelPh   = false;

      //cleanPh      = PhotonHelpers::passOQquality(ph);
      //cleanDelPh   = PhotonHelpers::passOQqualityDelayed(ph);

      //cleanPh    = ph->auxdata<char>("DFCommonPhotonsCleaning");
      //cleanDelPh = ph->auxdata<char>("DFCommonPhotonsCleaningNoTime");

    if( m_debug ) std::cout<<"Cleaning requirements"<<std::endl;

    h_PhotonCutFlow->Fill(8);

    ph->auxdecor<char>("isPreSel") = true ;
    //selectDec(*ph) = true;    
    m_MetCorrPhotons->push_back(ph);
    NumberOLRPhotons++;
  } 

  if( m_debug ) std::cout<<"Done with photon Loop, onto OLR"<<std::endl;

  //Apply OLR
  orTool->removeOverlaps(elsCorr, muonsCorr, JetsCorr, tausCorr, phCorr );

  if( m_debug ) std::cout<<"Done with OLR, now finish the selection"<<std::endl;

   /////////////////////////////////////////////////////////////////////////////////////////////////////////

   ///////////////// APPLY FINAL SELECTION AFTER OLR ///////////////////////////////////////////////////////

   /////////////////////////////////////////////////////////////////////////////////////////////////////////////

  for( auto mu : *muonsCorr ){

    myMuons.clear();
    myMuons.push_back( mu );

    //Check if object is selected
    //if( !selectDec(*mu) ) continue;
    //Mu_OLR.push_back( overlapAcc(*mu) );     
    //Check if it's overlapping
    //if( overlapAcc(*mu) ) continue; 
    
    if( !mu->auxdata<char>("isPreSel") ) continue;
    Mu_OLR.push_back( mu->auxdata<char>("overlaps") );
    if( mu->auxdata<char>("overlaps") ) continue; 
    
    h_MuonCutFlow->Fill(11);

    if( m_muonSelection->isBadMuon(*mu) ) BadMuonFlag=true;

    // Count number of loose muons
    if( !(m_iso->accept(*mu)) ){ NumberGoodLooseMuons++; TotalNumberGoodLooseMuons++; LooseMuon.SetPtEtaPhiM( (mu)->pt() * 0.001, (mu)->eta(), (mu)->phi(), MuonPDGMass ); LooseMuon_Phi.push_back( mu->phi() ); LooseMuon_Eta.push_back( mu->eta() );}                                                                          
                                                                                                                                           
    // Isolation Requirements                                                                
    if( !(m_iso->accept(*mu)) ) continue;

    MuonIso_bool = true; h_MuonCutFlow->Fill(12);
    h_Muon_Iso_pT_Num->Fill( (mu)->pt()*GeV );
    NumberGoodMuons++; TotalNumberGoodMuons++;
    Muon_Phi.push_back( mu->phi() ); Muon_Eta.push_back( mu->eta() );
                                                                                                        
    if( (mu)->pt()*0.001 >  Muon.Pt()  ){ Muon.SetPtEtaPhiM( (mu)->pt() * 0.001, (mu)->eta(), (mu)->phi(), MuonPDGMass  ); ChargeMuon = (mu)->charge(); } 

    //Trigger Matching
    if( m_match_Tool->match(myMuons,"HLT_mu50",0.1) ) MuonTriggerMatched=true;
    if( MuonTriggerMatched ) Mu_TriggerMatched = MuonTriggerMatched;  
    if( m_debug ) std::cout<<"HLT_Mu50 match "<<m_match_Tool->match(myMuons,"HLT_mu50",0.1)<<std::endl;

    //Get Reco Eff Weight
    if( isMC ){

      Muon_IsoEff_SF = GetMuonIsoSF(mu);
      Muon_Eff_SF = GetMuonRecoSF(mu);
      Muon_Track_SF = GetMuonTrackSF(mu);
      if(MuonTriggerMatched){ 
    	     Muon_TrigEff_SF = GetMuonTriggerSF(mu,rNum);
      	     EventWeight*=(Muon_TrigEff_SF);	     
      } // end of if statement
      EventWeight*=(Muon_IsoEff_SF*Muon_Eff_SF*Muon_Track_SF);

    } // end of isMC 

  } // end for loop over shallow copied muons

  for( auto el : *elsCorr ) {    

    myElectrons.clear();
    myElectrons.push_back( el );

    // Fill electrons with calibrated energy
    elecE[Counter] = ((el)->e())*GeV;
    elecpT[Counter] = ((el)->pt())*GeV;
    double el_eta = (el)->caloCluster()->etaBE(2);

    //Check if object is selected
    //if( !selectDec(*el) ) continue; 
    //Ele_MuonOLR.push_back( overlapAcc(*el) );         
    //Check if it's overlapping
    //if( overlapAcc(*el) ) continue; 

    if( !el->auxdata<char>("isPreSel") ) continue;
    Ele_MuonOLR.push_back( el->auxdata<char>("overlaps") );
    if( el->auxdata<char>("overlaps") ) continue; 

    h_ElectronCutFlow->Fill(11);

    // L1Calo cut
   // if( !TriggerDecision1 ){ 

	//if( el_eta > 1.0 && el_eta < 1.3 && elecpT[Counter] > 1000.0 ) continue;
	//if( el_eta < 1.0 && elecpT[Counter] > 3600.0 ) continue;
	//if( el_eta > 1.3 && elecpT[Counter] > 1800.0 ) continue;

    //} // end of if muon trigger not fired
    h_ElectronCutFlow->Fill(12);
    h_Electron_Iso_pT_Den->Fill( elecpT[Counter] ); // Iso eff plot

    //if( !m_isoCorrTool->applyCorrection(*el) ) continue;

    //if( !m_isoCorrTool->applyCorrection(*el) ) continue;

    // Isolation cut+3rd Lepton
    if( (m_iso->accept(*el) &&  (el)->auxdata<char>("LHTight")) ){  NumberGoodElectrons++; TotalNumberGoodElectrons++; h_ElectronCutFlow->Fill(13); Electron_Phi.push_back( el->phi() ); Electron_Eta.push_back( (el)->eta() );
    } else if( (el)->auxdata<char>("LHMedium") ){ NumberGoodLooseElectrons++; TotalNumberGoodLooseElectrons++; if( elecpT[Counter] > LooseElectron.Pt() ){ LooseElectron.SetPtEtaPhiM( elecpT[Counter], (el)->eta(), (el)->phi(), 0.511*GeV );} LooseElectron_Phi.push_back( el->phi() ); LooseElectron_Eta.push_back( (el)->eta() ); }     

    h_Electron_Iso_pT_Num->Fill( elecpT[Counter] ); // Iso eff plot

    //Trigger Matching
    if( rNum <= 284484 && rNum != 0 ){
  	if( m_match_Tool->match( myElectrons,"HLT_e60_lhmedium",0.07) || m_match_Tool->match( myElectrons,"HLT_e120_lhloose",0.07) )  ElectronTriggerMatched = true; 
    }else { if( m_match_Tool->match( myElectrons,"HLT_e60_lhmedium_nod0",0.07) || m_match_Tool->match( myElectrons,"HLT_e140_lhloose_nod0",0.07) ){ ElectronTriggerMatched = true;}  };// end of if statement

    if( ElectronTriggerMatched ) Ele_TriggerMatched = ElectronTriggerMatched;
    if( m_debug ) std::cout<<"Trigger match e60 "<<m_match_Tool->match( myElectrons,"HLT_e60_lhmedium",0.07)<<" Trigger match e120 "<<m_match_Tool->match( myElectrons,"HLT_e120_lhloose",0.07)<<std::endl;
    if( m_debug ) std::cout<<"Trigger match e60nod "<<m_match_Tool->match( myElectrons,"HLT_e60_lhmedium_nod0",0.07)<<" Trigger match e140nod "<<m_match_Tool->match( myElectrons,"HLT_e140_lhloose_nod0",0.07)<<std::endl;
    if( m_debug ) std::cout<<"Start of filling tree"<<std::endl;

    // Setup Electron 4-vector if its tight                                                                      
    if( elecpT[Counter] > Electron.Pt() &&  (el)->auxdata<char>("LHTight") && m_iso->accept(*el) ){ Electron.SetPtEtaPhiM( elecpT[Counter], (el)->eta(), (el)->phi(), 0.511*GeV  ); ChargeElectron = ((el)->charge()); }   
    
    if( isMC && (m_iso->accept(*el) &&  (el)->auxdata<char>("LHTight")) ){ 

   	  Electron_ID_SF = GetElectronIDTightSF(el);
      Electron_Reco_SF = GetElectronRecoSF(el);
      Electron_Iso_SF = GetElectronIsoTightSF(el);
      if( ElectronTriggerMatched ) Electron_Trig_SF = GetElectronTrigTightSF(el);

	  if(  rNum <= 284484 ){

		//L1 Calo Weight
		int keta = (el->eta())*10 + 25;
		double thresh = EMthresh[keta];
		double sigma = sqrt(elecpT[Counter]/EMthresh[keta])*EMsigma[keta];
        double wt =  erfc( (thresh - elecpT[Counter])/(sqrt(2.)*sigma) )/2.0;
        effL1Calo = 1.-wt; if(m_debug)  std::cout<<"L1Calo Weight "<<effL1Calo<<std::endl;

		L1CaloSF*=effL1Calo;	    	

	  } // end of if medium electron
      EventWeight*=(Electron_Reco_SF*Electron_ID_SF*Electron_Iso_SF*effL1Calo);
    } // end of if MC    

  } // end of electron loop
  								
  
  for ( auto xTau : *tausCorr ){
  
    double eta     = fabs(xTau->eta());
    double phi     = xTau->phi();
    double nTracks = xTau->nTracks();
    double charge  = fabs(xTau->charge());

    if( xTau->pt()/1000.0 < 65.0 ) continue;     
    if( fabs( xTau->eta() ) >= 2.5 ) continue; 
    if( fabs( xTau->eta() ) >= 1.37 && fabs( xTau->eta() ) <= 1.52 ) continue;   
    if( charge != 1.0 ) continue;
    if( nTracks != 1.0 && nTracks != 3.0 ) continue;

    Tau_OLR.push_back( xTau->auxdata<char>("overlaps") );

    if( !xTau->auxdata<char>("isPreSel") ) continue;
    h_TauCutFlow->Fill(10);

    if( !TauSelTool->accept( *xTau ) ) continue;
    h_TauCutFlow->Fill(11);
    
    //Check if it's overlapping
    //if( overlapAcc(*xTau) ) continue;     
    if( xTau->auxdata<char>("overlaps") ) continue;     
    h_TauCutFlow->Fill(12); 

    NumberGoodTaus++; Tau_Phi.push_back( xTau->phi() ); Tau_Eta.push_back( xTau->eta() );  // Increase number of good taus

      //Apply SF
      //TauEffSF = GetTauRecoSF(*xTau);
      if( isMC ){
		TauEffCorrTool->getEfficiencyScaleFactor(*xTau, TauRecoSF);
	        TauEffIDTool->getEfficiencyScaleFactor(*xTau, TauIDSF);
      		TauEffEleOLRTool->getEfficiencyScaleFactor(*xTau, TauEleOLRSF);
		EventWeight*=(TauRecoSF*TauIDSF*TauEleOLRSF);
		if(m_debug) std::cout<<"Tau Reco SF "<<TauRecoSF<<" TauIDSF "<<TauIDSF<<" TauEleOLRSF "<<TauEleOLRSF<<std::endl;
      } // end of if statement

      if( xTau->pt()*0.001 > Tau.Pt() ){ Tau.SetPtEtaPhiM( xTau->pt()*0.001, xTau->eta(), xTau->phi(), 1.77682  ); ChargeTau = xTau->charge(); }

  } // end of tau loop

   int NumberGoodJets=0, NumberOverlapJets=0;

  for( auto jet : *JetsCorr ) {

    //Check if object is selected
    //if( !selectDec(*jet) ) continue;
    //h_JetCutFlow->Fill(8);
    //Check if it's overlapping
    //if( overlapAcc(*jet) ) continue; 
    float newjvt    = jet->auxdata<float>("Jvt");
    float DetectorEta = 0.0; (jet)->getAttribute("DetectorEta",DetectorEta);
    
    //if( (int)eventInfo->eventNumber() == 14126 ){  std::cout<<"PRESELECTED jet with pT "<<(jet)->pt()*0.001<<" eta "<<jet->eta()<<" etaEM "<<DetectorEta<<" jvt "<<newjvt<<std::endl;}	 

    if( !jet->auxdata<char>("isPreSel") ) continue;
    h_JetCutFlow->Fill(8); 
    if( jet->auxdata<char>("overlaps") ){ 
    
        //if( (int)eventInfo->eventNumber() == 14126 ){  std::cout<<"OVERLAPING jet with pT "<<(jet)->pt()*0.001<<" eta "<<jet->eta()<<" etaEM "<<DetectorEta<<" jvt "<<newjvt<<std::endl;}
        NumberOverlapJets++; 
        continue;
        
    } // end of if statement
    h_JetCutFlow->Fill(9);   


       if( m_debug ) std::cout<<"Jet eta "<<(jet)->eta()<<" DetectorEta "<<DetectorEta<<std::endl;
  
       //Check Jet Cleaning
       if( (jet)->pt()*0.001 < 60.0 && fabs( DetectorEta ) <  2.4 && newjvt >= 0.59 && !m_jetCleaning->accept( *jet ) ) BadJetEvent=true;
       if( (jet)->pt()*0.001 < 60.0 && fabs( DetectorEta ) >= 2.4 && !m_jetCleaning->accept( *jet ) ) BadJetEvent=true;
       if( (jet)->pt()*0.001 >= 60.0 && !m_jetCleaning->accept( *jet ) ) BadJetEvent=true;
     
       // Require jets with pt < 50 to have jvt > 0.59 
       if( (jet)->pt()*0.001 < 60.0 && fabs( DetectorEta ) < 2.4 && newjvt <= 0.59 ){ 
        //if( (int)eventInfo->eventNumber() == 14126 ){  std::cout<<"FAILED jvt with pT "<<(jet)->pt()*0.001<<" Detector eta "<<DetectorEta<<" jvt "<<newjvt<<std::endl;}
        continue; 
       }// end of if statement 
       h_JetCutFlow->Fill(10); 

        if( (int)eventInfo->eventNumber() == 14126 ){  std::cout<<"Passed jvt"<<std::endl;}

      if( m_debug ) std::cout<<"About to do jet cleaning"<<std::endl;

       if( !m_jetCleaning->accept( *jet ) ) continue; //only keep good clean jet
       h_JetCutFlow->Fill(11); 
       nJets++;
      if( m_debug ) std::cout<<"jet cleaning done"<<std::endl;
        //if( (int)eventInfo->eventNumber() == 14126 ){  std::cout<<"Passed cleaning"<<std::endl;}
        //if( (int)eventInfo->eventNumber() == 14126 ){  std::cout<<"selected jet with pT "<<(jet)->pt()*0.001<<" eta "<<jet->eta()<<" jvt "<<newjvt<<std::endl;}

       double discriminant_mvx = 0.; 
       jet->btagging()->MVx_discriminant("MV2c10", discriminant_mvx ); 

       if( isMC ){ m_jetJvtSF->getEfficiencyScaleFactor(*jet,JVTSF); JetSF*=JVTSF; }
       //if( eventNumber == 1267307 ) Info( "Debug" , "JetPT = %f ", (jet)->pt()*0.001 );
       NumberGoodJets++;
       //Require b-tagged jet with 85% efficiency
       //if( discriminant_mvx < 0.645925 ){ 
       if( !m_btagSelection->accept( *jet) ){
	 if( isMC ){ 
	   if( m_btagSF->getInefficiencyScaleFactor(*jet,BTaggingSF) == CP::CorrectionCode::Error ){
	     Info( "BtaggingSF()", "JetBtagging SF returns error");
	   } // end of if statement                                              ; 
	   JetSF*=BTaggingSF; BTaggingSFTot*=BTaggingSF;
           if( m_debug ) std::cout<<"Jet is b-tagged"<<std::endl;	    
	   if( m_debug ) Info( "BTagging SF" , "JetBTaggingInEffSF = %f ", BTaggingSF ); 
	 } // end of isMC
	   continue;       
       } // end of disceiminant
       h_JetCutFlow->Fill(12); 
       
       if( m_btagSelection->accept( *jet) ){
       if( isMC ){
    	   if( m_btagSF->getScaleFactor(*jet,BTaggingSF) == CP::CorrectionCode::Error ){
    	     Info( "BtaggingSF()", "JetBtagging SF returns error");
    	   } // end of if statement
       } // end of isMC                                                 ; 
	   JetSF*=BTaggingSF; BTaggingSFTot*=BTaggingSF;
	   isGoodBJet = true;  
	} //end of is bjet

  } // end for loop over jets 

  EventWeight*=JetSF;
  Jet_N=nJets;

  if( m_debug ) std::cout<<"Done with OLR, onto MET"<<std::endl;

  // Build MET
  BuildMet              = new xAOD::MissingETContainer;
  BuildMetAux           = new xAOD::MissingETAuxContainer;
  softTrkMet            = new xAOD::MissingET;

  if( m_debug ) std::cout<<"Setting Original obj links"<<std::endl;
  if( !xAOD::setOriginalObjectLink( *muons, *muonsCorr ) ) Info( "BuildMET()", "ERROR::Failed to set the original muon object links!" );
  if( !xAOD::setOriginalObjectLink( *els, *elsCorr ) ) Info( "BuildMET()", "ERROR::Failed to set the original electron object links!" );
  if( !xAOD::setOriginalObjectLink( *taus, *tausCorr ) ) Info( "BuildMET()", "ERROR::Failed to set the original tau object links!" );
  if( !xAOD::setOriginalObjectLink( *jets, *JetsCorr ) ) Info( "BuildMET()", "ERROR::Failed to set the original jet object links!" );
  if( !xAOD::setOriginalObjectLink( *m_phCont, *phCorr ) ) Info( "BuildMET()", "ERROR::Failed to set the original jet object links!" );  

  if( m_debug ) std::cout<<"add copy containers to TStore"<<std::endl;
  //add copy containers to TStore
  if( !m_store->record( JetsCorr, "CalibJets").isSuccess() ) Info( "StoreObjects()", "ERROR::Failed to store CalibJets in Event Store" );
  if( !m_store->record( tausCorr, "CalibTaus").isSuccess() ) Info( "StoreObjects()", "ERROR::Failed to store CalibTaus in Event Store" );
  if( !m_store->record( elsCorr, "CalibElectrons").isSuccess() )Info( "StoreObjects()", "ERROR::Failed to store CalibElectrons in Event Store" );
  if( !m_store->record( phCorr , "CalibPhotons").isSuccess() )   Info( "StoreObjects()", "ERROR::Failed to store CalibPhotons in Event Store" );
  if( !m_store->record( muonsCorr, "CalibMuons").isSuccess() )  Info( "StoreObjects()", "ERROR::Failed to store CalibMuons in Event Store" );

  if( m_debug ) std::cout<<"Building MET"<<std::endl;

  BuildMet->setStore(BuildMetAux);
  m_metMapCont->resetObjSelectionFlags();

  m_metMaker->rebuildMET( "RefEle", xAOD::Type::Electron, BuildMet, m_MetCorrElectrons, m_metMapCont );
  m_metMaker->rebuildMET( "RefPhoton", xAOD::Type::Photon, BuildMet,m_MetCorrPhotons, m_metMapCont );  
  m_metMaker->rebuildMET( "RefTau", xAOD::Type::Tau, BuildMet, m_MetCorrTaus, m_metMapCont ); 
  m_metMaker->rebuildMET( "RefMuon", xAOD::Type::Muon, BuildMet,m_MetCorrMuons, m_metMapCont );
  m_metMaker->rebuildJetMET( "RefJet", "SoftClus", "PVSoftTrk", BuildMet,JetsCorr, m_metCont, m_metMapCont, true );

  if( m_debug ) std::cout<<"Getting MET"<<std::endl;

  m_metMaker->buildMETSum( "FinalTrk" , BuildMet, MissingETBase::Source::Track );
  MET = new xAOD::MissingET;
  MET = (*BuildMet)["FinalTrk"];

  if( m_debug ) std::cout<<"Got the MET"<<std::endl;

  //Set Neutrino 4-vector from missing energy
  Neutrino.SetPtEtaPhiM( MET->met()*GeV, Tau.Eta(), MET->phi(), 0.0 );
  MET_Et = MET->met()*GeV;
  MET_Phi = MET->phi();
  MET_Px = MET->mpx()*GeV;
  MET_Py = MET->mpy()*GeV;
  MET_SumEt = MET->sumet()*GeV;

  if(m_debug){ std::cout<<"Met ET: "<<MET->met()*GeV<<" px: "<<MET_Px<<" py: "<<MET_Py<<" phi: "<<MET_Phi<<" sumEt: "<<MET_SumEt<<std::endl; }

  BuildMet->clear();
  m_store->clear();

  // Fill tree variables
  isBTagged = isGoodBJet;
  if( NumberGoodMuons == 1 && NumberGoodElectrons == 1  ) isNlepGood = true;
  if( ElectronTriggerMatched == true || MuonTriggerMatched == true ) isTrigGood = true;
  if( ChargeElectron*ChargeMuon == -1.0 ) isOSGood = true;
  if( fabs(Dphi) >= 2.7 ) isBTBGood = true;
  dilepMass=Propagator.Mag(); 
  dilepPt=Propagator.Pt();
  dilepEta=Propagator.Rapidity(); 
  HLT_mu50 = TriggerDecision1;
  HLT_e60 = TriggerDecision3;
  HLT_e120 = TriggerDecision4;
  dilepPhi=Propagator.Phi(); 
  dilepDeltaPhi=Dphi;
  IsBadJetEvent=BadJetEvent; 
  weight       = EventWeight;
  Ele_L1CaloSF=effL1Calo;
  NumberTightMuons_tree = NumberGoodMuons;
  NumberLooseMuons_tree = NumberGoodLooseMuons;
  NumberTightElectrons_tree = NumberGoodElectrons;
  NumberLooseElectrons_tree = NumberGoodLooseElectrons;
   if( (eventInfo->errorState(xAOD::EventInfo::LAr)==xAOD::EventInfo::Error ) || (eventInfo->errorState(xAOD::EventInfo::Tile)==xAOD::EventInfo::Error ) || (eventInfo->isEventFlagBitSet(xAOD::EventInfo::Core, 18) )  ) isEvtClean= false;
  if( isNlepGood & isTrigGood & isOSGood & isBTBGood & isEvtClean & isKeepEvent & (HLT_mu50 || HLT_e60 || HLT_e120) ) isEvtGood = true;

  lfvTree->Fill();

  if( MuonPt_bool == true ){ h_CutFlow->Fill(11);
    if( MuonMCP_bool == true ){ h_CutFlow->Fill(12);
      if( MuonMSHits_bool == true ){ h_CutFlow->Fill(13);
        if( Muond0_bool == true ){ h_CutFlow->Fill(14);
	  if( Muonz0_bool == true ){ h_CutFlow->Fill(15);
	    if( MuonIso_bool == true ){ h_CutFlow->Fill(16);
            }
          }
        }
      }
    }  
  }

  /*if( (int)eventInfo->eventNumber() == 14126 ){  
  	std::cout<<"**************************************"<<std::endl;
  	std::cout<<"eventNumber "<<(int)eventInfo->eventNumber()<<"  Run "<<eventInfo->runNumber()<<std::endl;
  	std::cout<<"emu channel "<<m_electronMuon<<" etau channel "<<m_electronTau<<" mutau channel "<<m_muonTau<<std::endl;
  	std::cout<<"Met ET: "<<MET_Et<<" px: "<<MET_Px<<" py: "<<MET_Py<<" phi: "<<MET_Phi<<" sumEt: "<<MET_SumEt<<std::endl;
  	std::cout<<"Electron pT "<<Electron.Pt()<<" Muon Pt : "<<Muon.Pt()<<" Tau Pt : "<<Tau.Pt()<<std::endl;
    std::cout<<"Selected objects "<<std::endl;
    std::cout<<" nElectrons "<<NumberGoodElectrons<<" nMuons "<<NumberGoodMuons<<" nTaus "<<NumberGoodTaus<<" nJets "<<NumberGoodJets<<" nOverlapJets "<<NumberOverlapJets<<std::endl;
    std::cout<<"Preselected objects "<<std::endl;
    std::cout<<" nElectrons "<<NumberOLRElectrons<<" nMuons "<<NumberOLRMuons<<" nTaus "<<NumberOLRTaus<<" nJets "<<NumberOLRJets<<" nOverlapPhotons "<<NumberOLRPhotons<<std::endl;    
  	std::cout<<"**************************************"<<std::endl;
  }// end of if statement */

  //Bad Jet Event
  if( BadJetEvent || BadMuonFlag ){ return StatusCode::SUCCESS;} 
  h_CutFlow->Fill(17);

  //Number Leptons
  if( (NumberGoodMuons + NumberGoodElectrons + NumberGoodTaus) < 2 ){ return StatusCode::SUCCESS;}
  h_CutFlow->Fill(18);

  // 3rd Lepton Veto
  if( NumberGoodMuons > 1  || NumberGoodElectrons > 1  || NumberGoodTaus > 1  ){ return StatusCode::SUCCESS;}
  else if( NumberGoodMuons == 1 && NumberGoodElectrons == 1 && NumberGoodTaus == 1 ){ return StatusCode::SUCCESS;}
  if( (NumberGoodMuons + NumberGoodElectrons + NumberGoodTaus) < 2 ){ return StatusCode::SUCCESS;}
  h_CutFlow->Fill(19);
  if( (NumberGoodMuons == 1 && NumberGoodElectrons == 1) && (NumberGoodLooseMuons > 0 || NumberGoodLooseElectrons > 0) ){ return StatusCode::SUCCESS;}
  h_CutFlow->Fill(20);

  h_emuCutFlow->Fill(0); 
  h_etauCutFlow->Fill(0);
  h_mutauCutFlow->Fill(0);

  // Build the propagator at reconstructed level  
  if( NumberGoodMuons == 1 && NumberGoodElectrons == 1  ){ Propagator = Electron+Muon; m_electronMuon = true;}
  else if( NumberGoodElectrons == 0 ){ Propagator = Muon+Tau+Neutrino; m_muonTau = true;}
  else if( NumberGoodMuons == 0 ){ Propagator = Electron+Tau+Neutrino; m_electronTau = true;}

  if( m_electronMuon ) h_emuCutFlow->Fill(1);
  if( m_electronTau ) h_etauCutFlow->Fill(1);
  if( m_muonTau ) h_mutauCutFlow->Fill(1);

  if( ElectronTriggerMatched == false && MuonTriggerMatched == false ){ return StatusCode::SUCCESS;}
  h_CutFlow->Fill(21);
  if( m_electronMuon ) h_emuCutFlow->Fill(2);
  if( m_electronTau ) h_etauCutFlow->Fill(2);
  if( m_muonTau ) h_mutauCutFlow->Fill(2);  

  // Opposite Charge
  //if( ChargeElectron*ChargeMuon != -1.0  ) return StatusCode::SUCCESS;
  h_CutFlow->Fill(22);
  if( m_electronMuon ) h_emuCutFlow->Fill(3);
  if( m_electronTau ) h_etauCutFlow->Fill(3);
  if( m_muonTau ) h_mutauCutFlow->Fill(3);
  h_DeltaPhi->Fill( fabs(Dphi), EventWeight );  

  // DeltaPhi calculation
  Dphi = 0.0;
  if( m_electronMuon ) Dphi = Electron.Phi() - Muon.Phi();
  if( m_electronTau ) Dphi = Electron.Phi() - Tau.Phi();
  if( m_muonTau ) Dphi = Muon.Phi() - Tau.Phi();
  if( fabs(Dphi)>pi ) {
    if( Dphi>0 ){
      Dphi = 2*pi-Dphi;
    }
    else{
      Dphi = 2*pi+Dphi;
    }
  } // end of DeltaPhi calculation

  // Back to Back Leptons
  if( fabs(Dphi) < 2.7 ) return StatusCode::SUCCESS;
  h_CutFlow->Fill(23);
  if( m_electronMuon ) h_emuCutFlow->Fill(4);
  if( m_electronTau ) h_etauCutFlow->Fill(4);
  if( m_muonTau ) h_mutauCutFlow->Fill(4);  

  // Cut events that include a b-jet
  if(isGoodBJet == true){ numBEvent++; return StatusCode::SUCCESS; }
  h_CutFlow->Fill(24);
  if( m_electronMuon ) h_emuCutFlow->Fill(5);
  if( m_electronTau ) h_etauCutFlow->Fill(5);
  if( m_muonTau ) h_mutauCutFlow->Fill(5);

  if( m_electronMuon ) NumberElectronMuon++;
  if( m_electronTau ) NumberElectronTau++; 
  if( m_muonTau ) NumberMuonTau++; 

  if( MET_Et > 30.0 ) return StatusCode::SUCCESS;
  h_CutFlow->Fill(25);
  if( m_electronMuon ) h_emuCutFlow->Fill(6);
  if( m_electronTau ) h_etauCutFlow->Fill(6);
  if( m_muonTau ) h_mutauCutFlow->Fill(6);


  if( m_electronMuon ) NumberElectronMuon_MET++;
  if( m_electronTau ) NumberElectronTau_MET++; 
  if( m_muonTau ) NumberMuonTau_MET++; 
  
  if( m_debug ) std::cout<<"Propagator Mass "<<Propagator.Mag()<<std::endl;

  if( m_electronMuon ){  
  	h_emuCutFlow->Fill(7, PileUpWeight); 
  	h_emuCutFlow->Fill(8, PileUpWeight*Electron_Reco_SF); 
  	h_emuCutFlow->Fill(9, PileUpWeight*Electron_ID_SF*Electron_Reco_SF );
  	h_emuCutFlow->Fill(10, PileUpWeight*Electron_ID_SF*Electron_Reco_SF*Electron_Iso_SF );
  	h_emuCutFlow->Fill(11, PileUpWeight*Electron_ID_SF*Electron_Reco_SF*Electron_Iso_SF*Electron_Trig_SF );
  	h_emuCutFlow->Fill(12, PileUpWeight*Electron_ID_SF*Electron_Reco_SF*Electron_Iso_SF*Electron_Trig_SF*L1CaloSF );
  	h_emuCutFlow->Fill(13, PileUpWeight*Electron_ID_SF*Electron_Reco_SF*Electron_Trig_SF*Electron_Iso_SF*L1CaloSF*Muon_IsoEff_SF); 
  	h_emuCutFlow->Fill(14, PileUpWeight*Electron_ID_SF*Electron_Reco_SF*Electron_Trig_SF*Electron_Iso_SF*L1CaloSF*Muon_IsoEff_SF*Muon_Eff_SF ); 
  	h_emuCutFlow->Fill(15, PileUpWeight*Electron_ID_SF*Electron_Reco_SF*Electron_Trig_SF*Electron_Iso_SF*L1CaloSF*Muon_IsoEff_SF*Muon_Eff_SF*Muon_TrigEff_SF ); 
  	h_emuCutFlow->Fill(16, PileUpWeight*Electron_ID_SF*Electron_Reco_SF*Electron_Trig_SF*Electron_Iso_SF*L1CaloSF*Muon_IsoEff_SF*Muon_Eff_SF*Muon_TrigEff_SF*Muon_Track_SF );
  	h_emuCutFlow->Fill(17, PileUpWeight*Electron_ID_SF*Electron_Reco_SF*Electron_Trig_SF*Electron_Iso_SF*L1CaloSF*Muon_IsoEff_SF*Muon_Eff_SF*Muon_TrigEff_SF*Muon_Track_SF*MCWeight );
  	h_emuCutFlow->Fill(18, PileUpWeight*Electron_ID_SF*Electron_Reco_SF*Electron_Trig_SF*Electron_Iso_SF*L1CaloSF*Muon_IsoEff_SF*Muon_Eff_SF*Muon_TrigEff_SF*Muon_Track_SF*MCWeight*BTaggingSF );
  	h_emuCutFlow->Fill(19, PileUpWeight*Electron_ID_SF*Electron_Reco_SF*Electron_Trig_SF*Electron_Iso_SF*L1CaloSF*Muon_IsoEff_SF*Muon_Eff_SF*Muon_TrigEff_SF*Muon_Track_SF*MCWeight*JetSF );
  	h_emuCutFlow->Fill(20, EventWeight );
  } // end of if statement

  if( m_electronTau ){  
  	h_etauCutFlow->Fill(7, PileUpWeight); 
  	h_etauCutFlow->Fill(8, PileUpWeight*Electron_Reco_SF); 
  	h_etauCutFlow->Fill(9, PileUpWeight*Electron_ID_SF*Electron_Reco_SF );
  	h_etauCutFlow->Fill(10, PileUpWeight*Electron_ID_SF*Electron_Reco_SF*Electron_Iso_SF );
  	h_etauCutFlow->Fill(11, PileUpWeight*Electron_ID_SF*Electron_Reco_SF*Electron_Iso_SF*Electron_Trig_SF );
  	h_etauCutFlow->Fill(12, PileUpWeight*Electron_ID_SF*Electron_Reco_SF*Electron_Iso_SF*Electron_Trig_SF*L1CaloSF );
  	h_etauCutFlow->Fill(13, PileUpWeight*Electron_ID_SF*Electron_Reco_SF*Electron_Trig_SF*Electron_Iso_SF*L1CaloSF*TauRecoSF); 
  	h_etauCutFlow->Fill(14, PileUpWeight*Electron_ID_SF*Electron_Reco_SF*Electron_Trig_SF*Electron_Iso_SF*L1CaloSF*TauRecoSF*TauIDSF ); 
  	h_etauCutFlow->Fill(15, PileUpWeight*Electron_ID_SF*Electron_Reco_SF*Electron_Trig_SF*Electron_Iso_SF*L1CaloSF*TauRecoSF*TauIDSF*TauEleOLRSF );
  	h_etauCutFlow->Fill(16, PileUpWeight*Electron_ID_SF*Electron_Reco_SF*Electron_Trig_SF*Electron_Iso_SF*L1CaloSF*TauRecoSF*TauIDSF*TauEleOLRSF*MCWeight );
  	h_etauCutFlow->Fill(17, PileUpWeight*Electron_ID_SF*Electron_Reco_SF*Electron_Trig_SF*Electron_Iso_SF*L1CaloSF*TauRecoSF*TauIDSF*TauEleOLRSF*MCWeight*BTaggingSF );
  	h_etauCutFlow->Fill(18, PileUpWeight*Electron_ID_SF*Electron_Reco_SF*Electron_Trig_SF*Electron_Iso_SF*L1CaloSF*TauRecoSF*TauIDSF*TauEleOLRSF*MCWeight*JetSF );
  	h_etauCutFlow->Fill(19, EventWeight );

  } // end of if statement

  if( m_muonTau ){  
  	h_mutauCutFlow->Fill(7, PileUpWeight); 
  	h_mutauCutFlow->Fill(8, PileUpWeight*Muon_Eff_SF); 
  	h_mutauCutFlow->Fill(9, PileUpWeight*Muon_Eff_SF*Muon_IsoEff_SF );
  	h_mutauCutFlow->Fill(10, PileUpWeight*Muon_Eff_SF*Muon_IsoEff_SF*Muon_TrigEff_SF );
  	h_mutauCutFlow->Fill(11, PileUpWeight*Muon_Eff_SF*Muon_IsoEff_SF*Muon_TrigEff_SF*Muon_Track_SF );
  	h_mutauCutFlow->Fill(12, PileUpWeight*Muon_Eff_SF*Muon_IsoEff_SF*Muon_TrigEff_SF*Muon_Track_SF*effL1Calo );
  	h_mutauCutFlow->Fill(13, PileUpWeight*Muon_Eff_SF*Muon_IsoEff_SF*Muon_TrigEff_SF*Muon_Track_SF*effL1Calo*TauRecoSF); 
  	h_mutauCutFlow->Fill(14, PileUpWeight*Muon_Eff_SF*Muon_IsoEff_SF*Muon_TrigEff_SF*Muon_Track_SF*effL1Calo*TauRecoSF*TauIDSF ); 
  	h_mutauCutFlow->Fill(15, PileUpWeight*Muon_Eff_SF*Muon_IsoEff_SF*Muon_TrigEff_SF*Muon_Track_SF*effL1Calo*TauRecoSF*TauIDSF*TauEleOLRSF );
  	h_mutauCutFlow->Fill(16, PileUpWeight*Muon_Eff_SF*Muon_IsoEff_SF*Muon_TrigEff_SF*Muon_Track_SF*effL1Calo*TauRecoSF*TauIDSF*TauEleOLRSF*MCWeight );
  	h_mutauCutFlow->Fill(17, PileUpWeight*Muon_Eff_SF*Muon_IsoEff_SF*Muon_TrigEff_SF*Muon_Track_SF*effL1Calo*TauRecoSF*TauIDSF*TauEleOLRSF*MCWeight*BTaggingSF );
  	h_mutauCutFlow->Fill(18, PileUpWeight*Muon_Eff_SF*Muon_IsoEff_SF*Muon_TrigEff_SF*Muon_Track_SF*effL1Calo*TauRecoSF*TauIDSF*TauEleOLRSF*MCWeight*JetSF );
  	h_mutauCutFlow->Fill(19, EventWeight );

  } // end of if statement

  if( m_electronMuon ){ h_InvMass_emu->Fill( Propagator.Mag(), EventWeight );   h_L1CaloWeightedEvents_emu->Fill( 0.0, effL1Calo );}
  if( m_electronTau ){ h_InvMass_etau->Fill( Propagator.Mag(), EventWeight );   h_L1CaloWeightedEvents_etau->Fill( 0.0, effL1Calo ); }
  if( m_muonTau ){ h_InvMass_mutau->Fill( Propagator.Mag(), EventWeight );   h_L1CaloWeightedEvents_mutau->Fill( 0.0, effL1Calo ); }
  h_InvMass->Fill( Propagator.Mag(), EventWeight ); 
  h_InvMassTemplate->Fill( Propagator.Mag(), EventWeight ); 
  h_MuonPt->Fill( Muon.Pt() , EventWeight );         
  h_MuonEta->Fill( Muon.Eta() , EventWeight );         
  h_InvMassLinear->Fill( Propagator.Mag(), EventWeight  );
  if( m_electronMuon ) MassResolution_emu->Fill( bornMass,  fabs( bornMass - Propagator.Mag() )  );

  if( SignalSample == true ){ MassResolution->Fill( bornMass,  ( bornMass - Propagator.Mag() )/bornMass  ); MassResolutionLog->Fill( bornMass, ( bornMass - Propagator.Mag() )/bornMass  );}
   
  return EL::StatusCode::SUCCESS;
} // end of execute

