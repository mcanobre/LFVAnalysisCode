#include <MyAnalysis/MyxAODAnalysis.h>

EL::StatusCode MyxAODAnalysis :: histInitialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected

  lfvTree = new TTree("lfvTree","Z' lfv tree");
  //lfvTree->SetDirectory(lfvFile);
  SetTreeBranches();

  wk()->addOutput(lfvTree);


  TH1::SetDefaultSumw2(kTRUE);

  h_emuCutFlow = new TH1F("h_emuCutFlow","h_CutFlow",25,0,25);
  wk()->addOutput (h_emuCutFlow);

  h_etauCutFlow = new TH1F("h_etauCutFlow","h_CutFlow",25,0,25);
  wk()->addOutput (h_etauCutFlow);

  h_mutauCutFlow = new TH1F("h_mutauCutFlow","h_CutFlow",25,0,25);
  wk()->addOutput (h_mutauCutFlow);


  h_CutFlow = new TH1F("h_CutFlow","h_CutFlow",40,0,40);
  wk()->addOutput (h_CutFlow);

  h_CutFlow->GetXaxis()->SetBinLabel(1,"All events");
  h_CutFlow->GetXaxis()->SetBinLabel(2,"Event Cleaning");
  h_CutFlow->GetXaxis()->SetBinLabel(3,"Vertex");
  h_CutFlow->GetXaxis()->SetBinLabel(4,"Trigger");
  h_CutFlow->GetXaxis()->SetBinLabel(5,"Electron Author");
  h_CutFlow->GetXaxis()->SetBinLabel(6,"Electron pT");
  h_CutFlow->GetXaxis()->SetBinLabel(7,"Electron Eta");
  h_CutFlow->GetXaxis()->SetBinLabel(8,"Electron OQ");
  h_CutFlow->GetXaxis()->SetBinLabel(9,"Electron Likelihood ID");
  h_CutFlow->GetXaxis()->SetBinLabel(10,"Electron d0");
  h_CutFlow->GetXaxis()->SetBinLabel(11,"Electron Iso");
  h_CutFlow->GetXaxis()->SetBinLabel(12,"Muon pT");
  h_CutFlow->GetXaxis()->SetBinLabel(13,"Muon MCP");
  h_CutFlow->GetXaxis()->SetBinLabel(14,"Muon MS Hits");
  h_CutFlow->GetXaxis()->SetBinLabel(15,"Muon d0");
  h_CutFlow->GetXaxis()->SetBinLabel(16,"Muon z0");
  h_CutFlow->GetXaxis()->SetBinLabel(17,"Muon Iso");
  h_CutFlow->GetXaxis()->SetBinLabel(18,"3rd Tight Lepton Veto");
  h_CutFlow->GetXaxis()->SetBinLabel(19,"3rd Loose Lepton Veto");
  h_CutFlow->GetXaxis()->SetBinLabel(20,"Trigger matched");
  h_CutFlow->GetXaxis()->SetBinLabel(21,"Opposite Charge Leptons");
  h_CutFlow->GetXaxis()->SetBinLabel(22,"Back to Back Leptons");
  h_CutFlow->GetXaxis()->SetBinLabel(23,"b-Jet veto");
  h_CutFlow->GetXaxis()->SetBinLabel(24,"Pileup Weight");
  h_CutFlow->GetXaxis()->SetBinLabel(25,"Electron Reco SF");
  h_CutFlow->GetXaxis()->SetBinLabel(26,"Electron ID SF");
  h_CutFlow->GetXaxis()->SetBinLabel(27,"Electron Trig SF");
  h_CutFlow->GetXaxis()->SetBinLabel(28,"Electron Iso SF");
  h_CutFlow->GetXaxis()->SetBinLabel(29,"Muon Reco SF");
  h_CutFlow->GetXaxis()->SetBinLabel(30,"Muon Iso SF");
  h_CutFlow->GetXaxis()->SetBinLabel(31,"Muon Trig SF");
  h_CutFlow->GetXaxis()->SetBinLabel(32,"MCWeight");
  h_CutFlow->GetXaxis()->SetBinLabel(33,"Event Weight");

  h_ElectronCutFlow = new TH1F("h_ElectronCutFlow","h_CutFlow",15,0,15);
  wk()->addOutput (h_ElectronCutFlow);

  h_ElectronCutFlow->GetXaxis()->SetBinLabel(1,"All electrons");
  h_ElectronCutFlow->GetXaxis()->SetBinLabel(2,"Event Cleaning");
  h_ElectronCutFlow->GetXaxis()->SetBinLabel(3,"Vertex");
  h_ElectronCutFlow->GetXaxis()->SetBinLabel(4,"Trigger");
  h_ElectronCutFlow->GetXaxis()->SetBinLabel(5,"pre-selection");
  h_ElectronCutFlow->GetXaxis()->SetBinLabel(6,"Author");
  h_ElectronCutFlow->GetXaxis()->SetBinLabel(7,"pT");
  h_ElectronCutFlow->GetXaxis()->SetBinLabel(8,"Eta");
  h_ElectronCutFlow->GetXaxis()->SetBinLabel(9,"OQ");
  h_ElectronCutFlow->GetXaxis()->SetBinLabel(10,"OLR");
  h_ElectronCutFlow->GetXaxis()->SetBinLabel(11,"Likelihood ID");
  h_ElectronCutFlow->GetXaxis()->SetBinLabel(12,"Iso");
  
  h_PhotonCutFlow = new TH1F("h_PhotonCutFlow","h_CutFlow",15,0,15);
  wk()->addOutput (h_PhotonCutFlow);

  h_TauCutFlow = new TH1F("h_TauCutFlow","h_CutFlow",15,0,15);
  wk()->addOutput (h_TauCutFlow);

  h_TauCutFlow->GetXaxis()->SetBinLabel(1,"All electrons");
  h_TauCutFlow->GetXaxis()->SetBinLabel(2,"Event Cleaning");
  h_TauCutFlow->GetXaxis()->SetBinLabel(3,"Vertex");
  h_TauCutFlow->GetXaxis()->SetBinLabel(4,"Trigger");
  h_TauCutFlow->GetXaxis()->SetBinLabel(5,"pre-selection");
  h_TauCutFlow->GetXaxis()->SetBinLabel(6,"pT");
  h_TauCutFlow->GetXaxis()->SetBinLabel(7,"Eta < 2.47 crack");
  h_TauCutFlow->GetXaxis()->SetBinLabel(8,"nTracks");
  h_TauCutFlow->GetXaxis()->SetBinLabel(9,"Charge");
  h_TauCutFlow->GetXaxis()->SetBinLabel(10,"JetBDT");
  h_TauCutFlow->GetXaxis()->SetBinLabel(11,"ELEOR");
  h_TauCutFlow->GetXaxis()->SetBinLabel(12,"Tau Selector");
  h_TauCutFlow->GetXaxis()->SetBinLabel(13,"MUOR");

  h_MuonCutFlow = new TH1F("h_MuonCutFlow","h_CutFlow",15,0,15);
  wk()->addOutput (h_MuonCutFlow);

  h_MuonCutFlow->GetXaxis()->SetBinLabel(1,"All Muons");
  h_MuonCutFlow->GetXaxis()->SetBinLabel(2,"Event Cleaning");
  h_MuonCutFlow->GetXaxis()->SetBinLabel(3,"Vertex");
  h_MuonCutFlow->GetXaxis()->SetBinLabel(4,"Trigger");
  h_MuonCutFlow->GetXaxis()->SetBinLabel(5,"Pre-selection");
  h_MuonCutFlow->GetXaxis()->SetBinLabel(6,"Combined");
  h_MuonCutFlow->GetXaxis()->SetBinLabel(7,"pT");
  h_MuonCutFlow->GetXaxis()->SetBinLabel(8,"MCP Hits");
  h_MuonCutFlow->GetXaxis()->SetBinLabel(9,"MS Hits");
  h_MuonCutFlow->GetXaxis()->SetBinLabel(10,"d0");
  h_MuonCutFlow->GetXaxis()->SetBinLabel(12,"z0");
  h_MuonCutFlow->GetXaxis()->SetBinLabel(13,"Isolation");

  h_JetCutFlow = new TH1F("h_JetCutFlow","h_CutFlow",15,0,15);
  wk()->addOutput (h_JetCutFlow);

  h_JetCutFlow->GetXaxis()->SetBinLabel(1,"All Jets");
  h_JetCutFlow->GetXaxis()->SetBinLabel(2,"Event Cleaning");
  h_JetCutFlow->GetXaxis()->SetBinLabel(3,"Vertex");
  h_JetCutFlow->GetXaxis()->SetBinLabel(4,"Trigger");
  h_JetCutFlow->GetXaxis()->SetBinLabel(5,"pre-selection");
  h_JetCutFlow->GetXaxis()->SetBinLabel(6,"pT");
  h_JetCutFlow->GetXaxis()->SetBinLabel(7,"Eta");
  h_JetCutFlow->GetXaxis()->SetBinLabel(8,"JVT");
  h_JetCutFlow->GetXaxis()->SetBinLabel(9,"b-tagging");

  h_MuonPt = new TH1F("h_MuonPt","",50,0,2500);
  wk()->addOutput (h_MuonPt);

  h_MuonIso = new TH1F("h_MuonIso","",50,0,3);
  wk()->addOutput (h_MuonIso);

  h_MuonEta = new TH1F("h_MuonEta","",30,-3,3);
  wk()->addOutput (h_MuonEta);

  h_InvMassLinear = new TH1D("h_InvmassLinear","",120,0,6000);
  wk()->addOutput (h_InvMassLinear);

  h_muAvg = new TH1D("h_muAvg","",50,0,50);
  wk()->addOutput (h_muAvg);

  h_muAvg_Reweight = new TH1D("h_muAvg_Reweight","",50,0,50);
  wk()->addOutput (h_muAvg_Reweight);

  h_muAct = new TH1D("h_muAct","",50,0,50);
  wk()->addOutput (h_muAct);

  h_muAct_Reweight = new TH1D("h_muAct_Reweight","",50,0,50);
  wk()->addOutput (h_muAct_Reweight);

  h_L1CaloWeightedEvents_emu = new TH1D("h_L1CaloWeightedEvents_emu", "", 1,0,1);
  wk()->addOutput (h_L1CaloWeightedEvents_emu);

  h_L1CaloWeightedEvents_etau = new TH1D("h_L1CaloWeightedEvents_etau", "", 1,0,1);
  wk()->addOutput (h_L1CaloWeightedEvents_etau);

  h_L1CaloWeightedEvents_mutau = new TH1D("h_L1CaloWeightedEvents_mutau", "", 1,0,1);
  wk()->addOutput (h_L1CaloWeightedEvents_mutau);
   
  h_Ae_emu = new TH1F("h_Ae_emu", "", 1,0,1);
  wk()->addOutput (h_Ae_emu);

  h_Ae_etau = new TH1F("h_Ae_etau", "", 1,0,1);
  wk()->addOutput (h_Ae_etau);

  h_Ae_mutau = new TH1F("h_Ae_mutau", "", 1,0,1);
  wk()->addOutput (h_Ae_mutau);


  int nxbins = 40; int nxbins2 = 35;
  double xmin = 80;
  double xmax = 10000;
  double logxmin = TMath::Log10(xmin);
  double logxmax = TMath::Log10(xmax);
  double binwidth = (logxmax-logxmin)/double(nxbins);
  double xbins[41]; double xbins2[36];
  xbins[0] = xmin;
  for (int i=1;i<=nxbins;i++){
    xbins[i] = TMath::Power(10,logxmin+i*binwidth);
    if( i > 5 ){ xbins2[i-6] = TMath::Power(10,logxmin+i*binwidth); }
  } // end of for loop

  h_InvMass = new TH1F("h_InvMass","",nxbins,xbins);
  wk()->addOutput (h_InvMass);

  h_InvMass_emu = new TH1F("h_InvMass_emu","",nxbins,xbins);
  wk()->addOutput (h_InvMass_emu);

  h_InvMass_etau = new TH1F("h_InvMass_etau","",nxbins,xbins);
  wk()->addOutput (h_InvMass_etau);

  h_InvMass_mutau = new TH1F("h_InvMass_mutau","",nxbins,xbins);
  wk()->addOutput (h_InvMass_mutau);

  h_BornMass = new TH1F("h_BornMass","",nxbins,xbins);
  wk()->addOutput (h_BornMass);

  h_BornMassLinear = new TH1F("h_BornMassLinear","",1000,0,10000);
  wk()->addOutput (h_BornMassLinear);

  h_InvMassTemplate = new TH1F("h_InvMassTemplate","",nxbins2,xbins2);
  wk()->addOutput (h_InvMassTemplate);

  h_DeltaPhi = new TH1F("h_DeltaPhi","",34,0,3.1415);
  wk()->addOutput (h_DeltaPhi);

  MassResolution = new TProfile("MassResolution","",60,0,6000, 0.0,1.0); 
  wk()->addOutput (MassResolution);  

  MassResolutionLog = new TProfile("MassResolutionLog","",nxbins,xbins, 0.0,1.0); 
  wk()->addOutput (MassResolutionLog);  

  const int nPoints = 34;
  double MassPoints[nPoints] = {120, 150, 180, 220, 260, 300, 350, 400, 450, 500, 600, 700, 800, 900, 1000, 1100, 1200, 1300, 1400, 1500, 1600, 1700, 1800, 1900, 2000, 2200, 2400, 2600, 2800, 3000, 3500, 4000, 4500, 5000};

  MassResolution_emu = new TProfile("MassResolution_emu","",nPoints+1, MassPoints, 0.0,100.0); 
  wk()->addOutput (MassResolution_emu);  

  // Main Kinematics
  h_Electron_Pt = new TH1D("h_Electron_Pt","h_Electron_Pt",1200,0,6000);
  wk()->addOutput (h_Electron_Pt);

  h_Electron_Pt_BTrig = new TH1D("Eff/h_Electron_Pt_BTrig","h_Electron_Pt_BTrig",1200,0,6000);
  wk()->addOutput (h_Electron_Pt_BTrig);

  h_Electron_Pt_ATrig = new TH1D("Eff/h_Electron_Pt_ATrig","h_Electron_Pt_ATrig",1200,0,6000);
  wk()->addOutput (h_Electron_Pt_ATrig);

  h_Electron_PtBefore = new TH1D("Eff/h_Electron_PtBefore","h_Electron_PtBefore",1200,0,6000);
  wk()->addOutput (h_Electron_PtBefore);

  h_Electron_PtAfter = new TH1D("Eff/h_Electron_PtAfter","h_Electron_PtAfter",1200,0,6000);
  wk()->addOutput (h_Electron_PtAfter);

  h_Electron_PtIso = new TH2D("h_Electron_PtIso","h_Electron_PtIso",1200,0,6000,80,-40,40);
  wk()->addOutput (h_Electron_PtIso);

  h_Electron_Eta = new TH1D("h_Electron_Eta","h_Electron_Eta",100,-5,5);
  wk()->addOutput (h_Electron_Eta);

  h_Electron_Phi = new TH1D("h_Electron_Phi","h_Electron_Phi",80,-4,4);
  wk()->addOutput (h_Electron_Phi);

  h_Electron_Iso_pT_Den = new TH1D("h_Electron_Iso_pT_Den", "", 150, 0, 1500 );
  wk()->addOutput (h_Electron_Iso_pT_Den);

  h_Electron_Iso_pT_Num = new TH1D("h_Electron_Iso_pT_Num", "",150, 0,1500 );
  wk()->addOutput (h_Electron_Iso_pT_Num);

  h_Electron_ID_pT_Den = new TH1D("h_Electron_ID_pT_Den", "", 150, 0, 1500 );
  wk()->addOutput (h_Electron_ID_pT_Den);

  h_Electron_ID_pT_Num = new TH1D("h_Electron_ID_pT_Num", "",150, 0,1500 );
  wk()->addOutput (h_Electron_ID_pT_Num);

  h_Electron_ID_Eta_Den= new TH1D("h_Electron_ID_Eta_Den", "",50, -5, 5 );
  wk()->addOutput (h_Electron_ID_Eta_Den);

  h_Electron_ID_Eta_Num= new TH1D("h_Electron_ID_Eta_Num", "",50, -5, 5 );
  wk()->addOutput (h_Electron_ID_Eta_Num);

  h_Muon_ID_pT_Den= new TH1D("h_Muon_ID_pT_Den", "",150, 0,1500 );
  wk()->addOutput (h_Muon_ID_pT_Den);

  h_Muon_ID_pT_Num = new TH1D("h_Muon_ID_pT_Num", "",150, 0,1500 );
  wk()->addOutput (h_Muon_ID_pT_Num);

  h_Muon_Iso_pT_Den = new TH1D("h_Muon_Iso_pT_Den", "", 150, 0, 1500 );
  wk()->addOutput (h_Muon_Iso_pT_Den);

  h_Muon_Iso_pT_Num = new TH1D("h_Muon_Iso_pT_Num", "",150, 0,1500 );
  wk()->addOutput (h_Muon_Iso_pT_Num);

  h_Muon_ID_Eta_Den= new TH1D("h_Muon_ID_Eta_Den", "",50, -5, 5 );
  wk()->addOutput (h_Muon_ID_Eta_Den);

  h_Muon_ID_Eta_Num= new TH1D("h_Muon_ID_Eta_Num", "",50, -5, 5 );
  wk()->addOutput (h_Muon_ID_Eta_Num);

  // Electron ID Variables

  h_Electron_d0 = new TH1D("h_Electron_d0","h_Electron_d0",4000,-20,20);
  wk()->addOutput (h_Electron_d0);

  h_Electron_Pix = new TH1D("TrkAndShower/h_Electron_Pix","h_Electron_dPix",50,0,50);
  wk()->addOutput (h_Electron_Pix);

  h_Electron_SCT = new TH1D("TrkAndShower/h_Electron_SCT","h_Electron_SCT",50,0,50);
  wk()->addOutput (h_Electron_SCT);

  h_Electron_Reta = new TH1D("TrkAndShower/h_Electron_Reta","h_Electron_Reta",200,0,2);
  wk()->addOutput (h_Electron_Reta);

  h_Electron_Rphi = new TH1D("TrkAndShower/h_Electron_Rphi","h_Electron_Rphi",200,0,2);
  wk()->addOutput (h_Electron_Rphi);

  h_Electron_e277 = new TH1D("TrkAndShower/h_Electron_e277","h_Electron_e277",1000,0,5000);
  wk()->addOutput (h_Electron_e277);

  h_Electron_Rhad1 = new TH1D("TrkAndShower/h_Electron_Rhad1","h_Electron_Rhad1",200,-0.5,1.5);
  wk()->addOutput (h_Electron_Rhad1);

  h_Electron_Rhad = new TH1D("TrkAndShower/h_Electron_Rhad","h_Electron_Rhad",200,-0.5,1.5);
  wk()->addOutput (h_Electron_Rhad);

  h_Electron_weta1 = new TH1D("TrkAndShower/h_Electron_weta1","h_Electron_weta1",300,-1,2);
  wk()->addOutput (h_Electron_weta1);

  h_Electron_weta2 = new TH1D("TrkAndShower/h_Electron_weta2","h_Electron_weta2",2000,-0.1,0.1);
  wk()->addOutput (h_Electron_weta2);

  h_Electron_f1 = new TH1D("TrkAndShower/h_Electron_f1","h_Electron_f1",200,-1,1);
  wk()->addOutput (h_Electron_f1);

  h_Electron_f3 = new TH1D("TrkAndShower/h_Electron_f3","h_Electron_f3",200,-1,1);
  wk()->addOutput (h_Electron_f3);

  h_Electron_Eratio = new TH1D("TrkAndShower/h_Electron_Eratio","h_Electron_Eratio",200,0,2);
  wk()->addOutput (h_Electron_Eratio);

  h_Electron_d0After = new TH1D("TrkAndShower/h_Electron_d0After","h_Electron_d0After",4000,-20,20);
  wk()->addOutput (h_Electron_d0After);

  h_Electron_PixAfter = new TH1D("TrkAndShower/h_Electron_PixAfter","h_Electron_dPixAfter",50,0,50);
  wk()->addOutput (h_Electron_PixAfter);

  h_Electron_SCTAfter = new TH1D("TrkAndShower/h_Electron_SCTAfter","h_Electron_SCTAfter",50,0,50);
  wk()->addOutput (h_Electron_SCTAfter);

  h_Electron_RetaAfter = new TH1D("TrkAndShower/h_Electron_RetaAfter","h_Electron_RetaAfter",200,0,2);
  wk()->addOutput (h_Electron_RetaAfter);

  h_Electron_RphiAfter = new TH1D("TrkAndShower/h_Electron_RphiAfter","h_Electron_RphiAfter",200,0,2);
  wk()->addOutput (h_Electron_RphiAfter);

  h_Electron_e277After = new TH1D("TrkAndShower/h_Electron_e277After","h_Electron_e277After",1000,0,5000);
  wk()->addOutput (h_Electron_e277After);

  h_Electron_Rhad1After = new TH1D("TrkAndShower/h_Electron_Rhad1After","h_Electron_Rhad1After",200,-0.5,1.5);
  wk()->addOutput (h_Electron_Rhad1After);

  h_Electron_RhadAfter = new TH1D("TrkAndShower/h_Electron_RhadAfter","h_Electron_RhadAfter",200,-0.5,1.5);
  wk()->addOutput (h_Electron_RhadAfter);

  h_Electron_weta1After = new TH1D("TrkAndShower/h_Electron_weta1After","h_Electron_weta1After",300,-1,2);
  wk()->addOutput (h_Electron_weta1After);

  h_Electron_weta2After = new TH1D("TrkAndShower/h_Electron_weta2After","h_Electron_weta2After",2000,-0.1,0.1);
  wk()->addOutput (h_Electron_weta2After);

  h_Electron_f1After = new TH1D("TrkAndShower/h_Electron_f1After","h_Electron_f1After",200,-1,1);
  wk()->addOutput (h_Electron_f1After);

  h_Electron_f3After = new TH1D("TrkAndShower/h_Electron_f3After","h_Electron_f3After",200,-1,1);
  wk()->addOutput (h_Electron_f3After);

  h_Electron_EratioAfter = new TH1D("TrkAndShower/h_Electron_EratioAfter","h_Electron_EratioAfter",200,0,2);
  wk()->addOutput (h_Electron_EratioAfter);

  return EL::StatusCode::SUCCESS;
} // end of histInitialize

