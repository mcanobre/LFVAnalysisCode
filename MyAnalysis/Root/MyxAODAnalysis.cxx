#include <MyAnalysis/MyxAODAnalysis.h>


using namespace std;

// this is needed to distribute the algorithm to the workers
ClassImp(MyxAODAnalysis)

MyxAODAnalysis :: MyxAODAnalysis ()
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().

/*#include <MyAnalysis/meta_364156.h>
#include <MyAnalysis/meta_364157.h>
#include <MyAnalysis/meta_364158.h>
#include <MyAnalysis/meta_364159.h>
#include <MyAnalysis/meta_364160.h>
#include <MyAnalysis/meta_364161.h>
#include <MyAnalysis/meta_364162.h>
#include <MyAnalysis/meta_364163.h>
#include <MyAnalysis/meta_364164.h>
#include <MyAnalysis/meta_364165.h>
#include <MyAnalysis/meta_364166.h>
#include <MyAnalysis/meta_364167.h>
#include <MyAnalysis/meta_364168.h>
#include <MyAnalysis/meta_364169.h>
#include <MyAnalysis/meta_364170.h>
#include <MyAnalysis/meta_364171.h>
#include <MyAnalysis/meta_364172.h>
#include <MyAnalysis/meta_364173.h>
#include <MyAnalysis/meta_364174.h>
#include <MyAnalysis/meta_364175.h>
#include <MyAnalysis/meta_364176.h>
#include <MyAnalysis/meta_364177.h>
#include <MyAnalysis/meta_364178.h>
#include <MyAnalysis/meta_364179.h>
#include <MyAnalysis/meta_364180.h>
#include <MyAnalysis/meta_364181.h>
#include <MyAnalysis/meta_364182.h>
#include <MyAnalysis/meta_364183.h>
#include <MyAnalysis/meta_364184.h>
#include <MyAnalysis/meta_364185.h>
#include <MyAnalysis/meta_364186.h>
#include <MyAnalysis/meta_364187.h>
#include <MyAnalysis/meta_364188.h>
#include <MyAnalysis/meta_364189.h>
#include <MyAnalysis/meta_364190.h>
#include <MyAnalysis/meta_364191.h>
#include <MyAnalysis/meta_364192.h>
#include <MyAnalysis/meta_364193.h>
#include <MyAnalysis/meta_364194.h>
#include <MyAnalysis/meta_364195.h>
#include <MyAnalysis/meta_364196.h>
#include <MyAnalysis/meta_364197.h>*/

}



EL::StatusCode MyxAODAnalysis :: setupJob (EL::Job& job)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you

  // let's initialize the algorithm to use the xAODRootAccess package
  job.useXAOD ();
  //xAOD::Init(); // call before opening first file
  CHECK( "setupJob()", xAOD::Init() ); // call before opening first file

  return EL::StatusCode::SUCCESS;
}


EL::StatusCode MyxAODAnalysis :: fileExecute ()
{
  xAOD::TEvent* m_event = wk()->xaodEvent();
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  NewFile = true;
  // get the MetaData tree once a new file is opened, with
  TTree* MetaData = dynamic_cast<TTree*>(wk()->inputFile()->Get("MetaData"));
  if (!MetaData) {
    Error("fileExecute()", "MetaData not found! Exiting.");
    return EL::StatusCode::FAILURE;
  }
  MetaData->LoadTree(0);
  
  //check if file is from a DxAOD
  m_isDerivation = !MetaData->GetBranch("StreamAOD");
  
  /*if(m_isDerivation ){

    std::cout << "***** NOTE ***** You Are Running On A Derivation ***** NOTE *****" << std::endl;
    
    // check for corruption
    const xAOD::CutBookkeeperContainer* incompleteCBC = nullptr;
    if(!m_event->retrieveMetaInput(incompleteCBC, "IncompleteCutBookkeepers").isSuccess()){
      Error("initializeEvent()","Failed to retrieve IncompleteCutBookkeepers from MetaData! Exiting.");
      return EL::StatusCode::FAILURE;
    }
    if ( incompleteCBC->size() != 0 ) {
      Error("initializeEvent()","Found incomplete Bookkeepers! Check file for corruption.");
      return EL::StatusCode::FAILURE;
    }
    
    // Now, let's find the actual information
    const xAOD::CutBookkeeperContainer* completeCBC = 0;
    if(!m_event->retrieveMetaInput(completeCBC, "CutBookkeepers").isSuccess()){
      Error("initializeEvent()","Failed to retrieve CutBookkeepers from MetaData! Exiting.");
      return EL::StatusCode::FAILURE;
    }
    
    // Find the smallest cycle number, the original first processing step/cycle
    int minCycle = 10000;
    for ( auto cbk : *completeCBC ) {
      if ( ! cbk->name().empty()  && minCycle > cbk->cycle() ){ minCycle = cbk->cycle(); }
    }
    
    // Now, find the right one that contains all the needed info...
    const xAOD::CutBookkeeper* allEventsCBK=0;
    for ( auto cbk :  *completeCBC ) {
      if ( minCycle == cbk->cycle() && cbk->name() == "AllExecutedEvents" ){
        allEventsCBK = cbk;
        break;
      }
    }
    
    m_nEventsProcessed  = allEventsCBK->nAcceptedEvents();
    m_sumOfWeights        = allEventsCBK->sumOfEventWeights();
    m_sumOfWeightsSquared = allEventsCBK->sumOfEventWeightsSquared();

  } // end of is derivation   */

  return EL::StatusCode::SUCCESS;
   
} // end of if file execute



EL::StatusCode MyxAODAnalysis :: changeInput (bool firstFile)
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  return EL::StatusCode::SUCCESS;
}






EL::StatusCode MyxAODAnalysis :: postExecute ()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODAnalysis :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.
  std::cout << std::setprecision(9);

  xAOD::TEvent* event = wk()->xaodEvent();

  std::cout << "Event Cut Flow: " << std::endl;
  std::cout << "All:     " << h_CutFlow->GetBinContent(1) << std::endl;
  std::cout << "Event Cleaning:     " << h_CutFlow->GetBinContent(2) << std::endl;
  std::cout << "Vertex:     " << h_CutFlow->GetBinContent(3) << std::endl;
  std::cout << "Trigger: " << h_CutFlow->GetBinContent(4) << std::endl;
  std::cout << "Bad Jet Event:     " <<h_CutFlow->GetBinContent(18) << std::endl;
  std::cout << "3rd Tight Lepton Veto:     " <<h_CutFlow->GetBinContent(19) << std::endl;
  std::cout << "3rd Tight Lepton Veto:     " <<h_CutFlow->GetBinContent(20) << std::endl;
  std::cout << "3rd Loose Lepton Veto:     " <<h_CutFlow->GetBinContent(21) << std::endl;
  std::cout << "One trigger matched lepton:     " <<h_CutFlow->GetBinContent(22) << std::endl;
  std::cout << "Opposite Charge Leptons:     " <<h_CutFlow->GetBinContent(23) << std::endl;
  std::cout << "Back to Back Leptons:     " <<h_CutFlow->GetBinContent(24) << std::endl;
  std::cout << "b-jet veto:     " <<h_CutFlow->GetBinContent(25) << std::endl;
  std::cout << "MET < 30 GeV:     " <<h_CutFlow->GetBinContent(26) << std::endl;
  std::cout << "*****************************" << std::endl;
  std::cout << "*****************************" << std::endl;

  std::cout << "emu Cut Flow: " << std::endl;
  std::cout << "All " <<h_emuCutFlow->GetBinContent(1) << std::endl;
  std::cout << "Lepton " <<h_emuCutFlow->GetBinContent(2) << std::endl;
  std::cout << "One trigger matched lepton:     " <<h_emuCutFlow->GetBinContent(3) << std::endl;
  std::cout << "Opposite Charge Leptons:     " <<h_emuCutFlow->GetBinContent(4) << std::endl;
  std::cout << "Back to Back Leptons:     " <<h_emuCutFlow->GetBinContent(5) << std::endl;
  std::cout << "b-jet veto:     " <<h_emuCutFlow->GetBinContent(6) << std::endl;
  std::cout << "MET < 30 GeV:     " <<h_emuCutFlow->GetBinContent(7) << std::endl;
  std::cout << "Pileup Weight:     " <<h_emuCutFlow->GetBinContent(8) << std::endl;
  std::cout << "Electron Reco SF:     " <<h_emuCutFlow->GetBinContent(9) << std::endl;
  std::cout << "Electron ID SF:     " <<h_emuCutFlow->GetBinContent(10) << std::endl;
  std::cout << "Electron Iso SF:     " <<h_emuCutFlow->GetBinContent(11) << std::endl;
  std::cout << "Electron Trig SF:     " <<h_emuCutFlow->GetBinContent(12) << std::endl;
  std::cout << "Electron L1Calo:     " <<h_emuCutFlow->GetBinContent(13) << std::endl;
  std::cout << "Muon Reco SF:     " <<h_emuCutFlow->GetBinContent(14) << std::endl;
  std::cout << "Muon Iso SF:     " <<h_emuCutFlow->GetBinContent(15) << std::endl;
  std::cout << "Muon Trig SF:     " <<h_emuCutFlow->GetBinContent(16) << std::endl;
  std::cout << "Muon Track SF:     " <<h_emuCutFlow->GetBinContent(17) << std::endl;
  std::cout << "MCWeight:     " <<h_emuCutFlow->GetBinContent(18) << std::endl;
  std::cout << "BTagging SF:     " <<h_emuCutFlow->GetBinContent(19) << std::endl;
  std::cout << "JVT SF:     " <<h_emuCutFlow->GetBinContent(20) << std::endl;
  std::cout << "EventWeight:     " <<h_emuCutFlow->GetBinContent(21) << std::endl;
  std::cout << "*****************************" << std::endl;
  std::cout << "*****************************" << std::endl;

  std::cout << "etau Cut Flow: " << std::endl;
  std::cout << "All " <<h_etauCutFlow->GetBinContent(1) << std::endl;
  std::cout << "Lepton " <<h_etauCutFlow->GetBinContent(2) << std::endl;
  std::cout << "One trigger matched lepton:     " <<h_etauCutFlow->GetBinContent(3) << std::endl;
  std::cout << "Opposite Charge Leptons:     " <<h_etauCutFlow->GetBinContent(4) << std::endl;
  std::cout << "Back to Back Leptons:     " <<h_etauCutFlow->GetBinContent(5) << std::endl;
  std::cout << "b-jet veto:     " <<h_etauCutFlow->GetBinContent(6) << std::endl;
  std::cout << "MET < 30 GeV:     " <<h_etauCutFlow->GetBinContent(7) << std::endl;
  std::cout << "Pileup Weight:     " <<h_etauCutFlow->GetBinContent(8) << std::endl;
  std::cout << "Electron Reco SF:     " <<h_etauCutFlow->GetBinContent(9) << std::endl;
  std::cout << "Electron ID SF:     " <<h_etauCutFlow->GetBinContent(10) << std::endl;
  std::cout << "Electron Iso SF:     " <<h_etauCutFlow->GetBinContent(11) << std::endl;
  std::cout << "Electron Trig SF:     " <<h_etauCutFlow->GetBinContent(12) << std::endl;
  std::cout << "Electron L1Calo:     " <<h_etauCutFlow->GetBinContent(13) << std::endl;
  std::cout << "Tau Reco SF:     " <<h_etauCutFlow->GetBinContent(14) << std::endl;
  std::cout << "Tau ID SF:     " <<h_etauCutFlow->GetBinContent(15) << std::endl;
  std::cout << "Tau EleOLR SF:     " <<h_etauCutFlow->GetBinContent(16) << std::endl;
  std::cout << "MCWeight:     " <<h_etauCutFlow->GetBinContent(17) << std::endl;
  std::cout << "BTagging SF:     " <<h_etauCutFlow->GetBinContent(18) << std::endl;
  std::cout << "JVT SF:     " <<h_etauCutFlow->GetBinContent(19) << std::endl;
  std::cout << "EventWeight:     " <<h_etauCutFlow->GetBinContent(20) << std::endl;

  std::cout << "*****************************" << std::endl;
  std::cout << "*****************************" << std::endl;

  std::cout << "mutau Cut Flow: " << std::endl;
  std::cout << "All " <<h_mutauCutFlow->GetBinContent(1) << std::endl;
  std::cout << "Lepton " <<h_mutauCutFlow->GetBinContent(2) << std::endl;
  std::cout << "One trigger matched lepton:     " <<h_mutauCutFlow->GetBinContent(3) << std::endl;
  std::cout << "Opposite Charge Leptons:     " <<h_mutauCutFlow->GetBinContent(4) << std::endl;
  std::cout << "Back to Back Leptons:     " <<h_mutauCutFlow->GetBinContent(5) << std::endl;
  std::cout << "b-jet veto:     " <<h_mutauCutFlow->GetBinContent(6) << std::endl;
  std::cout << "MET < 30 GeV:     " <<h_mutauCutFlow->GetBinContent(7) << std::endl;
  std::cout << "Pileup Weight:     " <<h_mutauCutFlow->GetBinContent(8) << std::endl;
  std::cout << "Muon Reco SF:     " <<h_mutauCutFlow->GetBinContent(9) << std::endl;
  std::cout << "Muon Iso SF:     " <<h_mutauCutFlow->GetBinContent(10) << std::endl;
  std::cout << "Muon Trig SF:     " <<h_mutauCutFlow->GetBinContent(11) << std::endl;
  std::cout << "Muon Track SF:     " <<h_mutauCutFlow->GetBinContent(12) << std::endl;
  std::cout << "Electron L1Calo:     " <<h_mutauCutFlow->GetBinContent(13) << std::endl;
  std::cout << "Tau Reco SF:     " <<h_mutauCutFlow->GetBinContent(14) << std::endl;
  std::cout << "Tau ID SF:     " <<h_mutauCutFlow->GetBinContent(15) << std::endl;
  std::cout << "Tau EleOLR SF:     " <<h_mutauCutFlow->GetBinContent(16) << std::endl;
  std::cout << "MCWeight:     " <<h_mutauCutFlow->GetBinContent(17) << std::endl;
  std::cout << "BTagging SF:     " <<h_mutauCutFlow->GetBinContent(18) << std::endl;
  std::cout << "JVT SF:     " <<h_mutauCutFlow->GetBinContent(19) << std::endl;
  std::cout << "EventWeight:     " <<h_mutauCutFlow->GetBinContent(20) << std::endl;
  std::cout << "*****************************" << std::endl;
  std::cout << "*****************************" << std::endl;

  std::cout << "Electron Cut Flow: " << std::endl;
  std::cout << "Start     " << h_ElectronCutFlow->GetBinContent(1) << std::endl;
  std::cout << "Event Cleaning     " << h_ElectronCutFlow->GetBinContent(2) << std::endl;
  std::cout << "Vertex     " << h_ElectronCutFlow->GetBinContent(3) << std::endl;
  std::cout << "Trigger     " << h_ElectronCutFlow->GetBinContent(4) << std::endl;
  std::cout << "All after pre-selection    " << h_ElectronCutFlow->GetBinContent(5) << std::endl;
  std::cout << "Author    " << h_ElectronCutFlow->GetBinContent(6) << std::endl;
  std::cout << "pT     " << h_ElectronCutFlow->GetBinContent(7) << std::endl;
  std::cout << "Eta     " << h_ElectronCutFlow->GetBinContent(8) << std::endl;  
  std::cout << "Object Quality     " << h_ElectronCutFlow->GetBinContent(9) << std::endl;
  std::cout << "Likelihood ID     " << h_ElectronCutFlow->GetBinContent(10) << std::endl;
  std::cout << "Tracking     " << h_ElectronCutFlow->GetBinContent(11) << std::endl; 
  std::cout << "L1Calo     " << h_ElectronCutFlow->GetBinContent(12) << std::endl; 
  std::cout << "OLR     " << h_ElectronCutFlow->GetBinContent(13) << std::endl;
  std::cout << "Isolation     " << h_ElectronCutFlow->GetBinContent(14) << std::endl;
  std::cout << "*****************************" << std::endl;
  std::cout << "*****************************" << std::endl;

  std::cout << "Muon Cut Flow: " << std::endl;
  std::cout << "Start     " << h_MuonCutFlow->GetBinContent(1) << std::endl;
  std::cout << "Event Cleaning     " << h_MuonCutFlow->GetBinContent(2) << std::endl;
  std::cout << "Vertex     " << h_MuonCutFlow->GetBinContent(3) << std::endl;
  std::cout << "Trigger     " << h_MuonCutFlow->GetBinContent(4) << std::endl;
  std::cout << "All after pre-selection    " << h_MuonCutFlow->GetBinContent(5) << std::endl;
  std::cout << "Combined   " << h_MuonCutFlow->GetBinContent(6) << std::endl;
  std::cout << "pT     " << h_MuonCutFlow->GetBinContent(7) << std::endl;
  std::cout << "MCP Hits     " << h_MuonCutFlow->GetBinContent(8) << std::endl;
  std::cout << "MS Hits     " << h_MuonCutFlow->GetBinContent(9) << std::endl;
  std::cout << "d0    " << h_MuonCutFlow->GetBinContent(10) << std::endl;
  std::cout << "z0     " << h_MuonCutFlow->GetBinContent(11) << std::endl;
  std::cout << "Overlaps     " << h_MuonCutFlow->GetBinContent(12) << std::endl;
  std::cout << "Isolation     " << h_MuonCutFlow->GetBinContent(13) << std::endl;
  std::cout << "*****************************" << std::endl;
  std::cout << "*****************************" << std::endl;

  std::cout << "Tau Cut Flow: " << std::endl;
  std::cout << "Start     " << h_TauCutFlow->GetBinContent(1) << std::endl;
  std::cout << "Event Cleaning     " << h_TauCutFlow->GetBinContent(2) << std::endl;
  std::cout << "Vertex     " << h_TauCutFlow->GetBinContent(3) << std::endl;
  std::cout << "Trigger     " << h_TauCutFlow->GetBinContent(4) << std::endl;
  std::cout << "All after pre-sel    " << h_TauCutFlow->GetBinContent(5) << std::endl;
  std::cout << "pT: " << h_TauCutFlow->GetBinContent(6) << std::endl;
  std::cout << "Eta " << h_TauCutFlow->GetBinContent(7) << std::endl;
  std::cout << "Charge " << h_TauCutFlow->GetBinContent(8) << std::endl;
  std::cout << "nTracks " << h_TauCutFlow->GetBinContent(9) << std::endl;
  std::cout << "ID  " << h_TauCutFlow->GetBinContent(10) << std::endl;
  std::cout << "Selector No OLR  " << h_TauCutFlow->GetBinContent(11) << std::endl;
  std::cout << "Selector With OLR  " << h_TauCutFlow->GetBinContent(12) << std::endl;
  std::cout << "Overlaps  " << h_TauCutFlow->GetBinContent(13) << std::endl;
  std::cout << "*****************************" << std::endl;
  std::cout << "*****************************" << std::endl;

  std::cout << "Photon Cut Flow: " << std::endl;
  std::cout << "Start     " << h_PhotonCutFlow->GetBinContent(1) << std::endl;
  std::cout << "Event Cleaning     " << h_PhotonCutFlow->GetBinContent(2) << std::endl;
  std::cout << "Vertex     " << h_PhotonCutFlow->GetBinContent(3) << std::endl;
  std::cout << "Trigger     " << h_PhotonCutFlow->GetBinContent(4) << std::endl;
  std::cout << "pT    " << h_PhotonCutFlow->GetBinContent(5) << std::endl;
  std::cout << "Eta " << h_PhotonCutFlow->GetBinContent(6) << std::endl;
  std::cout << "Author " << h_PhotonCutFlow->GetBinContent(7) << std::endl;
  std::cout << "Object Quality " << h_PhotonCutFlow->GetBinContent(8) << std::endl;
  std::cout << "Clean " << h_PhotonCutFlow->GetBinContent(9) << std::endl;
  std::cout << "*****************************" << std::endl;
  std::cout << "*****************************" << std::endl;

  std::cout << "Jet Cut Flow: " << std::endl;
  std::cout << "Start     " << h_JetCutFlow->GetBinContent(1) << std::endl;
  std::cout << "Event Cleaning     " << h_JetCutFlow->GetBinContent(2) << std::endl;
  std::cout << "Vertex     " << h_JetCutFlow->GetBinContent(3) << std::endl;
  std::cout << "Trigger     " << h_JetCutFlow->GetBinContent(4) << std::endl;
  std::cout << "All after pre-selection    " << h_JetCutFlow->GetBinContent(5) << std::endl;
  std::cout << "pT   " << h_JetCutFlow->GetBinContent(6) << std::endl;
  std::cout << "Eta     " << h_JetCutFlow->GetBinContent(7) << std::endl;
  std::cout << "EleOLR    " << h_JetCutFlow->GetBinContent(8) << std::endl;
  std::cout << "TauOLR    " << h_JetCutFlow->GetBinContent(9) << std::endl;
  std::cout << "MuonOLR    " << h_JetCutFlow->GetBinContent(10) << std::endl;
  std::cout << "JVT     " << h_JetCutFlow->GetBinContent(11) << std::endl;
  std::cout << "Cleaning     " << h_JetCutFlow->GetBinContent(12) << std::endl;
  std::cout << "mv2c20    " << h_JetCutFlow->GetBinContent(13) << std::endl;
  std::cout << "*****************************" << std::endl;
  std::cout << "*****************************" << std::endl;

  std::cout << "Number of emu events "<<NumberElectronMuon<< std::endl;
  std::cout << "Number of etau events "<<NumberElectronTau<< std::endl;
  std::cout << "Number of mutau events "<<NumberMuonTau<< std::endl;

  std::cout << "Number of emu events MET < 30 "<<NumberElectronMuon_MET<< std::endl;
  std::cout << "Number of etau events MET < 30 "<<NumberElectronTau_MET<< std::endl;
  std::cout << "Number of mutau events MET < 30 "<<NumberMuonTau_MET<< std::endl;

  Info("finalize()", "Number of clean events = %i", m_numCleanEvents);

  std::cout<<"Total number good muons "<<TotalNumberGoodMuons<<" Total number good loose muons "<<TotalNumberGoodLooseMuons<<std::endl;
  std::cout<<"Total number good electrons "<<TotalNumberGoodElectrons<<" Total number good loose electrons "<<TotalNumberGoodLooseElectrons<<std::endl;

  //m_Pileup->WriteToFile();

  /*if(m_muonCalibrationAndSmearingTool_2015){
    delete m_muonCalibrationAndSmearingTool_2015;
    m_muonCalibrationAndSmearingTool_2015 = 0;
  }

  if(m_muonCalibrationAndSmearingTool_2016){
    delete m_muonCalibrationAndSmearingTool_2016;
    m_muonCalibrationAndSmearingTool_2016 = 0;
  }*/

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode MyxAODAnalysis :: histFinalize ()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.

  h_Ae_emu->SetBinContent( 1, h_L1CaloWeightedEvents_emu->GetBinContent(1)/h_CutFlow->GetBinContent(1) );
  h_Ae_etau->SetBinContent( 1, h_L1CaloWeightedEvents_etau->GetBinContent(1)/h_CutFlow->GetBinContent(1) );
  h_Ae_mutau->SetBinContent( 1, h_L1CaloWeightedEvents_mutau->GetBinContent(1)/h_CutFlow->GetBinContent(1) );

  return EL::StatusCode::SUCCESS;
}

/*double MyxAODAnalysis :: GetTauRecoSF(const xAOD::IParticle *p){

  float m_sf = 1;

  const xAOD::TauJet* tau = dynamic_cast<const xAOD::TauJet*> (p);
  if( TauEffCorrTool->getEfficiencyScaleFactor(tau, m_sf) == CP::CorrectionCode::Error ){
     Info( "GetTauRecoSF()", "TauEfficiencyScaleFactors returns Error CorrectionCode in getting SF");
  }

  if( m_debug ) Info( "GetTauRecoSF()" , "Tau Reco SF = %f ", m_sf );
  return (double)m_sf;

} // end of GetMuonRecoSF*/

double MyxAODAnalysis :: GetMuonTrackSF(const xAOD::IParticle *p){

  float m_sf = 1;

  const xAOD::Muon* mu = dynamic_cast<const xAOD::Muon*> (p);
  if( m_muTTVASF->getEfficiencyScaleFactor(*mu,m_sf) == CP::CorrectionCode::Error ){
     Info( "GetMuonTrackSF()", "MuonEfficiencyScaleFactors returns Error CorrectionCode in getting SF");
  }

  if( m_debug ) Info( "GetMuonRecoSF()" , "Muon Track SF = %f ", m_sf );
  return (double)m_sf;

} // end of GetMuonRecoSF

double MyxAODAnalysis :: GetMuonRecoSF(const xAOD::IParticle *p){

  float m_sf = 1;

  const xAOD::Muon* mu = dynamic_cast<const xAOD::Muon*> (p);
  if( m_muRecoSF->getEfficiencyScaleFactor(*mu,m_sf) == CP::CorrectionCode::Error ){
     Info( "GetMuonRecoSF()", "MuonEfficiencyScaleFactors returns Error CorrectionCode in getting SF");
  }

  if( m_debug ) Info( "GetMuonRecoSF()" , "Muon Reco SF = %f ", m_sf );
  return (double)m_sf;

} // end of GetMuonRecoSF

double MyxAODAnalysis :: GetMuonIsoSF(const xAOD::IParticle *p){

  float m_sf = 1;
  const xAOD::Muon* mu = dynamic_cast<const xAOD::Muon*> (p);
  if( m_muIsoSF->getEfficiencyScaleFactor(*mu,m_sf) == CP::CorrectionCode::Error ){
     Info( "GetMuonIsoSF()", "MuonEfficiencyScaleFactors returns Error CorrectionCode in getting SF");
  }

  if( m_debug ) Info( "GetMuonIsoSF()" , "Muon Iso SF = %f ", m_sf );
  return (double)m_sf;

} // end of GetMuonIsoSF

std::string MyxAODAnalysis :: GetFileName(std::string name){

  std::string delim = "/";
  std::string m_InName;
  size_t pos = 0;
  std::vector<std::string> m_file;
  while( (pos = name.find(delim)) != std::string::npos ){
    m_file.push_back( name.substr(0, pos) );
    name.erase(0, pos + delim.length());
  }

  m_InName = name;
  return m_InName;

}

double MyxAODAnalysis :: GetElectronIDTightSF(const xAOD::IParticle *p){

  double m_sf = 1;

  const xAOD::Electron* el = dynamic_cast<const xAOD::Electron*> (p);
  //std::cout<<"I am here "<<std::endl;
  if( m_ID_SF_Tight->getEfficiencyScaleFactor(*el,m_sf) == CP::CorrectionCode::Error ){
      Info( "GetElectronIDSF()", "ElectronEfficiencyCorrection returns Error CorrectionCode");
  }
  //std::cout<<"I am there "<<std::endl;
  if( m_debug ) Info( "GetElectronIDSF()" , "Elec IDs SF = %f ", m_sf );
    return m_sf;
} // end of GetElectronIDSF

double MyxAODAnalysis :: GetElectronRecoSF(const xAOD::IParticle *p){
  double m_sf = 1;
  const xAOD::Electron* el = dynamic_cast<const xAOD::Electron*> (p);
  if( m_Reco_SF->getEfficiencyScaleFactor(*el,m_sf) == CP::CorrectionCode::Error ){
      Info( "GetElectronRecoSF()", "ElectronEfficiencyCorrection returns Error CorrectionCode");
  }

  if( m_debug ) Info( "GetElectronRecoSF()" , "Elec Reco SF = %f ", m_sf );
  return m_sf;

} // end of GetElectronRecoSF

double MyxAODAnalysis :: GetElectronIsoTightSF(const xAOD::IParticle *p){
  double m_sf = 1;

  const xAOD::Electron* el = dynamic_cast<const xAOD::Electron*> (p);
  if( m_Iso_SF_Tight->getEfficiencyScaleFactor(*el,m_sf) == CP::CorrectionCode::Error ){
      Info( "GetElectronIsoSF()", "ElectronEfficiencyCorrection returns Error CorrectionCode");
  }

  if( m_debug ) Info( "GetElectronIsoSF()" , "Elec Iso SF = %f ", m_sf );
  return m_sf;

} // end of GetElectronIsoSF


double MyxAODAnalysis :: GetMuonTriggerSF(const xAOD::IParticle *p, int rNum){

  double m_sf = 1;

  const xAOD::Muon* muon = dynamic_cast<const xAOD::Muon*> (p);

  newMuon = new xAOD::Muon;
  newMuon->makePrivateStore(*muon);
  SelectedMuon->setStore(SelectedMuonAux);
  SelectedMuon->push_back(newMuon);
  //m_muTrigSF->setRunNumber(rNum);
  if( m_muTrigSF->getTriggerScaleFactor(*SelectedMuon,m_sf,m_muTMstring) == CP::CorrectionCode::Error ){
     Info( "GetMuonTriggerSF()", "MuonTriggerScaleFactors returns Error CorrectionCode in getting SF"); 
  } // end of if statement
  
  if( m_debug ) Info( "GetMuonTriggerSF()" , "Muon Trig SF = %f ", m_sf );
  SelectedMuon->clear();
  newMuon = 0;
  return m_sf;

} // end of MuontriggerSF


double MyxAODAnalysis :: GetElectronTrigTightSF(const xAOD::IParticle *p){

  double m_sf = 1;

  const xAOD::Electron* el = dynamic_cast<const xAOD::Electron*> (p);

  if( m_Trig_SF_Tight->getEfficiencyScaleFactor(*el,m_sf) == CP::CorrectionCode::Error ) Info( "GetElectronTrigSF()", "ElectronEfficiencyCorrection returns Error CorrectionCode");

  if( m_debug ) Info( "GetElectronTrigSF()" , "Electron Trig SF = %f ", m_sf );
  return m_sf;
} // end of ElectronTrigSF

float MyxAODAnalysis :: CalculateDeltaPhi( float Phi1, float Phi2 ){

      float Dphi = Phi1 - Phi2;
      if( fabs(Dphi)>TMath::Pi() ) {
      if( Dphi>0 ){
      	Dphi = 2*TMath::Pi()-Dphi;
      }
      else{
      	Dphi = 2*TMath::Pi()+Dphi;
    	}
      } // end of DeltaPhi calculation

      return Dphi;

} // end of function 
