#include <MyAnalysis/MyxAODAnalysis.h>

using namespace std;

// this is needed to distribute the algorithm to the workers
ClassImp(MyxAODAnalysis)

EL::StatusCode MyxAODAnalysis :: initialize ()
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.

  std::cout<<"Job Options"<<std::endl;
  std::cout<<"Systematic option "<<Arg_Sys<<std::endl;
  std::cout<<"isData "<<m_isData<<std::endl;
  std::cout<<"ApplyCorrections "<<ApplyCorrections<<std::endl; 
  std::cout<<"FastSim "<<m_FastSim<<std::endl; 
  std::cout<<"Debug "<<m_debug<<std::endl; 

  xAOD::TEvent* event = wk()->xaodEvent();

  // as a check, let's see the number of events in our xAOD
  Info("initialize()", "Number of events = %lli", event->getEntries() ); // print long long int

 // count number of events
  m_eventCounter = 0;
  m_numCleanEvents = 0;  
  NumberElectronMuon=0;
  NumberElectronTau=0; 
  NumberMuonTau=0; 
  NumberElectronMuon_MET=0;
  NumberElectronTau_MET=0; 
  NumberMuonTau_MET=0; 

  m_store = new xAOD::TStore;

  SelectedMuon    = new xAOD::MuonContainer;                                         
  SelectedMuonAux = new xAOD::MuonAuxContainer; 
  m_muTMstring = "HLT_mu50";

  std::cout << "Systematic: " << Arg_Sys << std::endl;

  std::string SelConf = "", ELORConf="";
  if( m_NoTauId ){
    SelConf = "TauSelConfNoID.conf"; ELORConf="TauElORNoID.conf";
  }
  else if( m_FailTauId ){
    SelConf = "TauSelConfFailLoose.conf"; ELORConf="TauElORFailLoose.conf";
  }
  else{
    SelConf = "TauSelConfLoose.conf"; ELORConf="TauElORLoose.conf";
    //SelConf = "TauSelConfMedium.conf"; ELORConf="TauElORMedium.conf";
  }
  TauSelTool = new TauAnalysisTools::TauSelectionTool( "TauSelectionToolLoose" );
  TauSelTool_NoOLR   = new TauAnalysisTools::TauSelectionTool( "TauSelectionToolElORLoose" );

  if( !TauSelTool->setProperty( "ConfigPath", ("$UserAnalysis_DIR/data/MyAnalysis/"+SelConf).c_str() ).isSuccess() ){
    Info( "TauToolsInitializer()", "Error in TauSelectionTool (ID WP) setting ConfigPath" );
  }
  if( !TauSelTool->setProperty( "CreateControlPlots", false ).isSuccess() ){
    Info( "InitializeTauSelectTool()", "Error in TauSelectionTool (Loose) setting control plots flag" );
  }
  if( !TauSelTool->initialize().isSuccess() ){
    Info( "TauToolsInitializer()", "Cannot initialize TauSelectionTool (ID WP) " );
  }
  else{
    Info( "TauToolsInitializer()", "TauSelectionTool (ID WP) initialized!" );
  }
  
  if( !TauSelTool_NoOLR->setProperty( "ConfigPath", ("$UserAnalysis_DIR/data/MyAnalysis/"+ELORConf).c_str() ).isSuccess() ){
    Info( "TauToolsInitializer()", "Error in TauSelectionTool (ElOR) setting ConfigPath" );
  }
  if( !TauSelTool_NoOLR->setProperty( "CreateControlPlots", false ).isSuccess() ){
    Info( "InitializeTauSelectTool()", "Error in TauSelectionTool (ElOR) setting control plots flag" );
  }
  if( !TauSelTool_NoOLR->initialize().isSuccess() ){
    Info( "TauToolsInitializer()", "Cannot initialize TauSelectionTool (ElOR) " );
  }
  else{
    Info( "TauToolsInitializer()", "TauSelectionTool (ElOR) initialized!" );
  }

  
  /* Tau Likelihood Decorator Tool */
  TOELLHDecorator = new TauAnalysisTools::TauOverlappingElectronLLHDecorator( "TauOverlappingElectronLLHDecorator");
  if( !TOELLHDecorator->initialize().isSuccess() ){
    Info( "TauToolsInitializer()", "Cannot initialize TauOverlappingElectronLLHDecorator" );
  }
  else{
    Info( "TauToolsInitializer()", "TauOverlappingElectronLLHDecorator initialized!" );
  }

  
  /* Tau Truth Matching Tool */
  T2MT = new TauAnalysisTools::TauTruthMatchingTool( "TauTruthMatchingTool" );
  if( !T2MT->setProperty( "WriteTruthTaus", true ).isSuccess() ){
    Info( "TauToolsInitializer()", "Error in TauTruthMatchingTool setting WriteTruthTaus" );
  }
  if( !T2MT->initialize().isSuccess() ){
    Info( "TauToolsInitializer()", "Cannot initialize TauTruthMatchingTool" );
  }
  else{
    Info( "TauToolsInitializer()", "TauTruthMatchingTool initialized!" );
  }
  
  
  /* Tau Smearing Tool */
  TauSmeTool = new TauAnalysisTools::TauSmearingTool( "TauSmearingTool" );
  if( !TauSmeTool->setProperty( "ApplyMVATES", true ).isSuccess() ){
    Info( "TauToolsInitializer()", "Error in TauSmearingTool MVA TES setting " );
  }
  if( !TauSmeTool->initialize().isSuccess() ){
    Info( "TauToolsInitializer()", "Cannot initialize TauSmearingTool" );
  }
  else{
    Info( "TauToolsInitializer()", "TauSmearingTool initialized!" );
  }

  
  /* Tau Efficiency Correction - Reco SF */
  TauEffCorrTool = new TauAnalysisTools::TauEfficiencyCorrectionsTool( "TauEfficiencyCorrectionsTool_RecoSF" );
  if( !TauEffCorrTool->setProperty( "EfficiencyCorrectionTypes", std::vector<int>({TauAnalysisTools::EfficiencyCorrectionType::SFRecoHadTau}) ).isSuccess() ){
    Info( "TauToolsInitializer()", "Error in TauEfficiencyCorrectionsTool efficiency correction type for Reco SF setting" );
  }
  if( !TauEffCorrTool->setProperty( "IDLevel", (int)TauAnalysisTools::e_JETID::JETIDBDTLOOSE ).isSuccess() ){
    Info( "TauToolsInitializer()", "Error in TauEfficiencyCorrectionsTool ID level for Reco SF setting" );
  }
  if( !TauEffCorrTool->setProperty( "UseHighPtUncert", true ).isSuccess() ){
    Info( "TauToolsInitializer()", "Error in TauEfficiencyCorrectionsTool High pT Uncertainties for Reco SF setting" );
  }
  //m_tauRecoSF->msg().setLevel( MSG::DEBUG );
  if( !TauEffCorrTool->initialize().isSuccess() ){
    Info( "TauToolsInitializer()", "Cannot initialize TauEfficiencyCorrectionsTool for Reco SF" );
  }
  else{
    Info( "TauToolsInitializer()", "TauEfficiencyCorrectionsTool for Reco SF initialized!" );
  }
  
  
  /* Tau Efficiency Correction - EleOlr SF */
  TauEffEleOLRTool = new TauAnalysisTools::TauEfficiencyCorrectionsTool( "TauEfficiencyCorrectionsTool_EleOlrSF" );
  if( !TauEffEleOLRTool->setProperty( "EfficiencyCorrectionTypes", std::vector<int>({TauAnalysisTools::EfficiencyCorrectionType::SFEleOLRHadTau}) ).isSuccess() ){
    Info( "TauToolsInitializer()", "Error in TauEfficiencyCorrectionsTool efficiency correction type for EleOlr SF setting" );
  }
  if( !TauEffEleOLRTool->setProperty( "IDLevel", (int)TauAnalysisTools::e_JETID::JETIDBDTLOOSE ).isSuccess() ){
    Info( "TauToolsInitializer()", "Error in TauEfficiencyCorrectionsTool ID level for EleOlr SF setting" );
  }
  if( !TauEffEleOLRTool->setProperty( "UseHighPtUncert", true ).isSuccess() ){
    Info( "TauToolsInitializer()", "Error in TauEfficiencyCorrectionsTool High pT Uncertainties for EleOlr SF setting" );
  }
  //m_tauElOlrSF->msg().setLevel( MSG::DEBUG );
  if( !TauEffEleOLRTool->initialize().isSuccess() ){
    Info( "TauToolsInitializer()", "Cannot initialize TauEfficiencyCorrectionsTool for EleOlr SF" );
  }
  else{ 
    Info( "TauToolsInitializer()", "TauEfficiencyCorrectionsTool for EleOlr SF initialized!" );
  }
  
  
  /* Tau Efficiency Correction - JetID SF */
  TauEffIDTool = new TauAnalysisTools::TauEfficiencyCorrectionsTool( "TauEfficiencyCorrectionsTool_JetIDSF" );
  if( !TauEffIDTool->setProperty( "EfficiencyCorrectionTypes", std::vector<int>({TauAnalysisTools::EfficiencyCorrectionType::SFJetIDHadTau}) ).isSuccess() ){
    Info( "TauToolsInitializer()", "Error in TauEfficiencyCorrectionsTool efficiency correction type for JetID SF setting" );
  }
  if( !TauEffIDTool->setProperty( "IDLevel", (int)TauAnalysisTools::e_JETID::JETIDBDTLOOSE ).isSuccess() ){
    Info( "TauToolsInitializer()", "Error in TauEfficiencyCorrectionsTool ID level for JetID SF setting" );
  }
  if( !TauEffIDTool->setProperty( "UseHighPtUncert", true ).isSuccess() ){
    Info( "TauToolsInitializer()", "Error in TauEfficiencyCorrectionsTool High pT Uncertainties for JetID SF setting" );
  }
  //m_tauJetIDSF->msg().setLevel( MSG::DEBUG );
  if( !TauEffIDTool->initialize().isSuccess() ){
    Info( "TauToolsInitializer()", "Cannot initialize TauEfficiencyCorrectionsTool for JetID SF" );
  }
  else{
    Info( "TauToolsInitializer()", "TauEfficiencyCorrectionsTool for JetID SF initialized!" );
  }


  // GRL
  m_grl = new GoodRunsListSelectionTool("GoodRunsListSelectionTool");
 
  std::string GRL_Year2015 = "GoodRunsLists/data15_13TeV/20170619/data15_13TeV.periodAllYear_DetStatus-v89-pro21-02_Unknown_PHYS_StandardGRL_All_Good_25ns.xml";
  std::string GRL_Year2016 = "GoodRunsLists/data16_13TeV/20180129/data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml";
  std::string GRL_Year2017 = "GoodRunsLists/data17_13TeV/20180619/data17_13TeV.periodAllYear_DetStatus-v99-pro22-01_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml";
  std::string GRL_Year2018 = "GoodRunsLists/data18_13TeV/20180906/data18_13TeV.periodAllYear_DetStatus-v102-pro22-03_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml";

  const char* GRL_Path2015 = gSystem->ExpandPathName( GRL_Year2015.c_str() );
  const char* GRL_Path2016 = gSystem->ExpandPathName( GRL_Year2016.c_str() );
  const char* GRL_Path2017 = gSystem->ExpandPathName( GRL_Year2017.c_str() );
  const char* GRL_Path2018 = gSystem->ExpandPathName( GRL_Year2018.c_str() );
  
  std::vector<std::string> vecStringGRL;
  vecStringGRL.push_back(GRL_Path2015);
  vecStringGRL.push_back(GRL_Path2016);
  vecStringGRL.push_back(GRL_Path2017);
  vecStringGRL.push_back(GRL_Path2018);

  if( !m_grl->setProperty( "GoodRunsListVec", vecStringGRL ).isSuccess() ){
    Info( "ToolsInitializer()", "Error in GoodRunsListSelectionTool vector GRL setting");
  }
  if( !m_grl->setProperty( "PassThrough", false ).isSuccess() ){ // if true (default) will ignore result of GRL and will just pass all events
    Info( "ToolsInitializer()", "Error in GoodRunsListSelectionTool PassTrough setting");
  }
  if( !m_grl->initialize().isSuccess() ){
    Info( "ToolsInitializer()", "Cannot initialize GoodRunsListSelectionTool!" );
  }
  else{
    Info( "ToolsInitializer()", "GoodRunsListSelectionTool initialized!" );
  }


  mcInfo = new LFVUtils::MCSampleInfo(EcmEnergy::LHCEnergy::ThirteenTeV);
  m_mcOver = new LFVUtils::MCSampleOverlap(); 

 /* Muon Calibration and Smearing Tool */
  m_muSmear2016 = new CP::MuonCalibrationAndSmearingTool( "MuonCorrectionTool2016" );
  m_muSmear2017 = new CP::MuonCalibrationAndSmearingTool( "MuonCorrectionTool2017" );
  m_muSmear2018 = new CP::MuonCalibrationAndSmearingTool( "MuonCorrectionTool2018" );
  
  if( !m_muSmear2016->setProperty( "Year", "Data16" ).isSuccess() ){ 
    Info( "MuonToolsInitializer()", "Error in MuonCalibrationAndSmearing Tool (2016) Year setting" );
  }
  if( !m_muSmear2016->setProperty( "StatComb", false ).isSuccess() ){
    Info( "MuonToolsInitializer()", "Error in MuonCalibrationAndSmearing Tool (2016) StatComb setting" );
  }
  if( !m_muSmear2016->setProperty( "SagittaRelease", "sagittaBiasDataAll_25_07_17" ).isSuccess() ){
    Info( "MuonToolsInitializer()", "Error in MuonCalibrationAndSmearing Tool (2016) SagittaRelease setting" );
  }
  if( !m_muSmear2016->setProperty( "SagittaCorr", true ).isSuccess() ){
    Info( "MuonToolsInitializer()", "Error in MuonCalibrationAndSmearing Tool (2016) SagittaCorr setting" );
  }
  if( !m_muSmear2016->setProperty( "doSagittaMCDistortion" , false ).isSuccess() ){
    Info( "MuonToolsInitializer()", "Error in MuonCalibrationAndSmearing Tool (2016) doSagittaMCDistortion setting" );
  }
  if( !m_muSmear2016->setProperty( "SagittaCorrPhaseSpace", true ).isSuccess() ){
    Info( "MuonToolsInitializer()", "Error in MuonCalibrationAndSmearing Tool (2016) SagittaCorrPhaseSpace setting" );
  }
  if( !m_muSmear2016->setProperty( "Release", "Recs2017_08_02" ).isSuccess() ){
    Info( "MuonToolsInitializer()", "Error in MuonCalibrationAndSmearing Tool (2016) Release setting" );
  }
  if( !m_muSmear2016->initialize().isSuccess() ){
    Info( "MuonToolsInitializer()", "Cannot initialize MuonCalibrationAndSmearingTool (2016)!" );
  }
  else{
    Info( "MuonToolsInitializer()", "MuonCalibrationAndSmearingTool (2016) initialized!" );
  }

  if( !m_muSmear2017->setProperty( "Year", "Data17" ).isSuccess() ){
    Info( "MuonToolsInitializer()", "Error in MuonCalibrationAndSmearing Tool (2017) Year setting" );
  }
  if( !m_muSmear2017->setProperty( "StatComb", false ).isSuccess() ){
    Info( "MuonToolsInitializer()", "Error in MuonCalibrationAndSmearing Tool (2017) StatComb setting" );
  }
  if( !m_muSmear2017->setProperty( "SagittaRelease", "sagittaBiasDataAll_30_07_18" ).isSuccess() ){
    Info( "MuonToolsInitializer()", "Error in MuonCalibrationAndSmearing Tool (2017) SagittaRelease setting" );
  }
  if( !m_muSmear2017->setProperty( "SagittaCorr", true ).isSuccess() ){
    Info( "MuonToolsInitializer()", "Error in MuonCalibrationAndSmearing Tool (2017) SagittaCorr setting" );
  }
  if( !m_muSmear2017->setProperty( "doSagittaMCDistortion" , false ).isSuccess() ){
    Info( "MuonToolsInitializer()", "Error in MuonCalibrationAndSmearing Tool (2017) doSagittaMCDistortion setting" );
  }
  if( !m_muSmear2017->setProperty( "SagittaCorrPhaseSpace", true ).isSuccess() ){
    Info( "MuonToolsInitializer()", "Error in MuonCalibrationAndSmearing Tool (2017) SagittaCorrPhaseSpace setting" );
  }
  if( !m_muSmear2017->setProperty( "Release", "Recs2017_08_02" ).isSuccess() ){
    Info( "MuonToolsInitializer()", "Error in MuonCalibrationAndSmearing Tool (2017) Release setting" );
  }
  if( !m_muSmear2017->initialize().isSuccess() ){
    Info( "MuonToolsInitializer()", "Cannot initialize MuonCalibrationAndSmearingTool (2017)!" );
  }
  else{
    Info( "MuonToolsInitializer()", "MuonCalibrationAndSmearingTool (2017) initialized!" );
  }

  if( !m_muSmear2018->setProperty( "Year", "Data17" ).isSuccess() ){
    Info( "MuonToolsInitializer()", "Error in MuonCalibrationAndSmearing Tool (2018) Year setting" );
  }
  if( !m_muSmear2018->setProperty( "StatComb", false ).isSuccess() ){
    Info( "MuonToolsInitializer()", "Error in MuonCalibrationAndSmearing Tool (2018) StatComb setting" );
  }
  if( !m_muSmear2018->setProperty( "SagittaRelease", "sagittaBiasDataAll_30_07_18" ).isSuccess() ){
    Info( "MuonToolsInitializer()", "Error in MuonCalibrationAndSmearing Tool (2018) SagittaRelease setting" );
  }
  if( !m_muSmear2018->setProperty( "SagittaCorr", false ).isSuccess() ){
    Info( "MuonToolsInitializer()", "Error in MuonCalibrationAndSmearing Tool (2018) SagittaCorr setting" );
  }
  if( !m_muSmear2018->setProperty( "doSagittaMCDistortion" , true ).isSuccess() ){
    Info( "MuonToolsInitializer()", "Error in MuonCalibrationAndSmearing Tool (2018) doSagittaMCDistortion setting" );
  }
  if( !m_muSmear2018->setProperty( "SagittaCorrPhaseSpace", false ).isSuccess() ){
    Info( "MuonToolsInitializer()", "Error in MuonCalibrationAndSmearing Tool (2018) SagittaCorrPhaseSpace setting" );
  }
  if( !m_muSmear2018->setProperty( "Release", "Recs2017_08_02" ).isSuccess() ){
    Info( "MuonToolsInitializer()", "Error in MuonCalibrationAndSmearing Tool (2018) Release setting" );
  }
  if( !m_muSmear2018->initialize().isSuccess() ){
    Info( "MuonToolsInitializer()", "Cannot initialize MuonCalibrationAndSmearingTool (2018)!" );
  }
  else{ 
    Info( "MuonToolsInitializer()", "MuonCalibrationAndSmearingTool (2018) initialized!" );
  }


  /* Muon Selector Tool */
  m_muonSelection = new CP::MuonSelectionTool( "MuSelector" );
  if( !m_muonSelection->setProperty( "MaxEta", 2.5 ).isSuccess() ){
    Info( "MuonToolsInitializer()", "Error in MuonSelectionTool eta cut" );
  }
  if(!m_muonSelection->setProperty( "MuQuality", 4 ).isSuccess() ){
    Info( "MuonToolsInitializer()", "Error in MuonSelectionTool quality cut" );
  }
  if(!m_muonSelection->initialize().isSuccess()){
    Info( "MuonToolsInitializer()", "Cannot initialize MuonSelectionTool!" );
  }
  else{
    Info( "MuonToolsInitializer()", "MuonSelectionTool initialized!" );
  }


  /* Isolation Tool */
  m_iso = new CP::IsolationSelectionTool( "ElectronIsolation" );
  if( !m_iso->setProperty( "ElectronWP", "FixedCutHighPtCaloOnly" ).isSuccess() ){
    Info( "ToolsInitializer()", "Error in IsolationSelectionTool working point configuration for electrons" );
  }
  if( !m_iso->setProperty( "MuonWP", "FixedCutHighPtTrackOnly" ).isSuccess() ){
    Info( "ToolsInitializer()", "Error in IsolationSelectionTool working point configuration for muons" );
  }
  if( !m_iso->initialize().isSuccess() ){
    Info( "ToolsInitializer()", "Cannot initialize IsolationSelectionTool!" );
  }
  else{
    Info( "ToolsInitializer()", "IsolationSelectionTool initialized!" );
  }

  /* Isolation Correction Tool */
  /*m_isoCorrTool = new CP::IsolationCorrectionTool("IsolationCorrection");
  if( !m_isoCorrTool->setProperty( "IsMC", !m_isData ).isSuccess() ){  //it should be removed
    Info( "ToolsInitializer()", "Error in IsolationCorrectionTool MC flag setting" );
  }
  if( !m_isoCorrTool->setProperty( "AFII_corr", m_FastSim ).isSuccess() ){
    Info( "ToolsInitializer()", "Error in IsolationCorrectionTool AFII setting" );
  }
  //if( !m_isoCorr->setProperty( "CorrFile", "IsolationCorrections/v1/isolation_ptcorrections_rel207.root" ).isSuccess() ){
  //Info( "ToolsInitializer()", "Error in IsolationCorrectionTool Correction File setting" );
  //}
  if( !m_isoCorrTool->initialize().isSuccess() ){
    Info( "ToolsInitializer()", "Cannot initialize IsolationCorrectionTool!" );
  }
  else{
    Info( "ToolsInitializer()", "IsolationCorrectionTool initialized!" );
  } */

  /* Missing Energy Builder */
  m_metMaker = asg::AnaToolHandle<IMETMaker>("met::METMaker/metMaker"); 
  if( !m_metMaker.retrieve().isSuccess() ){
    Info( "ToolsInitializer()", "Cannot intialize METMaker!" );
  }
  else{
    Info( "Tools Initializer()", "METMaker initialized!" );
  }     

 // configuration and initialization example
  std::cout<<m_MCtype<<std::endl;
  m_Pileup  = new CP::PileupReweightingTool("Pileup");
  std::vector<std::string> confFiles;
  std::vector<std::string> LumiCalcFiles;

  confFiles.push_back( ("$UserAnalysis_DIR/data/MyAnalysis/PU/PURW_Background_"+m_MCtype+".root").c_str() ); //to be updated
  confFiles.push_back( ("$UserAnalysis_DIR/data/MyAnalysis/PU/PURW_Signal_"+m_MCtype+".root").c_str() ); //to be updated
  if(m_MCtype=="mc16a"){
    LumiCalcFiles.push_back( "GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root" );
    LumiCalcFiles.push_back( "GoodRunsLists/data16_13TeV/20180129/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-009.root" );
  }
  else if(m_MCtype=="mc16d"){
    LumiCalcFiles.push_back( "lumicalc/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root" );
  }
  else if(m_MCtype=="mc16e"){
    LumiCalcFiles.push_back( "lumicalc/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-001.root" );
  } else{
    Info( "ToolsInitializer()", "Error in PileUpReweighting LumiCalcFiles setting: wrong mcType %s set! exiting ... ", m_MCtype.c_str() );
    exit(0);
  }
  if( !m_Pileup->setProperty( "ConfigFiles", confFiles).isSuccess() ){
    Info( "ToolsInitializer()", "Error in PileupReweightingTool Config Files setting");
  }
  if( !m_Pileup->setProperty( "LumiCalcFiles", LumiCalcFiles).isSuccess() ){
    Info( "ToolsInitializer()", "Error in PileupReweightingTool Lumi Calc Files setting");
  }
  PRWToolHandle = m_Pileup;

  //Trigger                                                                                                                                                                 
  m_TrigConfigTool = new TrigConf::xAODConfigTool("xAODConfigTool");
  if( !m_TrigConfigTool->initialize().isSuccess() ){
    Info( "ToolsInitializer()", "Cannot initialize TrigConf::xAODConfigTool" );
    return StatusCode::SUCCESS;
  }
  else{
    Info( "ToolsInitializer()", "TrigConf::xAODConfigTool initialized!" );
  }
  ToolHandle<TrigConf::ITrigConfigTool> configHandle(m_TrigConfigTool);

  /* Trigger Decision Tool */
  m_TrigDecTool = new Trig::TrigDecisionTool( "TrigDecisionTool" );
  if( !m_TrigDecTool->setProperty( "ConfigTool",ToolHandle<TrigConf::ITrigConfigTool>(m_TrigConfigTool) ).isSuccess() ){
    Info( "ToolsInitializer()", "Error in TrigDecisionTool configuration" );
  }
  if( !m_TrigDecTool->setProperty( "TrigDecisionKey", "xTrigDecision" ).isSuccess() ){
    Info( "ToolsInitializer()", "Error in TrigDecisionTool decisionn key" );
  }
  if( !m_TrigDecTool->initialize().isSuccess() ){
    Info( "ToolsInitializer()", "Cannot initialize Trig::TrigDecisionTool!" );
    return StatusCode::SUCCESS;
  }
  else{
    Info( "ToolsInitializer()", "Trig::TrigDecisionTool initialized!" );
  }
  ToolHandle<Trig::TrigDecisionTool> m_trigDec(m_TrigDecTool);

  /* Trigger Matching Tool */
  m_match_Tool = new Trig::MatchingTool("MatchingTool");
  //m_trigMatch->msg().setLevel( MSG::WARNING );
  if( !m_match_Tool->initialize().isSuccess() ){
    Info( "ToolsInitializer()", "Cannot initialize Trig::TrigMatchingTool!" );
  }
  else{
    Info( "ToolsInitializer()", "Trig::TrigMatchingTool initialized!" );
  }

 /* Electron Selector Tools - Tight, Medium and Loose */
  m_ElectronIDTool_Loose = new AsgElectronLikelihoodTool( "LooseLH"   );
  m_ElectronIDTool_Tight = new AsgElectronLikelihoodTool( "TightLH"   );
  m_ElectronIDTool = new AsgElectronLikelihoodTool( "MediumLH" );
  std::string confDir = "ElectronPhotonSelectorTools/offline/mc16_20170828/";
    
  if( !m_ElectronIDTool_Tight->setProperty( "WorkingPoint", "TightLHElectron").isSuccess() ){
    Info( "ElectronToolsInitializer()", "Error in AsgElectronLikelihoodTool (Tight) configuration" );
  }
  if( !m_ElectronIDTool->setProperty( "WorkingPoint", "MediumLHElectron").isSuccess() ){
    Info( "ElectronToolsInitializer()", "Error in AsgElectronLikelihoodTool (Medium) configuration" );
  }
  if( !m_ElectronIDTool_Loose->setProperty( "WorkingPoint", "LooseLHElectron").isSuccess() ){
    Info( "ElectronToolsInitializer()", "Error in AsgElectronLikelihoodTool (Loose) configuration" );
  }
  
  if( !m_ElectronIDTool_Tight->initialize().isSuccess() ){
    Info( "ElectronToolsInitializer()", "Cannot initialize AsgElectronLikelihoodTool (Tight) !" );
    return StatusCode::SUCCESS;
  }
  else{
    Info( "ElectronToolsInitializer()", "AsgElectronLikelihoodTool (Tight) initialized!" );
  }

  if( !m_ElectronIDTool_Loose->initialize().isSuccess() ){
    Info( "ElectronToolsInitializer()", "Cannot initialize AsgElectronLikelihoodTool (Loose) !" );
    return StatusCode::SUCCESS;
  }
  else{
    Info( "ElectronToolsInitializer()", "AsgElectronLikelihoodTool (Tight) initialized!" );
  }
  
  if( !m_ElectronIDTool->initialize().isSuccess() ){
    Info( "ElectronToolsInitializer()", "Cannot initialize AsgElectronLikelihoodTool (Medium)!" );
    return StatusCode::SUCCESS;
  }
  else{
    Info( "ElectronToolsInitializer()", "AsgElectronLikelihoodTool initialized (Medium)!" );
  }


  // Electron Calibration
  m_EleCalibTool = new CP::EgammaCalibrationAndSmearingTool("EgammaCalibrationAndSmearingTool");

  if( !m_EleCalibTool->setProperty( "ESModel", "es2017_R21_v1" ).isSuccess() ){
    Info( "ElectronToolsInitializer()", "Error in EgammaCalibrationAndSmearingTool ESModel setting" );
  }
  if( !m_EleCalibTool->setProperty( "decorrelationModel", "1NP_v1" ).isSuccess() ){
    Info( "ElectronToolsInitializer()", "Error in EgammaCalibrationAndSmearingTool decorrelation Model setting" );
  }
  if( !m_EleCalibTool->setProperty( "useAFII", (int)m_FastSim ).isSuccess() ){
    Info( "ElectronToolsInitializer()", "Error in EgammaCalibrationAndSmearingTool FastSim setting" );
  }
  if( !m_EleCalibTool->initialize().isSuccess() ){
    Info( "ElectronToolsInitializer()", "Cannot initialize EgammaCalibrationAndSmearingTool" );
  }
  else{
    Info( "ElectronToolsInitializer()", "EgammaCalibrationAndSmearingTool initialized!" );
  }
  
  int SimType=1; if( m_FastSim ) SimType=3;

  std::string m_ElecSFPath = "ElectronEfficiencyCorrection/2015_2017/rel21.2/Moriond_February2018_v1/";

  // Electron Reco Scale Factors                                                                        
  m_Reco_SF = new AsgElectronEfficiencyCorrectionTool("AsgElectronEfficiencyCorrectionTool_RecoSF");
  std::vector<std::string> Input_elRecoSF;
  Input_elRecoSF.push_back(m_ElecSFPath+"offline/efficiencySF.offline.RecoTrk.root");
  if( !m_Reco_SF->setProperty( "CorrectionFileNameList", Input_elRecoSF ).isSuccess() ){
    Info( "ElectronToolsInitializer()", "Error in ElectronEfficiencyCorrectionTool input file for Reco SF setting" );
  }
  if( !m_Reco_SF->setProperty( "ForceDataType", SimType ).isSuccess() ){
    Info( "ElectronToolsInitializer()", "Error in ElectronEfficiencyCorrectionTool data type for Reco SF setting" );
  }
  if( !m_Reco_SF->setProperty( "CorrelationModel", "TOTAL" ).isSuccess() ){
    Info( "ElectronToolsInitializer()", "Error in ElectronEfficiencyCorrectionTool Correlation Model for Reco SF setting" );
  }
  //if( m_verbose ) m_Reco_SF->msg().setLevel( MSG::VERBOSE );
  if( !m_Reco_SF->initialize().isSuccess() ){
    Info( "ElectronToolsInitializer()", "Cannot initialize ElectronEfficiencyCorrectionTool for Reco SF" );
  }
  else{ 
    Info( "ElectronToolsInitializer()", "ElectronEfficiencyCorrectionTool for Reco SF initialized!" );
  }


  // Electron ID Scale Factors
  m_ID_SF_Tight = new AsgElectronEfficiencyCorrectionTool("AsgElectronEfficiencyCorrectionTool_IDSF_Tight");
  m_ID_SF_Med = new AsgElectronEfficiencyCorrectionTool("AsgElectronEfficiencyCorrectionTool_IDSF_Med");
  std::vector<std::string> Input_elIDSF_Tig;
  std::vector<std::string> Input_elIDSF_Med;
  Input_elIDSF_Tig.push_back(m_ElecSFPath+"offline/efficiencySF.offline.TightLLH_d0z0_v13.root");
  Input_elIDSF_Med.push_back(m_ElecSFPath+"offline/efficiencySF.offline.MediumLLH_d0z0_v13.root");
    
  if( !m_ID_SF_Tight->setProperty( "CorrectionFileNameList", Input_elIDSF_Tig ).isSuccess() ){
    Info( "ElectronToolsInitializer()", "Error in ElectronEfficiencyCorrectionTool input file for ID SF (Tight) setting" );
  }
  if( !m_ID_SF_Tight->setProperty( "ForceDataType", SimType ).isSuccess() ){
    Info( "ElectronToolsInitializer()", "Error in ElectronEfficiencyCorrectionTool data type for ID SF (Tight) setting" );
  }
  if( !m_ID_SF_Tight->setProperty( "CorrelationModel", "TOTAL" ).isSuccess() ){
    Info( "ElectronToolsInitializer()", "Error in ElectronEfficiencyCorrectionTool Correlation Model for ID SF (Tight) setting" );
  }
  if( !m_ID_SF_Tight->initialize().isSuccess() ){
    Info( "ElectronToolsInitializer()", "Cannot initialize ElectronEfficiencyCorrectionTool for ID SF (Tight)" );
  }
  else{
    Info( "ElectronToolsInitializer()", "ElectronEfficiencyCorrectionTool for ID SF (Tight) initialized!" );
  }

  if( !m_ID_SF_Med->setProperty( "CorrectionFileNameList", Input_elIDSF_Med ).isSuccess() ){
    Info( "ElectronToolsInitializer()", "Error in ElectronEfficiencyCorrectionTool input file for ID SF (Tight) setting" );
  }
  if( !m_ID_SF_Med->setProperty( "ForceDataType", SimType ).isSuccess() ){
    Info( "ElectronToolsInitializer()", "Error in ElectronEfficiencyCorrectionTool data type for ID SF (Tight) setting" );
  }
  if( !m_ID_SF_Med->setProperty( "CorrelationModel", "TOTAL" ).isSuccess() ){
    Info( "ElectronToolsInitializer()", "Error in ElectronEfficiencyCorrectionTool Correlation Model for ID SF (Tight) setting" );
  }
  if( !m_ID_SF_Med->initialize().isSuccess() ){
    Info( "ElectronToolsInitializer()", "Cannot initialize ElectronEfficiencyCorrectionTool for ID SF (Tight)" );
  }
  else{
    Info( "ElectronToolsInitializer()", "ElectronEfficiencyCorrectionTool for ID SF (Tight) initialized!" );
  }

  // Electron Trigger Scale Factors                                                                  
  m_Trig_SF_Tight = new AsgElectronEfficiencyCorrectionTool("AsgElectronEfficiencyCorrectionTool_TrigSF_Tight");
  std::vector<std::string> Input_elTrigSF_Tig;
  Input_elTrigSF_Tig.push_back(m_ElecSFPath+"trigger/efficiencySF.SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2017_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0.TightLLH_d0z0_v13_isolFixedCutHighPtCaloOnly.root");
  
  if( !m_Trig_SF_Tight->setProperty( "CorrectionFileNameList", Input_elTrigSF_Tig ).isSuccess() ){
    Info( "ElectronToolsInitializer()", "Error in ElectronEfficiencyCorrectionTool input file for Trig SF (Tight) setting" );
  }
  if( !m_Trig_SF_Tight->setProperty( "ForceDataType", SimType ).isSuccess() ){
    Info( "ElectronToolsInitializer()", "Error in ElectronEfficiencyCorrectionTool data type for Trig SF (Tight) setting" );
  }
  if( !m_Trig_SF_Tight->setProperty( "CorrelationModel", "TOTAL" ).isSuccess() ){
    Info( "ElectronToolsInitializer()", "Error in ElectronEfficiencyCorrectionTool Correlation Model for Trig SF (Tight) setting" );
  }
  if( !m_Trig_SF_Tight->initialize().isSuccess() ){
    Info( "ElectronToolsInitializer()", "Cannot initialize ElectronEfficiencyCorrectionTool for Trig SF (Tight) " );
  }
  else{
    Info( "ElectronToolsInitializer()", "ElectronEfficiencyCorrectionTool for Trig SF (Tight) initialized!" );
  }

  // Electron Iso SF
  m_Iso_SF_Tight = new AsgElectronEfficiencyCorrectionTool("AsgElectronEfficiencyCorrectionTool_IsoSF_Tight");
  std::vector<std::string> Input_elIsoSF_Tig;
  Input_elIsoSF_Tig.push_back(m_ElecSFPath+"isolation/efficiencySF.Isolation.TightLLH_d0z0_v13_isolFixedCutHighPtCaloOnly.root");
  
  if( !m_Iso_SF_Tight->setProperty( "CorrectionFileNameList", Input_elIsoSF_Tig ).isSuccess() ){
    Info( "ElectronToolsInitializer()", "Error in ElectronEfficiencyCorrectionTool input file for Iso SF (Tight) setting" );
  }
  if( !m_Iso_SF_Tight->setProperty( "ForceDataType", SimType ).isSuccess() ){
    Info( "ElectronToolsInitializer()", "Error in ElectronEfficiencyCorrectionTool data type for Iso SF (Tight) setting" );
  }
  if( !m_Iso_SF_Tight->setProperty( "CorrelationModel", "TOTAL" ).isSuccess() ){
    Info( "ElectronToolsInitializer()", "Error in ElectronEfficiencyCorrectionTool Correlation Model for Iso SF (Tight) setting" );
  }
  if( !m_Iso_SF_Tight->initialize().isSuccess() ){
    Info( "ElectronToolsInitializer()", "Cannot initialize ElectronEfficiencyCorrectionTool for Iso SF (Tight)" );
  }
  else{
    Info( "ElectronToolsInitializer()", "ElectronEfficiencyCorrectionTool for Iso SF (Tight) initialized!" );
  }


  //Muon Trigger Scale Factors
  m_muTrigSF = new CP::MuonTriggerScaleFactors("TrigSFClass");
  
  if( !m_muTrigSF->setProperty( "MuonQuality", "HighPt" ).isSuccess() ){
    Info( "MuonToolsInitializer()", "Error in MuonTriggerScaleFactors muon quality setting" );
  }
  if( !m_muTrigSF->setProperty( "CalibrationRelease", "180905_TriggerUpdate" ).isSuccess() ){
    Info( "MuonToolsInitializer()", "Error in MuonTriggerScaleFactors calibration realease setting" );
  }
  if( !m_muTrigSF->initialize().isSuccess() ){
    Info( "MuonToolsInitializer()", "Cannot initialize MuonTriggerScaleFactors" );
  }
  else{
    Info( "MuonToolsInitializer()", "MuonTriggerScaleFactors initialized!" );
  }


  //Muon Iso Scale Factor
  m_muIsoSF = new CP::MuonEfficiencyScaleFactors("MuonIsoSF");
  if( !m_muIsoSF->setProperty( "WorkingPoint", "FixedCutHighPtTrackOnlyIso" ).isSuccess() ){
    Info( "MuonToolsInitializer()", "Error in MuonIsolationScaleFactors working point setting" );
  }
  if( !m_muIsoSF->setProperty( "CalibrationRelease", "180808_SummerUpdate" ).isSuccess() ){
    Info( "MuonToolsInitializer()", "Error in MuonIsolationScaleFactors calibration release setting" );
  }
  if( !m_muIsoSF->initialize().isSuccess() ){
    Info( "MuonToolsInitializer()", "Cannot initialize MuonIsolationScaleFactors" );
  }
  else{
    Info( "MuonToolsInitializer()", "MuonIsolationScaleFactors initialized!" );
  }


  //Muon TTVA
  m_muTTVASF = new CP::MuonEfficiencyScaleFactors("MuonTTVASF");
  if( !m_muTTVASF->setProperty( "WorkingPoint", "TTVA" ).isSuccess() ){
    Info( "MuonToolsInitializer()", "Error in MuonTrackToVertexAssociationScaleFactors working point setting" );
  }
  if( !m_muTTVASF->setProperty( "CalibrationRelease", "180808_SummerUpdate" ).isSuccess() ){
    Info( "MuonToolsInitializer()", "Error in MuonTrackToVertexAssociationScaleFactors calibration release setting" );
  }
  if( !m_muTTVASF->initialize().isSuccess() ){
    Info( "MuonToolsInitializer()", "Cannot initialize MuonTrackToVertexAssociationScaleFactors" );
  }
  else{
    Info( "MuonToolsInitializer()", "MuonTrackToVertexAssociationScaleFactors initialized!" );
  }

  m_muSmear.push_back(m_muSmear2016);
  m_muSmear.push_back(m_muSmear2017);
  m_muSmear.push_back(m_muSmear2018);


  // Muon Scale Factors
  m_muRecoSF = new CP::MuonEfficiencyScaleFactors("MuonRecoSF");
  if( !m_muRecoSF->setProperty( "WorkingPoint", "HighPt" ).isSuccess() ){
    Info( "MuonToolsInitializer()", "Error in MuonEfficiencyScaleFactors working point setting" );
  }
  if( !m_muRecoSF->setProperty( "CalibrationRelease", "180808_SummerUpdate" ).isSuccess() ){
    Info( "MuonToolsInitializer()", "Error in MuonEfficiencyScaleFactors calibration release setting" );
  }
  if( !m_muRecoSF->initialize().isSuccess() ){
    Info( "MuonToolsInitializer()", "Cannot initialize MuonEfficiencyScaleFactors" );
  }
  else{
    Info( "MuonToolsInitializer()", "MuonEfficiencyScaleFactors initialized!" );
  }



  //////////////////////////////////////////////////////////////////////////////////////////////////////
  /////////////////////////////////////// Jet tool /////////////////////////////////////////////////////

  /* Jet Vertex Tagger Tool */


  /* Jet Cleaning Tool */
  m_jetCleaning = new JetCleaningTool( "JetCleaning" );
  if( !m_jetCleaning->setProperty( "CutLevel", "LooseBad" ).isSuccess() ){
    Info( "JetToolsInitializer()", "Error in JetCleaningTool CutLevel setting" );
  }
  if( m_jetCleaning->setProperty( "DoUgly", false ).isSuccess() ){
    Info( "JetToolsInitializer()", "Error in JetCleaningTool DoUgly setting" );    
  }
  if( !m_jetCleaning->initialize().isSuccess() ){
    Info( "JetToolsInitializer()", "Cannot initialize JetCleaningTool" );
  }
  else{
    Info( "JetToolsInitializer()", "JetCleaningTool initialized!" );
  }

  /* Jet Forward JVT Tool */
  m_jetFrwJvtTool = new JetForwardJvtTool( "JetForwardJvtTool" );
  //if( !m_jetFrwJvtTool->setProperty( "CentralMaxPt",60e3 ).isSuccess() ){
  //  Info( "JetToolsInitialized()", "Error in JetForwardJvtTool setting CentralMaxPt value" );
  //}
  if( !m_jetFrwJvtTool->initialize().isSuccess() ){
    Info( "JetToolsInitialized()", "Cannot intialize JetForwardJvtTool!" );
  }
  else{
    Info( "JetToolsInitializer()", "JetForwardJvtTool initialized!" );
  }
  
  /* Jet JVT Efficiency Tool */
  m_jetJvtSF = new CP::JetJvtEfficiency( "jetJvtSF" );
  if( !m_jetJvtSF->setProperty( "SFFile", "JetJvtEfficiency/Moriond2018/JvtSFFile_EMTopoJets.root" ).isSuccess() ){
    Info( "JetToolsInitializer()", "Error in JetJvtEfficiency setting SF File");
  }
  if( !m_jetJvtSF->setProperty( "WorkingPoint", "Medium").isSuccess() ){
    Info( "JetToolsInitializer()", "Error in JetJvtEfficiency setting WorkingPoint Option");
  }
  if( !m_jetJvtSF->initialize().isSuccess() ){
    Info( "JetToolsInitializer()", "Cannot initialize JetJvtEfficiencyTool" );
  }
  else{
    Info( "JetToolsInitializer()", "JetJvtEfficiencyTool initialized!" );
  }
  
  /* Jet Calibration Tool */
  std::string m_jetConf     = "JES_data2017_2016_2015_Consolidated_EMTopo_2018_Rel21.config";
  std::string m_jetCalibSeq = "JetArea_Residual_EtaJES_GSC_Smear";
  if( m_FastSim ){
    m_jetConf     = "JES_MC16Recommendation_AFII_EMTopo_April2018_rel21.config";
    m_jetCalibSeq = "JetArea_Residual_EtaJES_GSC";
  }
  if( m_isData ) m_jetCalibSeq = "JetArea_Residual_EtaJES_GSC_Insitu";
  
  m_jetCalibration = new JetCalibrationTool( "JetCalib" );
  if( !m_jetCalibration->setProperty( "JetCollection", "AntiKt4EMTopo" ).isSuccess() ){
    Info( "JetToolsInitializer()", "Error in JetCalibrationTool setting Jet Colletion" );
  }
  if( !m_jetCalibration->setProperty( "CalibSequence", m_jetCalibSeq.c_str() ).isSuccess() ){
    Info( "JetToolsInitializer()", "Error in JetCalibrationTool setting Calibration Sequence" );
  }
  if( !m_jetCalibration->setProperty( "ConfigFile", m_jetConf.c_str() ).isSuccess() ){
    Info( "JetToolsInitializer()", "Error in JetCalibrationTool setting Config File" );
  }
  if( !m_jetCalibration->setProperty( "IsData", m_isData ).isSuccess() ){
    Info( "JetToolsInitializer()", "Error in JetCalibrationTool setting IsData flag" );
  }
  if( !m_jetCalibration->setProperty( "CalibArea", "00-04-82").isSuccess() ){
    Info( "JetToolsInitializer()", "Error in JetCalibrationTool setting CalibArea" );
  }
  if( !m_jetCalibration->initialize().isSuccess() ){
    Info( "JetToolsInitializer()", "Cannot initialize JetCalibrationTool" );
  }
  else{
    Info( "JetToolsInitializer()", "JetCalibrationTool initialized!" );
  }

  /* Jet Resolution Tool */
  m_JERTool = new JERTool( "JetResolution" );
  if( !m_JERTool->setProperty( "PlotFileName", "JetResolution/Prerec2015_xCalib_2012JER_ReducedTo9NP_Plots_v2.root" ).isSuccess() ){
    Info( "JetToolsInitializer()", "Error in JERTool setting Plot File Name" );
  }
  if( !m_JERTool->setProperty( "CollectionName", "AntiKt4EMTopoJets" ).isSuccess() ){
    Info( "JetToolsInitializer()", "Error in JERTool setting jet CollectionName" );
  }
  if( !m_JERTool->initialize().isSuccess() ){
    Info( "JetToolsInitializer()", "Cannot initialize JERTool (JetResolution)" );
  }
  else{
    Info( "JetToolsInitializer()", "JERTool (JetResolution) initialized!" );
  }
  
  /* Jet Smearing Tool */
  smearJetTool = new JERSmearingTool( "JetSmearing" );
  m_jetResoHand = ToolHandle<IJERTool>("JetResolution");
  if( !smearJetTool->setProperty( "JERTool", m_jetResoHand ).isSuccess() ){
    Info( "JetToolsInitializer()", "Error in JERSmearingTool setting JERTool Handle" );
  }
  if( !smearJetTool->setProperty("ApplyNominalSmearing", false ).isSuccess() ){
    Info( "JetToolsInitializer()", "Error in JERSmearingTool setting ApplyNominalSmearing Option" );
  }
  if( !smearJetTool->setProperty("isMC", !m_isData ).isSuccess() ){
    Info( "JetToolsInitializer()", "Error in JERSmearingTool setting isMC Option" );
  }
  if( !smearJetTool->setProperty("SystematicMode", "Simple" ).isSuccess() ){
    Info( "JetToolsInitializer()", "Error in JERSmearingTool setting SystematicMode Option" );
  }
  if( !smearJetTool->initialize().isSuccess() ){
    Info( "JetToolsInitializer()", "Cannot initialize JERSmearingTool (JetSmearing)" );
  }
  else{
    Info( "JetToolsInitializer()", "JERSmearingTool (JetSmearing) initialized!" );
  }

  /* Jet Uncertainties Tool */
  m_jetUnc = new JetUncertaintiesTool( "JESTool" );
  if( !m_jetUnc->setProperty( "JetDefinition", "AntiKt4EMTopo").isSuccess() ){
    Info( "JetToolsInitializer()", "Error in JetUncertaintiesTool setting Jet Definition Option" );
  }
  if( !m_jetUnc->setProperty( "MCType", "MC16" ).isSuccess() ){ 
    Info( "JetToolsInitializer()", "Error in JetUncertaintiesTool setting MC Type Option" );
  }
  if( !m_jetUnc->setProperty( "ConfigFile", "rel21/Fall2018/R4_SR_Scenario1_SimpleJER.config" ) ){
    Info( "JetToolsInitializer()", "Error in JetUncertaintiesTool setting Config File Option" );
  }
  if( !m_jetUnc->setProperty( "CalibArea", "CalibArea-06") ){
    Info( "JetToosInitializer()", "Error in JetUncertaintiesTool setting CalibArea Option");
  }
  if( !m_jetUnc->setProperty( "IsData", m_isData ).isSuccess() ){
    Info( "JetToosInitializer()", "Error in JetUncertaintiesTool setting IsData Option");
  }
  if( !m_jetUnc->initialize().isSuccess() ){
    Info( "JetToolsInitializer()", "Cannot initialize JetUncertaintiesTool" );
  }
  else{
    Info( "JetToolsInitializer()", "JetUncertaintiesTool initialized!" );
  }

  /* b-tagging Selector */
  m_btagSelection = new BTaggingSelectionTool( "BTagSelector" );
  if( !m_btagSelection->setProperty( "MaxEta", 2.5 ).isSuccess() ){
    Info( "JetToolsInitializer()", "Error in BTaggingSelectionTool setting MaxEta" );
  }
  if( !m_btagSelection->setProperty( "MinPt", 25000. ).isSuccess() ){
    Info( "JetToolsInitializer()", "Error in BTaggingSelectionTool setting MinPt" );
  }
  if( !m_btagSelection->setProperty( "FlvTagCutDefinitionsFileName", "xAODBTaggingEfficiency/13TeV/2017-21-13TeV-MC16-CDI-2018-10-19_v1.root" ).isSuccess() ){
    Info( "JetToolsInitializer()", "Error in BTaggingSelectionTool setting Cut Definition File" );
  }
  if( !m_btagSelection->setProperty( "TaggerName", "MV2c10"  ).isSuccess() ){
    Info( "JetToolsInitializer()", "Error in BTaggingSelectionTool setting Tagger Name" );
  }
  if( !m_btagSelection->setProperty( "OperatingPoint", "FixedCutBEff_77" ).isSuccess() ){
    Info( "JetToolsInitializer()", "Error in BTaggingSelectionTool setting Operating point" );
  }
  if( !m_btagSelection->setProperty( "JetAuthor", "AntiKt4EMTopoJets" ).isSuccess() ){
    Info( "JetToolsInitializer()", "Error in BTaggingSelectionTool setting Jet Author (algorithm)" );
  }
  if( !m_btagSelection->initialize().isSuccess() ){
    Info( "JetToolsInitializer()", "Cannot initialize BTaggingSelectionTool" );
  }
  else{
    Info( "JetToolsInitializer()", "BTaggingSelectionTool initialized!" );
  }

  /* b-Tagging Efficiency SF */
  m_btagSF = new BTaggingEfficiencyTool( "BTaggingEfficiency" );
  if( !m_btagSF->setProperty( "TaggerName", "MV2c10" ).isSuccess() ){
    Info( "JetToolsInitializer()", "Error in BTaggingEfficiencyTool setting Tagger Name" );
  }
  if( !m_btagSF->setProperty( "OperatingPoint", "FixedCutBEff_77" ).isSuccess() ){
    Info( "JetToolsInitializer()", "Error in BTaggingEfficiencyTool setting Operating point" );
  }
  if( !m_btagSF->setProperty( "JetAuthor", "AntiKt4EMTopoJets" ).isSuccess() ){
    Info( "JetToolsInitializer()", "Error in BTaggingEfficiencyTool setting Jet Author (algorithm)" );
  }
  if( !m_btagSF->setProperty( "ScaleFactorFileName", "xAODBTaggingEfficiency/13TeV/2017-21-13TeV-MC16-CDI-2018-10-19_v1.root" ).isSuccess() ){
    Info( "JetToolsInitializer()", "Error in BTaggingEfficiencyTool setting ScaleFactor File" );
  }
  if( !m_btagSF->setProperty( "ConeFlavourLabel", true ).isSuccess() ){
    Info( "JetToolsInitializer()", "Error in BTaggingEfficiencyTool setting Cone Flavour Label" );
  }
  if( !m_btagSF->setProperty( "SystematicsStrategy", "Envelope").isSuccess() ){
    Info( "JetToolsInitializer()", "Error in BTaggingEfficiencyTool setting Systematics Strategy" );
  }
  if( !m_btagSF->initialize().isSuccess() ){
    Info( "JetToolsInitializer()", "Cannot initialize BTaggingEfficiencyTool" );
  }
  else{
    Info( "JetToolsInitializer()", "BTaggingEfficiencyTool initialized!" );
  }

  // Configure the JVT tool
  pjvtag = new JetVertexTaggerTool( "JetVertexTagger" );
  hjvtagup = ToolHandle<IJetUpdateJvt>( "JetVertexTagger" );
  if( !pjvtag->setProperty( "JVTFileName", "JetMomentTools/JVTlikelihood_20140805.root" ).isSuccess() ){
    Info( "JetToolsInitializer()", "Error in JetVertexTaggerTool setting JVT File Name" );
  }
  if( !pjvtag->initialize().isSuccess() ){
    Info( "JetToolsInitializer()", "Cannot initialize JetVertexTaggerTool" );
  }
  else{
    Info( "JetToolsInitializer()", "JetVertexTaggerTool initialized!" );
  }

  // Systematic Variations
  const CP::SystematicRegistry& registry = CP::SystematicRegistry::getInstance();
  const CP::SystematicSet& recommendedSystematics = registry.recommendedSystematics(); // get list of recommended systematics

  // this is the nominal set, no systematic
  m_sysList.push_back(CP::SystematicSet());

  // OLR tool
  m_orFlags = ORUtils::ORFlags("OverlapRemovalTool", "isPreSel", "isOverlap");
  m_orFlags.doElectrons = true;
  m_orFlags.doMuons     = true;
  m_orFlags.doJets      = true;
  m_orFlags.doTaus      = true;
  m_orFlags.doPhotons   = false;
  ORUtils::recommendedTools(m_orFlags, toolBox);
  if(m_debug) toolBox.setGlobalProperty("OutputLevel", MSG::DEBUG);
  if( !toolBox.initialize().isSuccess() ){
    Info( "ToolsInitializer()", "Cannot initialize toolBox for OverlapRemovalTool" );
  }
  else{
    Info( "ToolsInitializer()", "toolBox for OverlapRemovalTool initialized" );
  }  
  orTool = std::move(toolBox.masterTool);     
  Info( "ToolsInitializer()", "OverlapRemovalTool initialized" );

  // loop over recommended systematics
  for(CP::SystematicSet::const_iterator sysItr = recommendedSystematics.begin(); sysItr != recommendedSystematics.end(); ++sysItr){
    TString syst_name = TString(sysItr->name());
    if(!syst_name.BeginsWith("EL") && !syst_name.BeginsWith("MU") && !syst_name.BeginsWith("EG") && !syst_name.BeginsWith("TAU") && !syst_name.BeginsWith("JET")) continue; 
    std::cout << "Systematic: " << (*sysItr).name() << std::endl;
    m_sysList.push_back( CP::SystematicSet() );
    m_sysList.back().insert( *sysItr );
  } // end for loop over recommended systematics

  return EL::StatusCode::SUCCESS;
} // end of initialise 
