#include <MyAnalysis/MyxAODAnalysis.h>

#ifdef __CLING__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#pragma link C++ namespace ORUtils;
#pragma link C++ class ORUtils::IOverlapRemovalTool+;
#pragma link C++ class ORUtils::IOverlapTool+;
#pragma link C++ class ORUtils::BaseOverlapTool+;
#pragma link C++ class ORUtils::OverlapRemovalTool+;

#pragma link C++ class ORUtils::DeltaROverlapTool+;
#pragma link C++ class ORUtils::EleEleOverlapTool+;
#pragma link C++ class ORUtils::EleJetOverlapTool+;
#pragma link C++ class ORUtils::EleMuSharedTrkOverlapTool+;
#pragma link C++ class ORUtils::MuJetOverlapTool+;
#pragma link C++ class ORUtils::AltMuJetOverlapTool+;
#pragma link C++ class ORUtils::TauLooseEleOverlapTool+;
#pragma link C++ class ORUtils::TauLooseMuOverlapTool+;
#pragma link C++ class ORUtils::TauJetOverlapTool+;
#pragma link C++ class ORUtils::TauAntiTauJetOverlapTool+;
#pragma link C++ class ORUtils::ObjLinkOverlapTool+;

// Helpers
#pragma link C++ class ORUtils::ToolBox+;
//#pragma link C++ class ORUtils::ToolBoxBase+;
//#pragma link C++ class ORUtils::ORToolBox+;
#pragma link C++ class ORUtils::BJetHelper+;
#pragma link C++ class ORUtils::DeltaRMatcher+;
#pragma link C++ class ORUtils::SlidingDeltaRMatcher+;
#pragma link C++ class ORUtils::MuJetGhostDRMatcher+;
#pragma link C++ class ORUtils::OverlapDecorationHelper+;
#pragma link C++ class ORUtils::OverlapLinkHelper+;

// Legacy tools
#pragma link C++ class IOverlapRemovalTool+;
#pragma link C++ class OverlapRemovalTool+;

#endif

#ifdef __CLING__
#pragma link C++ class MyxAODAnalysis+;
#endif
